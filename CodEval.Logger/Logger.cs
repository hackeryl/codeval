﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace CodEval.Logger
{
    public static class Logger
    {
        static Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void Info(Type source, object info)
        {
            ILog logger = LogManager.GetLogger(source);
            logger.Info(info);
        }

        public static void Debug(Type source, object debug)
        {
            ILog logger = LogManager.GetLogger(source);
            logger.Debug(debug);
        }

        public static void Warn(Type source, object warn)
        {
            ILog logger = LogManager.GetLogger(source);
            logger.Warn(warn);
        }

        public static void Error(Type source, object error)
        {
            ILog logger = LogManager.GetLogger(source);
            logger.Error(error);
        }

        public static void Error(Type source, object error, Exception exception)
        {
            ILog logger = LogManager.GetLogger(source);
            logger.Error(error, exception);
        }         
    }
}
