﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.DAL.Repository
{
    /// <summary>
    /// Unit of Work Pattern Implementation
    /// </summary>
    public class UnitOfWork : IDisposable
    {
        private DbContext context;

        private IRepository<Admin> adminsRepository;
        private IRepository<Contest> contestsRepository;
        private IRepository<Contestant> contestantsRepository;
        private IRepository<DefaultPassword> defaultPasswordsRepository;
        private IRepository<Problem> problemsRepository;
        private IRepository<ProblemCategory> problemCategoriesRepository;
        private IRepository<ProblemReview> problemReviewsRepository;
        private IRepository<ProgrammingLanguage> programmingLanguagesRepository;
        private IRepository<Question> questionsRepository;
        private IRepository<Status> statusesRepository;
        private IRepository<Submission> submissionsRepository;
        private IRepository<SubmissionResult> submissionResultsRepository;
        private IRepository<Test> testsRepository;

        public IRepository<Admin> AdminsRepository
        {
            get
            {
                if (this.adminsRepository == null)
                {
                    this.adminsRepository = new Repository<Admin>(context);
                }
                return adminsRepository;
            }
        }
        public IRepository<Contest> ContestsRepository
        {
            get
            {
                if (this.contestsRepository == null)
                {
                    this.contestsRepository = new Repository<Contest>(context);
                }
                return contestsRepository;
            }
        }
        public IRepository<Contestant> ContestantsRepository
        {
            get
            {
                if (this.contestantsRepository == null)
                {
                    this.contestantsRepository = new Repository<Contestant>(context);
                }
                return contestantsRepository;
            }
        }
        public IRepository<DefaultPassword> DefaultPasswordsRepository
        {
            get
            {
                if (this.defaultPasswordsRepository == null)
                {
                    this.defaultPasswordsRepository = new Repository<DefaultPassword>(context);
                }
                return defaultPasswordsRepository;
            }
        }
        public IRepository<Problem> ProblemsRepository
        {
            get
            {
                if (this.problemsRepository == null)
                {
                    this.problemsRepository = new Repository<Problem>(context);
                }
                return problemsRepository;
            }
        }
        public IRepository<ProblemCategory> ProblemCategoriesRepository
        {
            get
            {
                if (this.problemCategoriesRepository == null)
                {
                    this.problemCategoriesRepository = new Repository<ProblemCategory>(context);
                }
                return problemCategoriesRepository;
            }
        }
        public IRepository<ProblemReview> ProblemReviewsRepository
        {
            get
            {
                if (this.problemReviewsRepository == null)
                {
                    this.problemReviewsRepository = new Repository<ProblemReview>(context);
                }
                return problemReviewsRepository;
            }
        }
        public IRepository<ProgrammingLanguage> ProgrammingLanguagesRepository
        {
            get
            {
                if (this.programmingLanguagesRepository == null)
                {
                    this.programmingLanguagesRepository = new Repository<ProgrammingLanguage>(context);
                }
                return programmingLanguagesRepository;
            }
        }
        public IRepository<Question> QuestionsRepository
        {
            get
            {
                if (this.questionsRepository == null)
                {
                    this.questionsRepository = new Repository<Question>(context);
                }
                return questionsRepository;
            }
        }
        public IRepository<Status> StatusesRepository
        {
            get
            {
                if (this.statusesRepository == null)
                {
                    this.statusesRepository = new Repository<Status>(context);
                }
                return statusesRepository;
            }
        }
        public IRepository<Submission> SubmissionsRepository
        {
            get
            {
                if (this.submissionsRepository == null)
                {
                    this.submissionsRepository = new Repository<Submission>(context);
                }
                return submissionsRepository;
            }
        }
        public IRepository<SubmissionResult> SubmissionResultsRepository
        {
            get
            {
                if (this.submissionResultsRepository == null)
                {
                    this.submissionResultsRepository = new Repository<SubmissionResult>(context);
                }
                return submissionResultsRepository;
            }
        }
        public IRepository<Test> TestsRepository
        {
            get
            {
                if (this.testsRepository == null)
                {
                    this.testsRepository = new Repository<Test>(context);
                }
                return testsRepository;
            }
        }

        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
