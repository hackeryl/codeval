//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CodEval.DAL.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProblemReview
    {
        public int ID { get; set; }
        public int Id_Problem { get; set; }
        public int Id_Contestant { get; set; }
        public Nullable<int> Rate { get; set; }
        public string Comment { get; set; }
        public System.DateTime ReviewDate { get; set; }
    
        public virtual Contestant Contestant { get; set; }
        public virtual Problem Problem { get; set; }
    }
}
