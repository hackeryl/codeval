USE [master]
GO
/****** Object:  Database [CodEval]    Script Date: 30-Jun-15 13:04:24 ******/
CREATE DATABASE [CodEval]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CodEval', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CodEval.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CodEval_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CodEval_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CodEval] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CodEval].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CodEval] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CodEval] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CodEval] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CodEval] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CodEval] SET ARITHABORT OFF 
GO
ALTER DATABASE [CodEval] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CodEval] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CodEval] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CodEval] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CodEval] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CodEval] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CodEval] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CodEval] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CodEval] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CodEval] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CodEval] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CodEval] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CodEval] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CodEval] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CodEval] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CodEval] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CodEval] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CodEval] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CodEval] SET RECOVERY FULL 
GO
ALTER DATABASE [CodEval] SET  MULTI_USER 
GO
ALTER DATABASE [CodEval] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CodEval] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CodEval] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CodEval] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CodEval]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](512) NOT NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
	[ApiKey] [nvarchar](512) NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contest]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Id_Admin] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Competition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contest_ProgrammingLanguage_Link]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contest_ProgrammingLanguage_Link](
	[Id_Contest] [int] NOT NULL,
	[Id_ProgrammingLanguage] [int] NOT NULL,
 CONSTRAINT [PK_Contest_ProgrammingLanguage_Link] PRIMARY KEY CLUSTERED 
(
	[Id_Contest] ASC,
	[Id_ProgrammingLanguage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contestant]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contestant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](512) NOT NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Contestant] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DefaultPasswords]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultPasswords](
	[ContestantId] [int] NOT NULL,
	[DefaultPassword] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_DefaultPasswords] PRIMARY KEY CLUSTERED 
(
	[ContestantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Problem]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Id_Contest] [int] NOT NULL,
	[Id_Admin] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[TimeLimit] [int] NOT NULL,
	[MemoryLimit] [int] NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[Input] [nvarchar](max) NOT NULL,
	[Output] [nvarchar](max) NOT NULL,
	[Restrictions] [nvarchar](max) NULL,
	[Examples] [nvarchar](max) NULL,
 CONSTRAINT [PK_Problem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Problem_ProblemCategory_Link]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problem_ProblemCategory_Link](
	[Id_Problem] [int] NOT NULL,
	[Id_ProblemCategory] [int] NOT NULL,
 CONSTRAINT [PK_Problem_ProblemCategory_Link] PRIMARY KEY CLUSTERED 
(
	[Id_Problem] ASC,
	[Id_ProblemCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProblemCategory]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProblemCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](3072) NULL,
 CONSTRAINT [PK_ProblemCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProblemReview]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProblemReview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Id_Problem] [int] NOT NULL,
	[Id_Contestant] [int] NOT NULL,
	[Rate] [int] NULL,
	[Comment] [nvarchar](max) NULL,
	[ReviewDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProblemReview] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgrammingLanguage]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgrammingLanguage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[AcceptedSourcesExtensions] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_ProgrammingLanguage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Contestant] [int] NOT NULL,
	[ID_Admin] [int] NULL,
	[Question] [nvarchar](3072) NOT NULL,
	[Answer] [nvarchar](3072) NULL,
	[Answered] [bit] NOT NULL,
 CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Status]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Code] [nvarchar](8) NOT NULL,
	[Color] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Submission]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Submission](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Problem] [int] NOT NULL,
	[ID_Contestant] [int] NOT NULL,
	[FileName] [varchar](265) NOT NULL,
	[SubmissionTime] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Submission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubmissionResult]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubmissionResult](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Submission] [int] NOT NULL,
	[ID_Test] [int] NOT NULL,
	[ID_Status] [int] NOT NULL,
	[Score] [int] NOT NULL,
	[Time] [decimal](18, 2) NOT NULL,
	[Memory] [decimal](18, 2) NOT NULL,
	[EvaluationTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SubmissionResult] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Test]    Script Date: 30-Jun-15 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Test](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Problem] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[InputLocation] [nvarchar](128) NOT NULL,
	[OKLocation] [nvarchar](128) NOT NULL,
	[Points] [int] NOT NULL,
 CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([ID], [UserName], [Password], [FirstName], [LastName], [Email], [ApiKey]) VALUES (1, N'vasile_pojoga', N'6125BFAE69C043293E10C947415FD037', N'Vasile', N'Pojoga', N'vasea.pojoga@gmail.com', N'B9BF4CA5B29172C93CD79B6D23CC9')
INSERT [dbo].[Admin] ([ID], [UserName], [Password], [FirstName], [LastName], [Email], [ApiKey]) VALUES (2, N'catalin_vlas', N'6125BFAE69C043293E10C947415FD037', N'Catalin', N'Vlas', N'catalin.v@mail.ru', N'711f61b4-01e9-4aeb-9c2a-9752b2f2901e')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Contest] ON 

INSERT [dbo].[Contest] ([ID], [Id_Admin], [Name], [StartDate], [EndDate], [Description]) VALUES (6, 1, N'SVO 2014', CAST(0x0000A4BF00A10130 AS DateTime), CAST(0x0000A4C700A10130 AS DateTime), N'Scoala Viitorilor Olimpici, 2014')
INSERT [dbo].[Contest] ([ID], [Id_Admin], [Name], [StartDate], [EndDate], [Description]) VALUES (8, 1, N'SVO 2015', CAST(0x0000A4C600A6F950 AS DateTime), CAST(0x0000A4C900A6F950 AS DateTime), N'Scoala Viitorilor Olimpici, 2015')
SET IDENTITY_INSERT [dbo].[Contest] OFF
SET IDENTITY_INSERT [dbo].[Contestant] ON 

INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (1, N'vasile_pojoga', N'6125BFAE69C043293E10C947415FD037', N'Vasile', N'Pojoga', N'vasea.pojoga@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (2, N'catalin_vlas', N'6125BFAE69C043293E10C947415FD037', N'Catalin', N'Vlas', N'catalin.v@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (3, N'dimitrie_pirghie', N'6125BFAE69C043293E10C947415FD037', N'Dimitrie', N'Pirghie', N'dimitrie.pirghie@info.uaic.ro')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (6, N'alexandru_casian', N'6125BFAE69C043293E10C947415FD037', N'Alexandru', N'Casian', N'a.c@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (7, N'anatolii_guzovatii', N'6125BFAE69C043293E10C947415FD037', N'Anatolii', N'Guzovatii', N'a.g@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (10, N'cristina_arhiliuc', N'6125BFAE69C043293E10C947415FD037', N'Cristina', N'Arhiliuc', N'c.a@yahoo.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (11, N'cristina_artene', N'6125BFAE69C043293E10C947415FD037', N'Cristina', N'Artene', N'c.ae@mail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (12, N'dan_bejan', N'6125BFAE69C043293E10C947415FD037', N'Dan', N'Bejan', N'dan.b@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (13, N'daniel_toncu', N'6125BFAE69C043293E10C947415FD037', N'Daniel', N'Toncu', N'daniel@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (14, N'gabriel_cojocaru', N'6125BFAE69C043293E10C947415FD037', N'Gabriel', N'Cojocaru', N'gabi@mail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (15, N'gheorghe_mironica', N'6125BFAE69C043293E10C947415FD037', N'Gheorghe', N'Mironica', N'gicu@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (16, N'grigore_brodicico', N'6125BFAE69C043293E10C947415FD037', N'Grigore', N'Brodicico', N'brod@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (17, N'ion_ureche', N'6125BFAE69C043293E10C947415FD037', N'Ion', N'Ureche', N'ion.u@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (18, N'lilian_ciobanu', N'6125BFAE69C043293E10C947415FD037', N'Lilian', N'Ciobanu', N'lil@yahoo.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (19, N'marcel_bezdrighin', N'6125BFAE69C043293E10C947415FD037', N'Marcel', N'Bezdrighin', N'm.b@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (20, N'nicolae_moldovanu', N'6125BFAE69C043293E10C947415FD037', N'Nicolae', N'Moldovanu', N'n.m@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (21, N'nicusor_chiciuc', N'6125BFAE69C043293E10C947415FD037', N'Nicusor', N'Chiciuc', N'nicu@yahoo.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (22, N'tamara_trifan', N'6125BFAE69C043293E10C947415FD037', N'Tamara', N'Trifan', N't.t@mail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (23, N'vasile_catana', N'6125BFAE69C043293E10C947415FD037', N'Vasile', N'Catana', N'vasi@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (24, N'victor_ciuntu', N'6125BFAE69C043293E10C947415FD037', N'Victor', N'Ciuntu', N'victor@ciuntu.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (25, N'vasile_groza', N'6125BFAE69C043293E10C947415FD037', N'Vasile', N'Groza', N'vasil.g@gmail.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (26, N'vadim_casap', N'6125BFAE69C043293E10C947415FD037', N'Vadim', N'Casap', N'vadi@mail.ru')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (27, N'valeriu_motroi', N'6125BFAE69C043293E10C947415FD037', N'Valeriu', N'Motroi', N'valeriu@motroi.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (32, N'alexei_rusu', N'6125BFAE69C043293E10C947415FD037', N'Alexei', N'Rusu', N'alex@r.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (33, N'dumitru_savva', N'6125BFAE69C043293E10C947415FD037', N'Dumitru', N'Savva', N'savva@dummy.com')
INSERT [dbo].[Contestant] ([ID], [Username], [Password], [FirstName], [LastName], [Email]) VALUES (34, N'vlad_serema', N'6125BFAE69C043293E10C947415FD037', N'Vlad', N'Serema', N'serema@gmail.com')
SET IDENTITY_INSERT [dbo].[Contestant] OFF
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (1, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (2, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (3, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (6, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (7, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (10, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (11, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (12, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (13, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (14, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (15, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (16, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (17, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (18, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (19, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (20, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (21, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (22, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (23, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (24, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (25, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (26, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (27, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (32, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (33, N'6125BFAE69C043293E10C947415FD037')
INSERT [dbo].[DefaultPasswords] ([ContestantId], [DefaultPassword]) VALUES (34, N'6125BFAE69C043293E10C947415FD037')
SET IDENTITY_INSERT [dbo].[Problem] ON 

INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (36, 6, 1, N'joc', 400, 16, N'<p style="text-align:justify;"><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">Nargy si Fumeanu
joaca un joc pe un graf orientat fara circuite cu </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">N</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> noduri
(numerotate de la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">1</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">N</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">) si </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">M</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> arce. In acest graf exista </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">K</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> pioni plasati in noduri. Fiecare
jucator trebuie sa mute, cand ii vine randul, toti pionii care se pot muta din
nodurile in care se afla in noduri adiacente. Mai exact, un pion situat in
nodul </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">x</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> poate fi mutat in nodul </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">y</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">, daca exista arc de la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">x</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">y</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">.Cel care nu mai poate muta nici un pion atunci cand ii vine randul pierde
partida.La inceputul unui joc, prima mutare o face jucatorul Nargy.</span></p><p style="text-align:justify;"><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">Dandu-se mai multe
configuratii de pioni, sa se determine cine castiga jocul, daca ambii jucatori
joaca optim.</span><span lang="RO" style="font-size:13.5pt;font-family:Verdana, sans-serif;"> </span></p><p style="text-align:justify;"></p><p style="text-align:justify;"><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"><br /></span></p>', N'<p style="text-align:justify;"><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">Fisierul de
intrare </span><strong><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">joc.in</span></strong><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;"> </span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">contine pe prima
linie 3 numere naturale </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">T N M</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">. Urmatoarele </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">M</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> linii vor contine cate doua numere naturale </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">a b</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> cu semnificatia
ca exista arc de la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">a</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> la </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">b</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">. Urmatoarele </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">T</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> linii vor descrie configuratii de pioni astfel: primul numar de pe linie
va fi </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">K</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> si va fi urmat de </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">K</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> numere naturale reprezentand nodurile in
care se afla pioni. Numerele de pe aceeasi linie sunt separate prin spatii.</span></p>', N'<p><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">Fisierul de iesire </span><strong><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">joc.out</span></strong><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> va contine </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">T</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> linii, cate o linie pentru fiecare configuratie de pioni din fisierul de
intrare. Pe linia </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">i</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> se va scrie numele jucatorului care castiga
(</span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">Nargy</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;"> sau </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">Fumeanu</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">) pentru a </span><span lang="RO" style="font-size:10pt;font-family:''Courier New'';">i</span><span lang="RO" style="font-size:10pt;font-family:Verdana, sans-serif;">-a configuratie de pioni din fisierul de intrare.</span></p>', N'<p style="text-align:justify;"></p><ul><li><span lang="RO" style="font-size:10.0pt;font-family:''Courier New'';">1 &lt;= T &lt;=
5</span></li><li><span lang="RO" style="font-size:10.0pt;font-family:''Courier New'';">1 &lt;= N &lt;= 20 000</span></li><li><span lang="RO" style="font-size:10.0pt;font-family:''Courier New'';">1 &lt;= M &lt;= 50 000</span></li><li><span lang="RO" style="font-size:10.0pt;font-family:''Courier New'';">1 &lt;= K &lt;= 30 000</span></li><li><span lang="RO" style="font-size:10.0pt;font-family:''Verdana'',''sans-serif'';">Pot exista mai multi pioni in acelasi nod.</span></li></ul>', N'<table border="1"><tbody><tr style="vertical-align:top;text-align:left;"><td>joc.in</td><td>joc.out</td><td>Explicatie</td></tr><tr style="vertical-align:top;text-align:left;"><td style="width:75px;">2 4 3<br />2 1<br />3 1<br />4 3<br />3 2 2 3<br />2 1 4
            </td><td>Nargy<br />Fumeanu
 </td><td>1. Conform regulilor jocului Nargy trebuie sa mute cu toti cei 3 pioni, iar singura posibilitate este sa-i mute pe toti in 1. Fumeanu nu mai poate muta.<br />2. Nargy muta pionul din 4 in 3, iar Fumeanu muta din 3 in 1. Acestea sunt singurele mutari posibile.
            </td></tr></tbody></table>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (37, 6, 1, N'piloti', 100, 16, N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Vasile are o companie de
transport aerian. Pentru a se mentine pe piata, el trebuie sa reduca
cheltuielile c&acirc;t mai mult posibil.<br />La compania sa exista&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;piloti (</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;par). Pilotii sunt numerotati de la&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">1</span><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;la&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;&icirc;n ordinea crescatoare a v&acirc;rstei
(pilotul&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">1</span><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;este cel mai t&acirc;nar, pilotul&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;cel mai batr&acirc;n).<br />Vasile trebuie sa constituie&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N/2</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;echipaje. Un
echipaj este format din 2 piloti (capitanul si asistentul sau). Capitanul
trebuie sa fie mai &icirc;n v&acirc;rsta dec&acirc;t asistentul sau.<br />&Icirc;n contractul fiecarui pilot sunt prevazute doua salarii: unul pentru cazul &icirc;n
care el este capitan al echipajului, celalalt pentru cazul &icirc;n care el este
asistent. Evident, pentru orice pilot salariul sau de capitan este mai mare
dec&acirc;t salariul sau de asistent.<br />Salariile pot sa difere de la un pilot la altul. Chiar se poate &icirc;nt&acirc;mpla ca
salariul capitanului sa fie mai mic dec&acirc;t salariul asistentului sau.<br />Pentru a cheltui c&acirc;t mai putini bani pe salariile pilotilor, Vasile trebuie sa
determine o distribuire optimala a pilotilor pe echipaje.</span></p><p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Scrieti un program care
sa determine suma minima necesara pentru a plati salariile pilotilor.</span></p><p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;"></span></p>', N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Fisierul de
intrare&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">piloti.in</span><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;contine pe prima linie un numar
natural&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:''Courier New'';">&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">reprezent&acirc;nd numarul de piloti.<br />Pe urmatoarele&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii sunt
informatii despre salariile pilotilor. Pe linia&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">i+1</span></em><span style="font-size:9pt;font-family:''Courier New'';">&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">se afla doua numere
naturale&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">c</span></em><span style="font-size:9pt;font-family:''Courier New'';"> <em>a</em>&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">separate printr-un
spatiu (</span><em><span style="font-size:9pt;font-family:''Courier New'';">c</span></em><span style="font-size:9pt;font-family:''Courier New'';">&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">reprezinta salariul pilotului&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">i</span></em><span style="font-size:9pt;font-family:''Courier New'';">&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">pe post de capitan,
iar&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">a</span></em><span style="font-size:9pt;font-family:''Courier New'';">&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">reprezinta salariul pilotului&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">i</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;pe post de asistent).</span></p>', N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Fisierul de iesire&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">piloti.out&nbsp;</span><span style="font-size:9pt;font-family:Arial, sans-serif;">va
contine o singura linie pe care va fi afisata suma minima necesara pentru a
plati salariile celor&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:''Courier New'';"> </span><span style="font-size:9pt;font-family:Arial, sans-serif;">piloti.</span></p>', N'<p style="margin-bottom:7.5pt;"><strong></strong></p><ul type="disc"><li><em><span style="font-size:9.0pt;font-family:''Courier New'';">2
 &lt;= N &lt;= 10000; N</span></em><span style="font-size:9.0pt;font-family:''Courier New'';"> par;</span></li><li style="margin-bottom:7.5pt;"><span style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">Pentru orice pilot,&nbsp;</span><em><span style="font-size:9.0pt;font-family:''Courier New'';">1 &lt;= a &lt; c &lt;= 100 000</span></em></li></ul>', N'<table border="1"><tbody><tr style="vertical-align:top;text-align:left;"><td>piloti.in</td><td>piloti.out</td><td>piloti.in</td><td>piloti.out</td></tr><tr style="vertical-align:top;text-align:left;"><td>6<br />10000 7000<br />9000 3000<br />6000 4000<br />5000 1000<br />9000 3000<br />8000 6000

            </td><td>32000
 </td><td>6<br />5000 3000<br />4000 1000<br />9000 7000<br />11000 5000<br />7000 3000<br />8000 6000
            </td><td>33000
 </td></tr><tr style="vertical-align:top;text-align:left;"><td colspan="2">Perechea 1: 3 conducator 1 asistent  =  6000+7000=13000<br />Perechea 2: 4 conducator 2 asistent  =  5000+3000=8000<br />Perechea 3: 6 conducator 5 asistent  =  8000+3000=11000<br />Total =  32000

            </td><td colspan="2">Perechea 1: 3 conducator 1 asistent  =  9000+3000=12000<br />Perechea 2: 5 conducator 2 asistent  =  7000+1000=8000<br />Perechea 3: 6 conducator 4 asistent  =  8000+5000=13000<br />Total =  33000

            </td></tr></tbody></table>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (38, 6, 1, N'mijloc', 600, 8, N'<p style="margin-bottom:7.5pt;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">Avem o matrice cu&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">N</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii, numerotate
de la&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;la&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">N</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;si&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">M</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;coloane, numerotate de la&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;la&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">M</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">. Numarul de coloane este impar. Valorile
din matrice sunt numere naturale nenule. Orice valoare apare pe aceeasi linie
de cel mult doua ori. Exista mai multe &icirc;ntrebari &icirc;n legatura cu elementele
matricei. O &icirc;ntrebare este de urmatoarea forma: se da&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, un numar &icirc;ntreg. Se cere sa parcurgem un
drum prin matrice, de cost minim, care &icirc;ndeplineste conditiile:&nbsp;<br />-&nbsp;&nbsp;Pleacam de la coloana din mijloc, de deasupra liniei&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;(&icirc;i spunem
linia&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">0</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">).<br />-&nbsp;&nbsp;La un pas, daca ne gasim pe linia&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">i</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, ne deplasam pe linia&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">i</span></em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">+1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">; daca pe linia&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">i</span></em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">+1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;exista valori egale cu&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, trebuie sa ne deplasam obligatoriu &icirc;n
pozitia uneia dintre ele; daca nu exisa valori egale cu&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, ne deplasam obligatoriu &icirc;n pozitia
aflata la mijlocul liniei.&nbsp;<br />-&nbsp;&nbsp;&Icirc;n final trebuie sa ajungem &icirc;n pozitia de sub mijlocul ultimei
linii (sa spunem, la mijlocul liniei&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">N</span></em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">+1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">).<br />-&nbsp;&nbsp;Costul unui pas se calculeaza astfel: daca de pe linia&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">i</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, coloana&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">j</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;pasim pe linia&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">i</span></em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">+1</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, coloana&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">k</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">, costul acestui pas este </span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">modul(<em>j-k</em>)</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">.<br />-&nbsp;&nbsp;Costul unui drum este egal cu suma costurilor tuturor pasilor de
pe drum.</span></p><p style="margin-bottom:0.0001pt;"><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">D&acirc;ndu-se mai multe &icirc;ntrebari, sa se
determine raspunsul la fiecare dintre ele.</span></p><p style="margin-bottom:7.5pt;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;"></span></p>', N'<p style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">Fisierul&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">mijloc.in</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;contine pe prima
linie trei numere naturale,&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">N M Q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;reprezent&acirc;nd
respectiv, numarul de linii ale matricei, numarul de coloane ale matricei si
numarul de &icirc;ntrebari. Pe urmatoarele&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">N</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii, se gasesc c&acirc;te&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">M</span></em><em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">numere naturale, care
reprezinta elementele matricei. Acolo unde sunt mai multe numere pe o linie,
ele sunt separate prin c&acirc;te un spatiu. Urmatoarele&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';">Q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii, contin c&acirc;te un numar natural
care reprezinta o &icirc;ntrebare.</span></p>', N'<p style="margin-bottom:0.0001pt;"><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">&Icirc;n fisierul&nbsp;</span><span lang="RO" style="font-size:9pt;font-family:''Courier New'';background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">mijloc.out</span><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">&nbsp;se
vor scrie&nbsp;</span><em><span lang="RO" style="font-size:9pt;font-family:''Courier New'';background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">Q</span></em><span lang="RO" style="font-size:9pt;font-family:Arial, sans-serif;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;">&nbsp;numere,
c&acirc;te unul pe fiecare linie, reprezent&acirc;nd raspunsurile la &icirc;ntrebari, &icirc;n ordinea
&icirc;n care acestea sunt &icirc;n fisierul de intrare.</span></p>', N'<p style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><strong></strong></p><ul type="square"><li style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9.0pt;font-family:''Courier New'';">1 &lt;= <em>N</em>
 &lt;= 20</span></li><li style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9.0pt;font-family:''Courier New'';">1 &lt;= <em>M</em>
 &lt;= 4999, <em>M</em></span><span lang="RO" style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">&nbsp;impar</span></li><li style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9.0pt;font-family:''Courier New'';">1 &lt;= <em>Q</em>
 &lt;= 5000</span></li><li style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><span lang="RO" style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">Valorile din matrice si &icirc;ntrebarile sunt numere
     naturale nenule mai mici sau egale dec&acirc;t un miliard.</span></li><li style="margin-bottom:0.0001pt;"><span lang="RO" style="font-size:9.0pt;font-family:''Courier New'';">Timp maxim de executie/test: <strong>0.6
 secunde</strong></span></li><li style="margin-bottom:0.0001pt;"><span lang="RO" style="font-size:9.0pt;font-family:''Courier New'';">Memorie totala
     disponibila/stiva: <strong>8MB/1 MB</strong></span></li><li style="margin-bottom:7.5pt;text-align:justify;background-image:initial;background-attachment:initial;background-size:initial;background-origin:initial;background-clip:initial;background-position:initial;background-repeat:initial;"><em><span lang="RO" style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">&Icirc;ntrebarile
 se pot repeta</span></em><span lang="RO" style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">.</span></li></ul>', N'<table border="1"><tbody><tr style="vertical-align:top;text-align:left;"><td>mijloc.in</td><td>mijloc.out</td><td>mijloc.in</td><td>mijloc.out</td></tr><tr style="vertical-align:top;text-align:left;"><td>6 5 4<br />1 2 3 4 5<br />5 4 3 2 1<br />1 1 2 2 3<br />3 3 2 5 6<br />1 2 5 2 1<br />3 3 1 2 4<br />3<br />1<br />9<br />3
 </td><td>8<br />14<br />0<br />8
 </td><td>5 7 10<br />22 21 13 25 21 16 18<br />18 17 14 13 18 19 16<br />17 17 19 20 14 18 11<br />13 16 15 20 18 16 15<br />12 24 21 15 18 26 24<br />10<br />23<br />20<br />19<br />17<br />20<br />22<br />12<br />20<br />12
 </td><td>0<br />0<br />0<br />6<br />4<br />0<br />6<br />6<br />0<br />6
 </td></tr></tbody></table>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (39, 6, 1, N'depasiri', 100, 16, N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Ion sta la intrarea in
tunel, iar Vasile sta la iesire. Fiecare isi noteaza numarul de inregistrare al
fiecarei masini care trece pe langa el si trimit aceste informatiii patrulei de
politie care astepta pe drum, ceva mai la vale.<br />Utilizand informatiile oferite de Ion si Vasile, politia poate determina daca
anumiti soferi au facut depasiri in tunel (ceea ce este strict interzis).
Presupunem ca in tunel masinile nu se pot opri.</span></p><p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Scrieti un program care
sa determine numarul de soferi despre care politia poate afirma cu siguranta ca
au facut o depasire in tunel.</span></p><p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;"></span></p>', N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Fisierul de
intrare&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">depasiri.in</span><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;contine&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">2N+1</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii. Pe prima linie se afla un numar natural&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">, care reprezinta numarul de masini care au
trecut prin tunel. Pe urmatoarele&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii se afla numerele de inregistrare ale masinilor care au
intrat in tunel, in ordinea intrarii. Pe urmatoarele&nbsp;</span><em><span style="font-size:9pt;font-family:''Courier New'';">N</span></em><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;linii se afla numerele de inregistrare ale
masinilor care au iesit din tunel in ordinea iesirii.</span></p>', N'<p style="margin-bottom:7.5pt;"><span style="font-size:9pt;font-family:Arial, sans-serif;">Fisierul de iesire&nbsp;</span><span style="font-size:9pt;font-family:''Courier New'';">depasiri.out</span><span style="font-size:9pt;font-family:Arial, sans-serif;">&nbsp;contine o singura
linie pe care se afla numarul de soferi care vor primi amenda (cei despre care
politia poate afirma cu siguranta ca au efectuat depasiri in tunel).</span></p>', N'<p style="margin-bottom:7.5pt;"><strong></strong></p><ul type="disc"><li><em><span style="font-size:9.0pt;font-family:''Courier New'';">N
 &lt;=1000</span></em></li><li style="margin-bottom:7.5pt;"><span style="font-size:9.0pt;font-family:''Arial'',''sans-serif'';">Numerele de inregistrare ale masinilor au cel putin 6
     si cel mult 8 caractere care pot fi numai litere mari ale alfabetului
     englez si cifre.</span></li></ul>', N'<table border="1"><tbody><tr style="vertical-align:top;text-align:left;"><td>depasiri.in</td><td>depasiri.out</td><td>depasiri.in</td><td>depasiri.out</td><td>depasiri.in</td><td>depasiri.out</td></tr><tr style="vertical-align:top;text-align:left;"><td>4<br />ZG431SN<br />ZG5080K<br />ST123D<br />ZG206A<br />ZG206A<br />ZG431SN<br />ZG5080K<br />ST123D
 </td><td>1</td><td>5<br />ZG206A<br />PU234Q<br />OS945CK<br />ZG431SN<br />ZG5962J<br />ZG5962J<br />OS945CK<br />ZG206A<br />PU234Q<br />ZG431SN
 </td><td>2</td><td>5<br />ZG508OK<br />PU305A<br />RI604B<br />ZG206A<br />ZG232ZF<br />PU305A<br />ZG232ZF<br />ZG206A<br />ZG508OK<br />RI604B
 </td><td>3</td></tr></tbody></table>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (47, 8, 1, N'mosul', 500, 64, N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">Se apropie
Craciunul iar Mo?ul trebuie sa &icirc;mparta cadouri pentru copiii cumin?i. Acesta se
afla acum &icirc;n vacan?a pe &rdquo;Norul Minunat&rdquo; ?i trebuie sa ajunga la depozitul sau
pentru a-?i &icirc;ncarca sania ?i a &icirc;mpar?i cadourile la timp. Sania Mo?ului &icirc;mpreuna
cu renumi?ii reni zboara de pe un nor pe altul &icirc;ntre care exista o unda magica.
Fiecare unda magica are o proprietate interesanta: pe ea se poate zbura cu
minim &nbsp;L si maxim R reni. Nerespectarea
acestei proprieta?i duce la ruperea undei magice. Mo?ul are foarte mul?i reni
?i to?i abia a?teapta sa mearga cu el. &Icirc;nsa acest lucru este imposibil din
cauza proprieta?ii undelor magice. Ace?tia vor macar sa afle care este numarul
maxim &nbsp;de posibilita?i &nbsp;pe care le are Mo?ul de a alege un numar de
reni pe un traseu.</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">&nbsp;Mai exact, &icirc;nainte de a porni la drum Mo?ul
alege un numar x de reni pe care sa-i ia cu el. Definim &icirc;ncrederea unui drum ca
fiind numarul de x-uri distincte pe care &icirc;i putem fixa la &icirc;nceput pentru a
putea parcurge drumul respectiv. Voi trebuie sa-i spune?i Mo?ului care este &icirc;ncrederea
maxima a unui drum dintre toate drumurile posibile de la &rdquo;Norul Minunat&rdquo; la
depozit.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">Pe prima linie &icirc;n
fi?ierul de intrare <strong>mosul.in</strong> sunt
date N ?i M, numarul de nori respectiv numarul de unde magice. Pe urmatoarele M
linii c&acirc;te 4 numere A, B, L, R cu semnifica?ia ca exista o unda magica &icirc;ntre
norul A ?i norul B pe care se poate zbura cu minim &nbsp;L si maxim &nbsp;R reni.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">Pe prima linie &icirc;n
fi?ierul de ie?ire <strong>mosul.out</strong> se va
afi?a numarul maxim K sau &rdquo;FARA CADOURI&rdquo;&nbsp;
&icirc;n cazul &icirc;n care Mo?ul nu poate ajunge la depozit anul acesta.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">2 &lt;= N &lt;=
1000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">0 &lt;= M &lt;= 3000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">1 &lt;= A, B &lt;= N&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">1
&lt;= Li &lt;= Ri &lt;= 1000000</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';">Timpul de execu?ie
nu va depa?i 0.5 sec. pe test.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';"><span style="text-decoration:underline;">&Icirc;ntre
2 nori pot exista mai multe unde magice</span></span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';"><span style="text-decoration:underline;">Pe
orice unda magica se poate zbura &icirc;n ambele direc?ii.</span></span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Times New Roman'',''serif'';"><span style="text-decoration:underline;">Vom considera ca &rdquo;Norul
Minunat&rdquo; este norul 1, iar depozitul se afla &icirc;n norul N.</span></span></p>', N'<p><strong><em></em></strong></p><table><colgroup><col style="width:319px;" /><col style="width:319px;" /></colgroup><thead><tr><th style="text-align:left;"><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">MOSUL.IN</span>
 </th><th style="text-align:left;"><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">MOSUL.OUT</span>
 </th></tr></thead><tbody><tr><td><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">4 4</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 2 1 10</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">2 4 3 5</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 3 1 5</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">2 4 2 7</span>
 </td><td><p><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">6</span><br /><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Explica?ie: Exista 2 trasee de la 1 la 4:</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 -&gt; 2 -&gt; 4 cu [3,5]</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 -&gt; 2 -&gt; 4 cu [2,7]</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Deci K = 6 ce corespunde celei de-a doua secven?a de
  zboruri. X = {2,3,4,5,6,7}</span></p><p><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';"></span></p>
 
 
 
 
 </td></tr><tr style="height:84.6pt;"><td><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">5 6</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 2 1 10</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">2 5 11 20</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 4 2 5</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1 3 10 11</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">3 4 12 10000</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">4 5 6 6</span>
 </td><td><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">FARA CADOURI</span><br /><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Explica?ie:</span><br /><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Mo?ul nu poate ajunge pe norul 5 deoarece pentru a
  ajunge pe norul 4 are nevoie de </span><span style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">[2,5] </span><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">reni iar pentru a zbura de pe 4 pe 5 are nevoie de </span><span style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">[6,6] </span><span lang="RO" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">reni, iar intersectia acestor intervale este mul?imea
  vida.</span>
 </td></tr></tbody></table><p></p>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (48, 8, 1, N'3-sir', 200, 32, N'<span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Calibri'',''sans-serif'';">Maria
i-a scris deja Mo?ului ca vrea sa primeasca &icirc;n dar o bra?ara&nbsp; &rdquo;frumoasa&rdquo; alcatuita din perle colorate. Ea
considera o bra?ara frumoasa, daca bra?ara este alcatuita din maxim trei culori
( ro?u, galben ?i verde ), dar ea nu se mul?ume?te cu at&icirc;t. Maria mai vrea ca
&icirc;n bra?ara ei sa nu se intilneasca o perla ro?ie vecina cu una galbena, mai
pu?in prima ?i ultima perla, care sunt despar?ite de o laca?ica. Maria este &icirc;nca
mica ?i curioasa de multe lucruri pe care &icirc;nca nu le poate afla ?i care de
multe ori sunt o provocare chiar ?i pentru un elev de clasa 11-12 </span><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:Wingdings;">J</span><span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Calibri'',''sans-serif'';">. De aceasta data ea se intreaba c&icirc;te bra?ari frumoase
distincte de lungime N &icirc;i poate aduce
Mo?ul. Aceasta &icirc;nsa nu este o intrebare simpla ?i desigur Maria va cere
ajutorul vostru, iar in schimb va va rasplati cu 100 de puncte la ?VO-ul de
astazi. Deoarece acest numar poate fi foarte mare , ea se multumeste sa-i
spune?i rezultatul modulo P.</span>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">&Icirc;n fi?ierul 3-sir.in se afla pe prima linie un numar T,
reprezent&acirc;nd numarul de &icirc;ntrebari pe care vi le pune Maria. Pe urmatoarele T linii
se afla c&acirc;te doua numere N ?i P, cu semnifica?ia din enun?.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">&Icirc;n fi?ierul 3-sir.out ve?i afi?a T linii. Pe linia i se
va gasi raspunsul la &icirc;ntrebarea i.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">1&lt;=T&lt;=30;&nbsp;&nbsp;
1&lt;=N, P&lt;=1&nbsp;000&nbsp;000&nbsp;000;</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;">Timpul de execu?ie nu va
depa?i 0.2 sec. pe test;</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;">Pentru 10% din teste N&lt;=10, iar pentru 50% din teste
N&lt;=1&nbsp;000&nbsp;000;</span></p>', N'<p><strong><em></em></strong></p><table><colgroup><col style="width:319px;" /><col style="width:319px;" /></colgroup><thead><tr><th style="text-align:left;"><span lang="RO" style="font-size:14.0pt;">3-sir.in</span>
 </th><th style="text-align:left;"><span lang="RO" style="font-size:14.0pt;">3-sir.out</span>
 </th></tr></thead><tbody><tr><td><span lang="RO" style="font-size:14.0pt;">4<br />1 997<br />4 997<br />21 997<br />999999999 13</span>
 </td><td><span lang="RO" style="font-size:14.0pt;">3<br />41<br />22<br />5</span>
 </td></tr></tbody></table><p></p>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (49, 8, 1, N'forum', 100, 32, N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">Se apropie frumoasele sarbatori
de iarna, dar mai ales Craciunul, pe care to?i &icirc;l a?teptam cu nerabdare. Maria se
g&acirc;nde?te ce sa-i scrie Mo?ului ?i nu se poate decide a?a ca s-a g&acirc;ndit sa
creeze un forum al &rdquo;copiilor cumin?i&rdquo; unde fiecare copil va spune ce vrea sa-i
aduca Mo?ul, pentru ca nu cumva sa ceara ceva ce vrea ?i alt copil (ea vrea ca
cadoul ei sa fie unic). Pentru a crea acest forum ea trebuie sa faca mai &icirc;ntii
sistemul de &icirc;nregistrare al utilizatorilor. Chiar daca este &icirc;nca mica ea ?tie cum
sa faca acest lucru ?i vrea sa vada daca ?ti?i ?i voi. </span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;">&nbsp;Sistemul de
&icirc;nregistrare prime?te o comanda ?i &icirc;n fun?ie de comanda primita returneaza un mesaj
?i anume:</span></p><ol><li><span lang="RO" style="font-size:14.0pt;line-height:115%;">Pentru
comanda </span><span lang="RO" style="font-size:13.5pt;line-height:115%;">&laquo;<em>register
username password</em>&raquo; &icirc;nregistreaza utilizatorul cu numele <em>username</em> ?i parola <em>password</em>. Daca utilizatorul cu numele respectiv exista deja se returneaza mesajul &laquo;<em>fail: user already exists</em>&raquo;, altfel se
returneaza&nbsp; &laquo;<em>success: new user added</em>&raquo;</span></li><li><span lang="RO" style="font-size:14.0pt;line-height:115%;">Pentru
comanda </span><span lang="RO" style="font-size:13.5pt;line-height:115%;">&laquo;<em>login
username password</em>&raquo; intra pe forum cu numele <em>username</em> ?i parola <em>password</em>.
Daca utilizatorul cu numele respectiv nu exista se afi?eaza &laquo;<em>fail: no such user</em>&raquo;, daca utilizatorul
exista, dar a introdus parola gre?it se returneaza &laquo;<em>fail: incorrect password</em>&raquo;, daca utilizatorul este deja logat se
returneaza &laquo;<em>fail: already logged in</em>&raquo;,altfel
se returneaza &laquo;<em>success: user logged in</em>&raquo;</span></li><li><span lang="RO" style="font-size:13.5pt;line-height:115%;">Pentru comanda &laquo;<em>logout username</em>&raquo;
delogheaza utilizatorul cu numele <em>username</em>.
Daca utilizatorul nu exista se returneaza &laquo;<em>fail:
no such user</em>&raquo;, daca utilizatorul este deja delogat se returneaza &laquo;<em>fail: already logged out</em>&raquo;, altfel se
returneaza &laquo;<em>success: user logged out</em>&raquo;.</span></li></ol>', N'<span lang="RO" style="font-size:14.0pt;line-height:115%;font-family:''Calibri'',''sans-serif'';">Pe
prima linie a fi?ierului de intrare <strong><em>forum.in</em></strong> se gase?te numarul N, care
reprezinta numarul de opera?ii. Pe urmatoarele n linii se va gasi c&acirc;te o
opera?ie pe linie &icirc;n formatul descris mai sus.</span>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">Fi?ierul <strong><em>forum.out</em></strong> va con?ine N linii. Pe
linia <em>i</em> ve?i scrie mesajul returnat
la executarea opera?iei <em>i</em> din
fi?ierul de intrare.</span></p>', N'<p><span lang="RO" style="font-size:14.0pt;line-height:115%;">N&lt;=200; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Caracterele
utilizate pentru nume ?i parola vor avea codul ASCII &icirc;ntre 33 ?i 130;</span></p><p><span lang="RO" style="font-size:14.0pt;line-height:115%;">Timpul
de execu?ie nu va depa?i 0.1 sec.</span></p>', N'<p><strong><em></em></strong></p><table><colgroup><col style="width:319px;" /><col style="width:319px;" /></colgroup><thead><tr><th><span lang="RO" style="font-size:14.0pt;">forum.in</span>
 </th><th><span lang="RO" style="font-size:14.0pt;">forum.out</span>
 </th></tr></thead><tbody><tr><td><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">6</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">register
 vasya 12345</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">login
 vasya 1234</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">login
 vasya 12345</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">login
 anakin C-3PO</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">logout
 vasya</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">logout
 vasya</span><br /></td><td><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">success:
 new user added</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">fail:
 incorrect password</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">success:
 user logged in</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">fail:
 no such user</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">success:
 user logged out</span><br /><span lang="RO" style="font-size:14pt;font-family:''Courier New'';">fail:
 already logged out</span><br /></td></tr></tbody></table><p></p><p></p>')
INSERT [dbo].[Problem] ([ID], [Id_Contest], [Id_Admin], [Name], [TimeLimit], [MemoryLimit], [Body], [Input], [Output], [Restrictions], [Examples]) VALUES (50, 8, 1, N'dilema', 200, 16, N'<p><span style="font-size:14.0pt;line-height:115%;">Mo?ul, fiind ?i el curios de dorin?ele copiilor care &icirc;nca nu i-au trimis scrisori, s-a &icirc;nregistrat pe forumul Mariei. Dupa ce a citit unele postari nu a ramas prea &icirc;nc&icirc;ntat, deoarece unii copii doresc cadouri prea mari care abia de &icirc;ncap &icirc;n sanie. Astfel, Mo?ul nu poate duce dec&acirc;t un singur cadou mare la un moment dat de timp ?i respectiv ar pierde prea mult timp cu transportarea acestora. Din acest motiv el lasa pe seama spiridu?ilor transportarea cadourilor mari utiliz&acirc;nd sania de rezerva. Spiridu?ii cunosc pentru fiecare copil timpul <em>d<sub>i</sub></em><sub> &nbsp;</sub>c&acirc;nd acesta doarme ?i timpul <em>t<sub>i</sub> </em>necesar pentru a ajunge din baza la casa copilului respectiv. Cu alte cuvinte daca ei decid sa duca cadoul copilului I, ar trebui sa porneasca din baza &icirc;n momentul d<sub>i</sub>-t<sub>i</sub> pentru ca la momentul d<sub>i</sub> c&acirc;nd copilul doarme ei sa fie deja pe acoperi?ul casei sale pentru a lasa cadoul ?i a ram&icirc;ne neobserva?i. Analiz&acirc;nd informa?iile fiecarui copil spiridu?ii au realizat ca unii copii pot sa ram&acirc;na fara cadouri. Voi trebuie sa le spune?i spiridu?ilor care este numarul minim de cadouri pe care nu le pot transporta &icirc;n noaptea de Craciun pentru ca ei sa raporteze Mo?ului c&acirc;?i copii risca sa ram&acirc;na fara cadouri.</span></p>', N'<p><span style="font-size:14.0pt;line-height:115%;">Pe prima linie a fi?ierului de intrare <strong><em>dilema.in</em></strong> se afla numarul N, de copii care doresc cadouri mari. Pe linia 2 se afla N numere separate prin c&acirc;te un spa?iu reprezent&acirc;nd timpul c&acirc;nd doarme fiecare copil. Pe linia 3 la fel se afla N numere separate prin spa?iu care semnifica c&acirc;t timp dureaza drumul p&acirc;na la fiecare copil.</span></p>', N'<p><span style="font-size:14.0pt;line-height:115%;">Pe prima linie a fi?ierului de ie?ire <strong><em>dilema.out</em></strong> ve?i afi?a numarul minim de copii care risca sa ram&acirc;na fara un cadou mare.</span></p>', N'<p><strong><em></em></strong></p><ul><li><span lang="PT-BR" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">1&le;N&le;1000; 1&le;</span><span lang="IT" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">d<sub>1</sub>,d<sub>2</sub>,...,d<sub>N</sub></span><span lang="PT-BR" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">&le;5000; 1&le;</span><span lang="IT" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">t<sub>1</sub>,t<sub>2</sub>,...,t<sub>N</sub></span><span lang="PT-BR" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">&le;2500</span></li><li><span lang="IT" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Numerele d<sub>1</sub>,d<sub>2</sub>,...,d<sub>N</sub> sunt distincte doua c&acirc;te doua</span><span lang="PT-BR" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">.</span></li><li><span lang="PT-BR" style="font-size:14.0pt;font-family:''Times New Roman'',''serif'';">Timpul de execu?ie nu va depa?i 0.2 sec. pe test.&nbsp;</span></li></ul>', N'<p><strong><em></em></strong></p><table><colgroup><col style="width:319px;" /><col style="width:319px;" /></colgroup><thead><tr><th><span lang="RO" style="font-size:small;">dilema.in</span> </th><th><span lang="RO" style="font-size:small;">dilema.out</span> </th></tr></thead><tbody><tr><td><span lang="IT" style="font-family:''Courier New'';font-size:small;">7</span><br /><span lang="IT" style="font-family:''Courier New'';font-size:small;">27 9 28 37 3 54 50</span><br /><span lang="IT" style="font-family:''Courier New'';font-size:small;">1 5 5 4 5 2 2</span> </td><td><span lang="RO" style="font-size:small;">3</span> </td></tr></tbody></table><p></p>')
SET IDENTITY_INSERT [dbo].[Problem] OFF
SET IDENTITY_INSERT [dbo].[ProblemCategory] ON 

INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (1, N'Greedy', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (2, N'Dynamic Programming', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (3, N'Graphs', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (4, N'Trees', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (5, N'Divide et Impera', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (6, N'Implementation', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (7, N'Recursion', NULL)
INSERT [dbo].[ProblemCategory] ([ID], [Name], [Description]) VALUES (8, N'Backtracking', NULL)
SET IDENTITY_INSERT [dbo].[ProblemCategory] OFF
SET IDENTITY_INSERT [dbo].[ProgrammingLanguage] ON 

INSERT [dbo].[ProgrammingLanguage] ([ID], [Name], [AcceptedSourcesExtensions]) VALUES (1, N'C++', N'.cpp')
INSERT [dbo].[ProgrammingLanguage] ([ID], [Name], [AcceptedSourcesExtensions]) VALUES (2, N'C', N'.c')
INSERT [dbo].[ProgrammingLanguage] ([ID], [Name], [AcceptedSourcesExtensions]) VALUES (3, N'Pascal', N'.pas')
SET IDENTITY_INSERT [dbo].[ProgrammingLanguage] OFF
SET IDENTITY_INSERT [dbo].[Question] ON 

INSERT [dbo].[Question] ([ID], [ID_Contestant], [ID_Admin], [Question], [Answer], [Answered]) VALUES (1, 1, 1, N'Unde pot gasi documentatia Pascal?', N'http://www.freepascal.org/docs-html/prog/prog.html', 1)
INSERT [dbo].[Question] ([ID], [ID_Contestant], [ID_Admin], [Question], [Answer], [Answered]) VALUES (2, 2, NULL, N'Care este limita memorie pentru problema 3-sir?', N'', 0)
INSERT [dbo].[Question] ([ID], [ID_Contestant], [ID_Admin], [Question], [Answer], [Answered]) VALUES (3, 32, 1, N'Care este versiunea compilatorului pentru c++?', N'4.2.2', 1)
INSERT [dbo].[Question] ([ID], [ID_Contestant], [ID_Admin], [Question], [Answer], [Answered]) VALUES (4, 7, 1, N'care este versiunea compilatorului pentru Pascal?', N'7.1.5', 1)
SET IDENTITY_INSERT [dbo].[Question] OFF
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (1, N'OK', N'OK', N'#00FF00')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (2, N'Wrong Answer', N'WA', N'#FF0000')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (3, N'Memory Limit Excedeed', N'MLE', N'#FF0000')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (4, N'Time Limit Excedeed', N'TLE', N'#FF0000')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (5, N'Did Not Compile', N'DNC', N'#FFFF00')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (6, N'Missing Output File', N'MOF', N'#FFFF00')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (7, N'Executed', N'EXE', N'#0000FF')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (8, N'Compiled', N'COM', N'#0000FF')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (9, N'Killed', N'KILL', N'#FF0000')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (11, N'Segmentation Fault', N'SF', N'#FF0000')
INSERT [dbo].[Status] ([ID], [Name], [Code], [Color]) VALUES (12, N'Plagiarized Code', N'PC', N'#FFA500')
SET IDENTITY_INSERT [dbo].[Status] OFF
SET IDENTITY_INSERT [dbo].[Submission] ON 

INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2031, 50, 2, N'a8beb03a-46e3-45ac-8718-969a5a911b5adilema.cpp', CAST(0x0000A4C600AAB563 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2032, 49, 2, N'a85d1b34-4682-4630-8dc6-96d391e7c2afforum.pas', CAST(0x0000A4C600AB43A9 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2033, 48, 2, N'b04d616a-7525-42ba-991b-64fee7e5236a3-sir.cpp', CAST(0x0000A4C600AB7BAD AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2034, 47, 2, N'2403fbf8-c4c7-442e-aa9c-4c3d5b072595mosul.cpp', CAST(0x0000A4C600AB95F4 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2035, 47, 2, N'57620f4d-4012-49b2-b7b2-7710cc2f00d0mosul.cpp', CAST(0x0000A4C600ACEE0E AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2036, 47, 2, N'66ff909a-f1da-4b5a-83b3-cba2d30ab854mosul.cpp', CAST(0x0000A4C600F285E3 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2037, 48, 2, N'3d15215f-705d-44fa-8cb4-00282bc74d913-sir.cpp', CAST(0x0000A4C600F28F6F AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2038, 49, 2, N'6b80bfbc-e885-48e4-a846-b3a0b82b7124forum.cpp', CAST(0x0000A4C600F29A6A AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2039, 50, 2, N'd61d00ab-b8db-4fc6-91fb-77c8e622c8eedilema.cpp', CAST(0x0000A4C600F2A310 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2040, 47, 32, N'32455ad7-6d9b-4d2e-aa9b-ce179c9e95f3mosul.pas', CAST(0x0000A4C600F33AED AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2041, 48, 32, N'7601ca0f-ccd2-410e-9ae0-fcb167fc8dcb3-sir.pas', CAST(0x0000A4C600F34125 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2042, 49, 32, N'19c44757-6cab-490d-9f38-5207132b3f03forum.pas', CAST(0x0000A4C600F3496F AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2043, 50, 32, N'891c686c-e32c-4fb4-80ac-7f0d51ea750ddilema.pas', CAST(0x0000A4C600F35241 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2044, 47, 7, N'1aaf3c27-58de-4065-a427-9c546384dc9fmosul.cpp', CAST(0x0000A4C600F3BCEF AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2045, 48, 7, N'd0cfd3df-7ead-4294-9b22-c31d91d2b9ee3-sir.cpp', CAST(0x0000A4C600F3C76A AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2046, 49, 7, N'40a6516c-b15d-412c-8495-3211520ae3baforum.cpp', CAST(0x0000A4C600F3CDAF AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2047, 50, 7, N'b14a8fa2-2bc3-42bf-ac31-7d0e230a752fdilema.cpp', CAST(0x0000A4C600F3D427 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2048, 47, 33, N'38ae131b-5d10-4aab-b2b0-2f34a06d06b9mosul.cpp', CAST(0x0000A4C600F3FA8D AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2049, 48, 33, N'3b49e4e2-04ed-4d8f-9c1c-6f94590755243-sir.cpp', CAST(0x0000A4C600F402FF AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2050, 49, 33, N'ed885159-ade7-435c-8837-6795c138957aforum.pas', CAST(0x0000A4C600F40A2F AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2051, 50, 33, N'5f309b51-5d56-4cc0-9f52-95f21e3ccb2adilema.cpp', CAST(0x0000A4C600F41ABC AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2052, 49, 34, N'f80d8da5-78b9-48fb-8b58-59881712bdb7forum.cpp', CAST(0x0000A4C600F45FA9 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2053, 50, 34, N'b7acd748-66b8-4c0e-a535-9da4481de0fadilema.cpp', CAST(0x0000A4C600F47354 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2054, 49, 7, N'd8271155-61cc-4585-ac94-5816a848a470forum.cpp', CAST(0x0000A4C6011003D5 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2055, 50, 7, N'e29d0879-1178-4179-bcc3-32580fe62ff2dilema.cpp', CAST(0x0000A4C60110118B AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2056, 50, 7, N'a6df57df-7747-4430-b83a-71b2848f7d86dilema.cpp', CAST(0x0000A4C700C6F993 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2057, 49, 7, N'6b7ddf8e-b212-44a7-b9bd-d14b8223e3f2forum.cpp', CAST(0x0000A4C700C7041E AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2058, 48, 7, N'4e921c1d-17b1-4fb9-9ff1-1643ada7d3ed3-sir.cpp', CAST(0x0000A4C700C70EE5 AS DateTime), 2)
INSERT [dbo].[Submission] ([ID], [ID_Problem], [ID_Contestant], [FileName], [SubmissionTime], [Status]) VALUES (2059, 47, 7, N'89c6ccb5-8b7d-408a-8eb4-f6207686c00amosul.cpp', CAST(0x0000A4C700C7158C AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Submission] OFF
SET IDENTITY_INSERT [dbo].[SubmissionResult] ON 

INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1275, 2033, 517, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1276, 2033, 518, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1277, 2033, 519, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1278, 2033, 520, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1279, 2033, 521, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1280, 2033, 522, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1281, 2033, 523, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1282, 2033, 524, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1283, 2033, 525, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1284, 2033, 526, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1285, 2032, 527, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1286, 2032, 528, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.17 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1287, 2032, 529, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1288, 2032, 530, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1289, 2032, 531, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.08 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1290, 2032, 532, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1291, 2032, 533, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1292, 2032, 534, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1293, 2032, 535, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.16 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1294, 2032, 536, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1295, 2031, 537, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1296, 2031, 538, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1297, 2031, 539, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1298, 2031, 540, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1299, 2031, 541, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1300, 2031, 542, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1301, 2031, 543, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1302, 2031, 544, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1303, 2031, 545, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1304, 2031, 546, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1305, 2035, 507, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1306, 2035, 508, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1307, 2035, 509, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1308, 2035, 510, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1309, 2035, 511, 4, 0, CAST(562.50 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1310, 2035, 512, 4, 0, CAST(468.75 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1311, 2035, 513, 4, 0, CAST(562.50 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1312, 2035, 514, 4, 0, CAST(546.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1313, 2035, 515, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.76 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1314, 2035, 516, 4, 0, CAST(515.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1315, 2034, 507, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1316, 2034, 508, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1317, 2034, 509, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1318, 2034, 510, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1319, 2034, 511, 4, 0, CAST(500.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1320, 2034, 512, 4, 0, CAST(500.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1321, 2034, 513, 4, 0, CAST(531.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1322, 2034, 514, 4, 0, CAST(437.50 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1323, 2034, 515, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1324, 2034, 516, 4, 0, CAST(531.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1325, 2048, 507, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1326, 2048, 508, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(0.64 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1327, 2048, 509, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1328, 2048, 510, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1329, 2048, 511, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1330, 2048, 512, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1331, 2048, 513, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1332, 2048, 514, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1333, 2048, 515, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1334, 2048, 516, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1335, 2044, 507, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1336, 2044, 508, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1337, 2044, 509, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1338, 2044, 510, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.24 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1339, 2044, 511, 4, 0, CAST(484.37 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1340, 2044, 512, 4, 0, CAST(453.12 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1341, 2044, 513, 4, 0, CAST(421.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1342, 2044, 514, 4, 0, CAST(500.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1343, 2044, 515, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1344, 2044, 516, 4, 0, CAST(515.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1345, 2040, 507, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1346, 2040, 508, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1347, 2040, 509, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1348, 2040, 510, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1349, 2040, 511, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1350, 2040, 512, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1351, 2040, 513, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1352, 2040, 514, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1353, 2040, 515, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1354, 2040, 516, 5, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1355, 2036, 507, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1356, 2036, 508, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1357, 2036, 509, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1358, 2036, 510, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1359, 2036, 511, 1, 10, CAST(78.12 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1360, 2036, 512, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1361, 2036, 513, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1362, 2036, 514, 1, 10, CAST(156.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1363, 2036, 515, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1364, 2036, 516, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1365, 2049, 517, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1366, 2049, 518, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1367, 2049, 519, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1368, 2049, 520, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1369, 2049, 521, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1370, 2049, 522, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1371, 2049, 523, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1372, 2049, 524, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1373, 2049, 525, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
GO
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1374, 2049, 526, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1375, 2045, 517, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1376, 2045, 518, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1377, 2045, 519, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1378, 2045, 520, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1379, 2045, 521, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1380, 2045, 522, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1381, 2045, 523, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1382, 2045, 524, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1383, 2045, 525, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1384, 2045, 526, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1385, 2041, 517, 4, 0, CAST(328.12 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1386, 2041, 518, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.56 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1387, 2041, 519, 1, 10, CAST(62.50 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1388, 2041, 520, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1389, 2041, 521, 1, 10, CAST(156.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1390, 2041, 522, 4, 0, CAST(312.50 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1391, 2041, 523, 4, 0, CAST(234.37 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1392, 2041, 524, 4, 0, CAST(328.12 AS Decimal(18, 2)), CAST(1.32 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1393, 2041, 525, 4, 0, CAST(234.37 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1394, 2041, 526, 4, 0, CAST(328.12 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1395, 2037, 517, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1396, 2037, 518, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1397, 2037, 519, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1398, 2037, 520, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1399, 2037, 521, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1400, 2037, 522, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1401, 2037, 523, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1402, 2037, 524, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1403, 2037, 525, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1404, 2037, 526, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1405, 2052, 527, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1406, 2052, 528, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1407, 2052, 529, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1408, 2052, 530, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1409, 2052, 531, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1410, 2052, 532, 2, 0, CAST(46.87 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1411, 2052, 533, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1412, 2052, 534, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1413, 2052, 535, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1414, 2052, 536, 4, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1415, 2050, 527, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1416, 2050, 528, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1417, 2050, 529, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1418, 2050, 530, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1419, 2050, 531, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1420, 2050, 532, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1421, 2050, 533, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1422, 2050, 534, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1423, 2050, 535, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1424, 2050, 536, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1425, 2046, 527, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1426, 2046, 528, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1427, 2046, 529, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.57 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1428, 2046, 530, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1429, 2046, 531, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1430, 2046, 532, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1431, 2046, 533, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1432, 2046, 534, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1433, 2046, 535, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1434, 2046, 536, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1435, 2042, 527, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1436, 2042, 528, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1437, 2042, 529, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.15 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1438, 2042, 530, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1439, 2042, 531, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1440, 2042, 532, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1441, 2042, 533, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1442, 2042, 534, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1443, 2042, 535, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1444, 2042, 536, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1445, 2038, 527, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1446, 2038, 528, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1447, 2038, 529, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.07 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1448, 2038, 530, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1449, 2038, 531, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1450, 2038, 532, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1451, 2038, 533, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1452, 2038, 534, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1453, 2038, 535, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1454, 2038, 536, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1455, 2053, 537, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1456, 2053, 538, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1457, 2053, 539, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1458, 2053, 540, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1459, 2053, 541, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1460, 2053, 542, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1461, 2053, 543, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1462, 2053, 544, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1463, 2053, 545, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1464, 2053, 546, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CE37EF AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1465, 2051, 537, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1466, 2051, 538, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1467, 2051, 539, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1468, 2051, 540, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1469, 2051, 541, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1470, 2051, 542, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1471, 2051, 543, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1472, 2051, 544, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1473, 2051, 545, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
GO
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1474, 2051, 546, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1475, 2047, 537, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1476, 2047, 538, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1477, 2047, 539, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1478, 2047, 540, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1479, 2047, 541, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1480, 2047, 542, 2, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1481, 2047, 543, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1482, 2047, 544, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1483, 2047, 545, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1484, 2047, 546, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600FAD3C1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1485, 2043, 537, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1486, 2043, 538, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.33 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1487, 2043, 539, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1488, 2043, 540, 2, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1489, 2043, 541, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1490, 2043, 542, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1491, 2043, 543, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1492, 2043, 544, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1493, 2043, 545, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1494, 2043, 546, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.51 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1495, 2039, 537, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1496, 2039, 538, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1497, 2039, 539, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1498, 2039, 540, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1499, 2039, 541, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1500, 2039, 542, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1501, 2039, 543, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1502, 2039, 544, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1503, 2039, 545, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1504, 2039, 546, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C600F7C2FD AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1505, 2054, 527, 4, 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1506, 2054, 528, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1507, 2054, 529, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1508, 2054, 530, 4, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1509, 2054, 531, 4, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1510, 2054, 532, 4, 0, CAST(46.87 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1511, 2054, 533, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1512, 2054, 534, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1513, 2054, 535, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1514, 2054, 536, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1515, 2055, 537, 1, 10, CAST(62.50 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1516, 2055, 538, 1, 10, CAST(46.87 AS Decimal(18, 2)), CAST(0.82 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1517, 2055, 539, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(0.78 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1518, 2055, 540, 2, 0, CAST(46.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1519, 2055, 541, 4, 0, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1520, 2055, 542, 4, 0, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1521, 2055, 543, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1522, 2055, 544, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1523, 2055, 545, 4, 0, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1524, 2055, 546, 1, 10, CAST(78.12 AS Decimal(18, 2)), CAST(0.82 AS Decimal(18, 2)), CAST(0x0000A4C601106FD1 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1525, 2059, 507, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1526, 2059, 508, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1527, 2059, 509, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1528, 2059, 510, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1529, 2059, 511, 4, 0, CAST(296.87 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1530, 2059, 512, 4, 0, CAST(234.37 AS Decimal(18, 2)), CAST(0.44 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1531, 2059, 513, 4, 0, CAST(218.75 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1532, 2059, 514, 4, 0, CAST(93.75 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1533, 2059, 515, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1534, 2059, 516, 4, 0, CAST(218.75 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1535, 2058, 517, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1536, 2058, 518, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1537, 2058, 519, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1538, 2058, 520, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1539, 2058, 521, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1540, 2058, 522, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1541, 2058, 523, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1542, 2058, 524, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1543, 2058, 525, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1544, 2058, 526, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1545, 2057, 527, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.91 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1546, 2057, 528, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1547, 2057, 529, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1548, 2057, 530, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1549, 2057, 531, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1550, 2057, 532, 4, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1551, 2057, 533, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1552, 2057, 534, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1553, 2057, 535, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1554, 2057, 536, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1555, 2056, 537, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.07 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1556, 2056, 538, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1557, 2056, 539, 1, 10, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1558, 2056, 540, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1559, 2056, 541, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(0.06 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1560, 2056, 542, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1561, 2056, 543, 2, 0, CAST(15.62 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1562, 2056, 544, 1, 10, CAST(0.00 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1563, 2056, 545, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
INSERT [dbo].[SubmissionResult] ([ID], [ID_Submission], [ID_Test], [ID_Status], [Score], [Time], [Memory], [EvaluationTime]) VALUES (1564, 2056, 546, 1, 10, CAST(31.25 AS Decimal(18, 2)), CAST(1.10 AS Decimal(18, 2)), CAST(0x0000A4C700CEE415 AS DateTime))
SET IDENTITY_INSERT [dbo].[SubmissionResult] OFF
SET IDENTITY_INSERT [dbo].[Test] ON 

INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (387, 36, N'joc0', N'joc0-ea5eebf6-f220-41cb-92a0-065acbc3496a.in', N'joc0-93dd26bb-3b65-4498-95d7-ed404bb7de94.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (388, 36, N'joc1', N'joc1-fe5bf53a-5928-4f0f-ba81-63ce13857a8c.in', N'joc1-b3e8bdb3-f372-4ea9-9284-a21f95dd4fbb.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (389, 36, N'joc2', N'joc2-b99815f4-fbfc-4dfb-9fe8-c6724a8277f4.in', N'joc2-4ec96fe4-9fa6-42a2-881b-e248d4cb88ce.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (390, 36, N'joc3', N'joc3-c535f18d-2ebd-449e-8084-29b0c1a62ca3.in', N'joc3-c959f4eb-ea5b-4f36-952f-9685ed35afc0.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (391, 36, N'joc4', N'joc4-d132dc32-68bf-4d98-ada8-81eff3304654.in', N'joc4-bc6ebdc9-ad04-4bef-adba-47e6de9ae947.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (392, 36, N'joc5', N'joc5-625c14de-f349-4a4d-a5bd-8717bc4d7cb9.in', N'joc5-8a2e642f-0201-4bd3-9196-2810dfb0947c.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (393, 36, N'joc6', N'joc6-33c486dd-b965-4ee7-9a11-58bcb05b3212.in', N'joc6-a2322261-a226-4b35-b05c-20b8dae0c7db.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (394, 36, N'joc7', N'joc7-df55ab38-81b0-4c68-b520-337ce23f92e2.in', N'joc7-24172828-1ba4-4fa8-a842-270a44dded5c.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (395, 36, N'joc8', N'joc8-8ea490e1-8ecb-4395-8c94-9e231fa44f4d.in', N'joc8-80ba8280-30a9-492f-98ce-6348ca7336dd.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (396, 36, N'joc9', N'joc9-e6b938bc-9aa3-4624-b077-028170d0991f.in', N'joc9-4032b9f1-5e1f-40c0-92ba-7766b0d43729.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (407, 38, N'mijloc0', N'mijloc0-54c251a8-5d02-46a4-8fe0-212fa33280e5.in', N'mijloc0-d0729ff8-9659-4d13-8bad-7c07c2201f64.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (408, 38, N'mijloc1', N'mijloc1-92f8d508-488e-44a0-af85-589f3eeb5aad.in', N'mijloc1-fc416e07-fe46-46c0-97b9-b4a735ca09b2.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (409, 38, N'mijloc2', N'mijloc2-afec0291-8cab-4ca4-a8be-5f6ab478814a.in', N'mijloc2-d93d74a1-b84e-4147-a25f-6b27b52945a9.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (410, 38, N'mijloc3', N'mijloc3-e1c1725f-0bdc-4311-86ab-b2b50f1fb2f8.in', N'mijloc3-d96e8fe9-28ff-41af-9475-5367a2a77064.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (411, 38, N'mijloc4', N'mijloc4-a16ce54e-d783-4f9b-8b4e-4ef67643de7e.in', N'mijloc4-a0db9957-0533-474f-b5dc-a48ffd3c406e.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (412, 38, N'mijloc5', N'mijloc5-1f525f1c-07c4-4ec0-84c8-a0d4e71e3b16.in', N'mijloc5-a3b0a2e4-ce6b-48df-8336-0af0d10a04c0.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (413, 38, N'mijloc6', N'mijloc6-6c6945ec-6510-4584-adcc-9acc147a86e7.in', N'mijloc6-333af8d5-e593-4503-af1d-ddd44e5a49ed.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (414, 38, N'mijloc7', N'mijloc7-e212ee80-40e8-49ed-b3c3-e1fc7918327e.in', N'mijloc7-17e5e26f-9f26-40c2-b738-f684c43f6480.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (415, 38, N'mijloc8', N'mijloc8-8bb977a7-d25b-4fe5-baed-a7caadf5a2c5.in', N'mijloc8-95228050-394c-4f8a-bda8-15eda1060123.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (416, 38, N'mijloc9', N'mijloc9-c77e0f7a-c535-4c63-be25-2c551b051698.in', N'mijloc9-02c9c625-b8ec-451b-af7c-5e96192a0812.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (417, 39, N'depasiri0', N'depasiri0-fd0e52b1-48a4-4521-9cfc-79b3a60154c2.in', N'depasiri0-74247155-37c4-4d21-ae02-b3c3a81aa9f6.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (418, 39, N'depasiri1', N'depasiri1-a76efc4d-5f9a-4f4a-a008-203b012d0031.in', N'depasiri1-08870c5f-1248-4004-aad2-35f1bea8b034.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (419, 39, N'depasiri2', N'depasiri2-c65af19c-4d91-4959-9157-e29190a9635b.in', N'depasiri2-efee3943-40e1-416e-8425-79e0d7b37f14.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (420, 39, N'depasiri3', N'depasiri3-0491d761-b933-46f8-af13-f04b413a423d.in', N'depasiri3-bcb73470-b647-44ae-8902-fa74c59b94f0.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (421, 39, N'depasiri4', N'depasiri4-525dce3f-f730-42a2-846c-16aa1dc1041d.in', N'depasiri4-f01f7619-d440-4709-b571-09d70baf1f1e.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (422, 39, N'depasiri5', N'depasiri5-bbc6804e-c73b-4afd-add2-1d66b5a40e81.in', N'depasiri5-e88d0b11-9bce-4cad-b8c1-d8f5e2be4c74.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (423, 39, N'depasiri6', N'depasiri6-35eaa006-b62f-445f-8e4d-28a8ac0060eb.in', N'depasiri6-4d7ab6a3-e4b5-4ac6-b1b4-60c93d7fa68c.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (424, 39, N'depasiri7', N'depasiri7-08b9061d-ff7d-4e6e-992e-53b0ebdc3b02.in', N'depasiri7-bbaeddca-16e3-4cad-ae98-06e12ad7d5da.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (425, 39, N'depasiri8', N'depasiri8-124ba483-4694-4f08-9c5b-d0f138941eb5.in', N'depasiri8-352c3b76-fc7a-4558-b63a-1e5f065317f3.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (426, 39, N'depasiri9', N'depasiri9-62e82a28-3b1a-482f-825e-7ee2ebc38f83.in', N'depasiri9-05bb9d91-f3f6-4e3d-9368-57b7c5440c4d.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (427, 37, N'piloti0', N'piloti0-a6ac16bf-4e5c-4919-8ab2-1aa6c7fdd0de.in', N'piloti0-79694dde-96d1-45da-acf1-e45d4cff1177.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (428, 37, N'piloti1', N'piloti1-57017168-6991-4076-959c-8cdf09336d8a.in', N'piloti1-71117e5a-1968-4975-afac-8c7a535a9f1a.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (429, 37, N'piloti2', N'piloti2-34ae63ca-cf63-4bc5-8c1b-60ae4d166caa.in', N'piloti2-4f2fe311-1258-47f6-a239-fcad31819d41.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (430, 37, N'piloti3', N'piloti3-a6e77772-cfbb-44a5-bef3-2ec29fef88f1.in', N'piloti3-508b6a27-73e2-4c92-8cc0-abc1820d40d9.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (431, 37, N'piloti4', N'piloti4-196cbee9-1a80-4779-bcba-fafd1d936b75.in', N'piloti4-c016ae41-4ff2-4589-971b-2cf6c75ec31f.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (432, 37, N'piloti5', N'piloti5-a9ca20cc-84f0-47b7-bbc1-ceee57c3df34.in', N'piloti5-10da140b-77e8-4dee-820b-7924097f18ca.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (433, 37, N'piloti6', N'piloti6-01210561-48b0-45ec-b86b-04589f50be8d.in', N'piloti6-7642eacd-d80c-4cdb-a42b-a352728447b8.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (434, 37, N'piloti7', N'piloti7-9ddd26f9-dd50-4a77-b362-077bbe884ef0.in', N'piloti7-f72c73c3-68df-4857-92e4-dea3282a1390.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (435, 37, N'piloti8', N'piloti8-9cd4ef46-6661-4b35-b261-d31dcfedacb9.in', N'piloti8-c9473f35-663e-494b-861f-c6f191187095.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (436, 37, N'piloti9', N'piloti9-e413ebb8-ec11-44be-98e1-596452adaa01.in', N'piloti9-28c97175-4c50-4e67-b4b1-3732e0b0bf3e.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (507, 47, N'mosul0', N'mosul0-d0dbb7b9-370a-48a8-a894-a36f8ccc1890.in', N'mosul0-50568fb8-b8c8-4894-bcc2-9959332e18c4.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (508, 47, N'mosul1', N'mosul1-cb58befc-2293-4198-a39a-65d9bbb9823d.in', N'mosul1-49b2586f-ec51-4b6d-a67a-16cb23c57566.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (509, 47, N'mosul2', N'mosul2-255db710-410b-4a95-a40e-1b09c32d5be7.in', N'mosul2-7d9397f0-1003-4cb3-a32f-e7e52254f473.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (510, 47, N'mosul3', N'mosul3-f8cb2b40-6dd6-4a11-afb6-fbc22e014148.in', N'mosul3-a22fc4cd-8ccb-4d9b-8d78-3ad832ad10c6.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (511, 47, N'mosul4', N'mosul4-f1213414-b1dc-421e-a7ec-5b0269541e7d.in', N'mosul4-6307d82f-844b-40d9-b2b2-14b19e2add46.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (512, 47, N'mosul5', N'mosul5-b35ff428-a62c-423f-997a-3ad1c1ece3a7.in', N'mosul5-1eeb307c-3a56-4830-9178-15cba0f6a75f.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (513, 47, N'mosul6', N'mosul6-3401322b-c7df-4abe-be68-23059bb6723c.in', N'mosul6-0ad61f47-9399-4712-863c-d7b67f6c5aa7.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (514, 47, N'mosul7', N'mosul7-e7b2c388-d51d-4150-97af-ee7049fe08dd.in', N'mosul7-c8edd606-92fe-4e19-829b-d50dc067c1c6.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (515, 47, N'mosul8', N'mosul8-98eca918-b77a-408c-abca-25ab43ba3c01.in', N'mosul8-a1bcfa13-6aaa-403d-a244-5c98fc6b8216.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (516, 47, N'mosul9', N'mosul9-4cfbcafa-db69-4e35-b844-dfb1393f39e5.in', N'mosul9-911393cf-bc17-4eff-85cd-f177d797d7b3.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (517, 48, N'3-sir0', N'3-sir0-c09b5e7a-6c29-4ecc-97be-2461eb0e25e3.in', N'3-sir0-faeb1ff6-333d-47aa-9edf-4bb25fdf89d9.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (518, 48, N'3-sir1', N'3-sir1-0e86ee5b-d683-462d-a95d-aefd6766e880.in', N'3-sir1-0c49de34-1582-45cb-bd89-51ad2c449ff8.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (519, 48, N'3-sir2', N'3-sir2-b52e422c-ee43-4b8d-95db-cd7d2668e3b1.in', N'3-sir2-b2a28e24-e048-452c-a7e0-9443147040aa.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (520, 48, N'3-sir3', N'3-sir3-f8dad1d2-a5fe-468f-997e-c27537aa91db.in', N'3-sir3-19522b33-ee75-409b-b40c-f17655ba47ee.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (521, 48, N'3-sir4', N'3-sir4-2cafe783-7b7b-4250-84c8-71e15e5865c2.in', N'3-sir4-dd21e88c-ef97-48de-acd9-a0168b96e237.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (522, 48, N'3-sir5', N'3-sir5-5fb0e63c-5be2-4ed2-80aa-ebb1e6d3b2a4.in', N'3-sir5-24506685-9c0e-4cc4-aee9-a4837f12b6f8.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (523, 48, N'3-sir6', N'3-sir6-a213c18d-5349-44ae-bd72-a4cecde8e8b8.in', N'3-sir6-caef94c1-2cfe-4cc4-ad79-b8534c5e0552.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (524, 48, N'3-sir7', N'3-sir7-563c653e-c0d3-462c-b4e2-84c302194301.in', N'3-sir7-11b9936c-9fa6-49dc-a37f-b41a77f163bd.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (525, 48, N'3-sir8', N'3-sir8-05940ea9-0964-49fd-bdc9-959ef3b6aca9.in', N'3-sir8-f89b654f-556c-4d06-938b-531a07c42b39.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (526, 48, N'3-sir9', N'3-sir9-81b93e60-dd52-4647-86ad-8a664657de29.in', N'3-sir9-b4211f07-8c9d-4b80-b74b-83c15e4ac51f.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (527, 49, N'forum0', N'forum0-36cabc24-de95-4436-8160-c6bc49740091.in', N'forum0-7ccc2b39-c39b-4cf7-bf14-57df112548ff.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (528, 49, N'forum1', N'forum1-d69cf650-02ca-4446-9b45-b93422a49ca8.in', N'forum1-74ed71ab-5065-4ae3-a349-efb800d64ccc.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (529, 49, N'forum2', N'forum2-931ea9dd-3ee8-4a10-8123-9db51705827e.in', N'forum2-1dbe86d9-974b-4e94-ad83-06e0fc79411e.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (530, 49, N'forum3', N'forum3-a1b6cc33-d28a-432f-a4b3-bc2747634289.in', N'forum3-c116de79-866a-4931-82a7-cc3b1edd6ad7.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (531, 49, N'forum4', N'forum4-81334650-62dc-4253-80e4-b17557261001.in', N'forum4-306ff68f-462a-442f-bf10-2ac055d5228a.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (532, 49, N'forum5', N'forum5-1df34110-fc57-4ad5-943f-d233bd4027a5.in', N'forum5-e93b9e68-6ca6-4842-85bf-5f7168de14bb.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (533, 49, N'forum6', N'forum6-579e57ff-0e34-4b16-9349-1f143651a6b1.in', N'forum6-62528043-804f-4d21-b44e-3319e007cdc2.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (534, 49, N'forum7', N'forum7-49fecedc-e2a5-42d4-9b60-09a82e0d1e30.in', N'forum7-4c5b6884-55c3-4c3c-92ed-575c2a7e724a.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (535, 49, N'forum8', N'forum8-b5c6dccf-c032-4c80-909a-16ac8691fc57.in', N'forum8-cd579b7a-8318-4199-8254-30deb589a5f8.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (536, 49, N'forum9', N'forum9-218cb563-03ab-4f8f-a688-6e9a66e605d3.in', N'forum9-c9a0575a-dc4f-4d11-8ac8-1033b94ba867.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (537, 50, N'dilema0', N'dilema0-05678c24-a99d-4870-a26e-2b5de68a5d41.in', N'dilema0-5ac5de75-5966-4cf3-8072-c245ba8deff9.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (538, 50, N'dilema1', N'dilema1-3de7caf1-3ff4-4256-ae94-8f5d4e9fb4df.in', N'dilema1-70586fb9-13f9-4993-9a2a-b1d93ff42c18.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (539, 50, N'dilema2', N'dilema2-04356246-f338-474b-bd56-811b61ba53c6.in', N'dilema2-ee6c828e-e084-4449-a151-c7b8c9e2aaf3.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (540, 50, N'dilema3', N'dilema3-485f28b2-2e07-46d0-b001-882cc8ca5273.in', N'dilema3-67083e9d-98e3-4570-a178-c0172a4d81e0.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (541, 50, N'dilema4', N'dilema4-59d67e50-1305-44f1-8096-a552dd285e7e.in', N'dilema4-92afcc7c-fcb6-4f89-b1e0-12722b4389e4.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (542, 50, N'dilema5', N'dilema5-ec0ea743-d83c-477f-a86e-1ed714af5e71.in', N'dilema5-f0ba2ff4-6937-475c-b809-00353922462b.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (543, 50, N'dilema6', N'dilema6-3e46eeed-6b80-4c1c-b49b-82bc1fee308d.in', N'dilema6-56bb8c73-c65c-4a8b-9836-934fb8660851.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (544, 50, N'dilema7', N'dilema7-c4a0928a-b567-4665-926b-dfafdb51cdcd.in', N'dilema7-6a0332d1-d823-4469-8acc-18812a87bcde.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (545, 50, N'dilema8', N'dilema8-dbc60beb-dd6d-4286-ad42-d827331c4883.in', N'dilema8-4cd7413c-25bc-4db1-a94d-f261ff182aec.ok', 10)
INSERT [dbo].[Test] ([ID], [ID_Problem], [Name], [InputLocation], [OKLocation], [Points]) VALUES (546, 50, N'dilema9', N'dilema9-6570e868-9862-43b7-a408-39f1bcd47f58.in', N'dilema9-cff40b32-5db8-421d-ab0f-a82b362b5b77.ok', 10)
SET IDENTITY_INSERT [dbo].[Test] OFF
ALTER TABLE [dbo].[Contest]  WITH CHECK ADD  CONSTRAINT [FK_Contest_Admin] FOREIGN KEY([Id_Admin])
REFERENCES [dbo].[Admin] ([ID])
GO
ALTER TABLE [dbo].[Contest] CHECK CONSTRAINT [FK_Contest_Admin]
GO
ALTER TABLE [dbo].[Contest_ProgrammingLanguage_Link]  WITH CHECK ADD  CONSTRAINT [FK_Contest_ProgrammingLanguage_Link_Contest] FOREIGN KEY([Id_Contest])
REFERENCES [dbo].[Contest] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Contest_ProgrammingLanguage_Link] CHECK CONSTRAINT [FK_Contest_ProgrammingLanguage_Link_Contest]
GO
ALTER TABLE [dbo].[Contest_ProgrammingLanguage_Link]  WITH CHECK ADD  CONSTRAINT [FK_Contest_ProgrammingLanguage_Link_ProgrammingLanguage] FOREIGN KEY([Id_ProgrammingLanguage])
REFERENCES [dbo].[ProgrammingLanguage] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Contest_ProgrammingLanguage_Link] CHECK CONSTRAINT [FK_Contest_ProgrammingLanguage_Link_ProgrammingLanguage]
GO
ALTER TABLE [dbo].[DefaultPasswords]  WITH CHECK ADD  CONSTRAINT [FK_DefaultPasswords_Contestant] FOREIGN KEY([ContestantId])
REFERENCES [dbo].[Contestant] ([ID])
GO
ALTER TABLE [dbo].[DefaultPasswords] CHECK CONSTRAINT [FK_DefaultPasswords_Contestant]
GO
ALTER TABLE [dbo].[Problem]  WITH CHECK ADD  CONSTRAINT [FK_Problem_Admin] FOREIGN KEY([Id_Admin])
REFERENCES [dbo].[Admin] ([ID])
GO
ALTER TABLE [dbo].[Problem] CHECK CONSTRAINT [FK_Problem_Admin]
GO
ALTER TABLE [dbo].[Problem]  WITH CHECK ADD  CONSTRAINT [FK_Problem_Contest] FOREIGN KEY([Id_Contest])
REFERENCES [dbo].[Contest] ([ID])
GO
ALTER TABLE [dbo].[Problem] CHECK CONSTRAINT [FK_Problem_Contest]
GO
ALTER TABLE [dbo].[Problem_ProblemCategory_Link]  WITH CHECK ADD  CONSTRAINT [FK_Problem_ProblemCategory_Link_Problem_ProblemCategory_Link] FOREIGN KEY([Id_Problem])
REFERENCES [dbo].[Problem] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Problem_ProblemCategory_Link] CHECK CONSTRAINT [FK_Problem_ProblemCategory_Link_Problem_ProblemCategory_Link]
GO
ALTER TABLE [dbo].[Problem_ProblemCategory_Link]  WITH CHECK ADD  CONSTRAINT [FK_Problem_ProblemCategory_Link_ProblemCategory] FOREIGN KEY([Id_ProblemCategory])
REFERENCES [dbo].[ProblemCategory] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Problem_ProblemCategory_Link] CHECK CONSTRAINT [FK_Problem_ProblemCategory_Link_ProblemCategory]
GO
ALTER TABLE [dbo].[ProblemReview]  WITH CHECK ADD  CONSTRAINT [FK_ProblemReview_Contestant] FOREIGN KEY([Id_Contestant])
REFERENCES [dbo].[Contestant] ([ID])
GO
ALTER TABLE [dbo].[ProblemReview] CHECK CONSTRAINT [FK_ProblemReview_Contestant]
GO
ALTER TABLE [dbo].[ProblemReview]  WITH CHECK ADD  CONSTRAINT [FK_ProblemReview_Problem] FOREIGN KEY([Id_Problem])
REFERENCES [dbo].[Problem] ([ID])
GO
ALTER TABLE [dbo].[ProblemReview] CHECK CONSTRAINT [FK_ProblemReview_Problem]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Admin] FOREIGN KEY([ID_Admin])
REFERENCES [dbo].[Admin] ([ID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Admin]
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD  CONSTRAINT [FK_Question_Contestant] FOREIGN KEY([ID_Contestant])
REFERENCES [dbo].[Contestant] ([ID])
GO
ALTER TABLE [dbo].[Question] CHECK CONSTRAINT [FK_Question_Contestant]
GO
ALTER TABLE [dbo].[Submission]  WITH CHECK ADD  CONSTRAINT [FK_Submission_Contestant] FOREIGN KEY([ID_Contestant])
REFERENCES [dbo].[Contestant] ([ID])
GO
ALTER TABLE [dbo].[Submission] CHECK CONSTRAINT [FK_Submission_Contestant]
GO
ALTER TABLE [dbo].[Submission]  WITH CHECK ADD  CONSTRAINT [FK_Submission_Problem] FOREIGN KEY([ID_Problem])
REFERENCES [dbo].[Problem] ([ID])
GO
ALTER TABLE [dbo].[Submission] CHECK CONSTRAINT [FK_Submission_Problem]
GO
ALTER TABLE [dbo].[SubmissionResult]  WITH CHECK ADD  CONSTRAINT [FK_SubmissionResult_Status] FOREIGN KEY([ID_Status])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[SubmissionResult] CHECK CONSTRAINT [FK_SubmissionResult_Status]
GO
ALTER TABLE [dbo].[SubmissionResult]  WITH CHECK ADD  CONSTRAINT [FK_SubmissionResult_Submission] FOREIGN KEY([ID_Submission])
REFERENCES [dbo].[Submission] ([ID])
GO
ALTER TABLE [dbo].[SubmissionResult] CHECK CONSTRAINT [FK_SubmissionResult_Submission]
GO
ALTER TABLE [dbo].[SubmissionResult]  WITH CHECK ADD  CONSTRAINT [FK_SubmissionResult_Test] FOREIGN KEY([ID_Test])
REFERENCES [dbo].[Test] ([ID])
GO
ALTER TABLE [dbo].[SubmissionResult] CHECK CONSTRAINT [FK_SubmissionResult_Test]
GO
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [FK_Test_Problem] FOREIGN KEY([ID_Problem])
REFERENCES [dbo].[Problem] ([ID])
GO
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [FK_Test_Problem]
GO
USE [master]
GO
ALTER DATABASE [CodEval] SET  READ_WRITE 
GO
