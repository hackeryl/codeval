﻿using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;

namespace CodEval.Api.Filters
{
    public class AuthorizedAdminAttribute : AuthorizationFilterAttribute
    {
        private AdminsService adminsService;        

        public AuthorizedAdminAttribute()
            : this(new CodEvalEntities())
        {

        }

        public AuthorizedAdminAttribute(DbContext context)
        {
            adminsService = new AdminsService(context, String.Empty);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var key = actionContext.Request.GetQueryNameValuePairs().FirstOrDefault(a => a.Key == "key");
            if (!String.IsNullOrWhiteSpace(key.Value))
            {
                if (adminsService.IsValidApiKey(key.Value))
                {
                    return;
                }
            }
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Content = new StringContent("Invalid Api Key!");
        }
    } 
}