﻿using CodEval.Api.Filters;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class ContestsController : ApiController
    {
        private ContestsService contestsService;

        public ContestsController()
            : this(new CodEvalEntities())
        {

        }

        public ContestsController(DbContext context)
        {
            contestsService = new ContestsService(context);
        }

        [HttpGet]
        public IEnumerable<Contest> Get()
        {
            return contestsService.GetAllContests();
        }

        // GET api/contests/5
        [HttpGet]
        public Contest Get(int id)
        {
            return contestsService.GetContestById(id);
        }
    }
}
