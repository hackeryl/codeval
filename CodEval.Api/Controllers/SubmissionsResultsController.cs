﻿using CodEval.Api.Filters;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class SubmissionsResultsController : ApiController
    {
        private static object flag = new object();

        private ContestsService contestsService;
        private ContestantsService contestantsService;
        private ProblemsService problemsService;

        public SubmissionsResultsController()
            : this(new CodEvalEntities())
        {

        }

        public SubmissionsResultsController(DbContext context)
        {
            contestsService = new ContestsService(context);
            contestantsService = new ContestantsService(context);
            problemsService = new ProblemsService(context);
        }

        // GET api/results
        [HttpGet]
        public IEnumerable<SubmissionResult> Get()
        {
            return contestsService.GetAllSubmissionsResults();
        }

        // GET api/results/5
        [HttpGet]
        public SubmissionResult Get(int id)
        {
            return contestsService.GetSubmissionResultById(id);
        }

        // POST api/results
        [HttpPost]
        public void Post([FromBody]List<SubmissionResult> results)
        {
            lock (flag)
            {
                contestsService.AddSubmissionsResults(results);
            }
        }

        // POST api/results
        [HttpPost]
        public void PostAsync([FromBody]List<SubmissionResult> results)
        {
            Task.Run(() =>
            {
                lock (flag)
                {
                    contestsService.AddSubmissionsResults(results);
                }
            });
            //UpdateResults(results);
        }

        // DELETE api/results/5
        [HttpDelete]
        private void Delete(int id)
        {
            var result = contestsService.GetSubmissionResultById(id);
            if (result != null)
            {
                contestsService.DeleteSubmissionResult(result);
            }
        }

        //[NonAction]
        //private async Task UpdateResults(List<SubmissionResult> results)
        //{
        //    if (results != null && results.Count > 0)
        //    {
        //        Task.Run(() =>
        //        {
        //            contestsService.AddSubmissionsResults(results);
        //        });
        //    }
        //}        
    }
}
