﻿using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CodEval.BLL.Models;
using CodEval.Api.Filters;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class SubmissionsController : ApiController
    {
        private static readonly string SubmissionsFolder = CodEval.Api.Properties.Settings.Default.RootDataFolder + @"Submissions\";
        private static object flag = new object();

        private ContestsService contestsService;
        private ContestantsService contestantsService;
        private ProblemsService problemsService;

        public SubmissionsController()
            : this(new CodEvalEntities())
        {

        }

        public SubmissionsController(DbContext context)
        {
            contestsService = new ContestsService(context);
            contestantsService = new ContestantsService(context);
            problemsService = new ProblemsService(context);
        }

        // GET api/Submissions
        [HttpGet]
        public IEnumerable<Submission> Get()
        {
            lock (flag)
            {
                return contestsService.GetAllSubmissions();
            }
        }

        // GET api/Submissions/5
        [HttpGet]
        public Submission Get(int id)
        {
            lock (flag)
            {
                return contestsService.GetSubmissionById(id);
            }
        }

        // GET api/Submissions/GetByStatus?status=SubmissionStatus.Submitted
        [HttpGet]
        public IEnumerable<Submission> GetByStatus(SubmissionStatus status, SubmissionStatus? newStatus)
        {
            lock (flag)
            {
                return contestsService.GetAllSubmissionsByStatus(status, newStatus);
            }
        }

        // GET api/Submissions/GetByContestStatus?status=SubmissionStatus.Submitted&idContest=5
        // GET api/Submissions/GetByContestStatus?status=SubmissionStatus.Submitted&idContest=5&newStatus=SubmissionStatus.InQueue
        [HttpGet]
        public IEnumerable<Submission> GetByContest_Status(int idContest, SubmissionStatus currentStatus, SubmissionStatus? newStatus)
        {
            lock (flag)
            {
                return contestsService.GetAllContestSubmissionsByStatus(idContest, currentStatus, newStatus);                
            }
        }

        // POST api/Submissions/5/SetStatus?status=SubmissionStatus.Submitted
        [HttpPost]
        public HttpResponseMessage SetStatus(int id, [FromBody]SubmissionStatus status)
        {
            lock (flag)
            {
                if (contestsService.SetSubmissionStatus(id, status))
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }            
        }

        // GET api/Submissions/5/SubmissionFile
        [HttpGet]
        public HttpResponseMessage GetSubmissionFile(int id)
        {
            var submission = Get(id);
            if (submission != null)
            {
                var contestant = contestantsService.GetContestantById(submission.ID_Contestant);
                var problem = problemsService.GetProblemById(submission.ID_Problem);
                if (contestant != null && problem != null)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.OK);
                    var stream = new System.IO.FileStream(SubmissionsFolder + problem.Id_Contest + @"\" + contestant.Username + @"\" + submission.FileName, System.IO.FileMode.Open);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    {
                        FileName = submission.FileName
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                    return response;
                }
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}
