﻿using CodEval.Api.Filters;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class ProblemsController : ApiController
    {
        private ProblemsService problemsService;

        public ProblemsController()
            : this(new CodEvalEntities())
        {

        }

        public ProblemsController(DbContext context)
        {
            problemsService = new ProblemsService(context);
        }

        // GET api/problems
        [HttpGet]
        public IEnumerable<Problem> Get()
        {
            return problemsService.GetAllProblems();
        }

        // GET api/problems/5
        [HttpGet]
        public Problem Get(int id)
        {
            return problemsService.GetProblemById(id);
        }

        // GET api/problems/?idContest=5
        [HttpGet]
        public IEnumerable<Problem> GetByContestId(int idContest)
        {
            return problemsService.GetAllProblemsByContestId(idContest);
        }
    }
}
