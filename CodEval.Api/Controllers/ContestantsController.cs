﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CodEval.BLL;
using CodEval.DAL;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System.Data.Entity;
using CodEval.Api.Filters;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class ContestantsController : ApiController
    {
        private ContestantsService contestantsService;

        public ContestantsController()
            : this(new CodEvalEntities())
        {

        }

        public ContestantsController(DbContext context)
        {
            contestantsService = new ContestantsService(context);
        }

        // GET api/contestants
        [HttpGet]
        public IEnumerable<Contestant> Get()
        {
            return contestantsService.GetAllContestants();
        }

        // GET api/contestants/5
        [HttpGet]
        public Contestant Get(int id)
        {
            return contestantsService.GetContestantById(id);
        }
    }
}
