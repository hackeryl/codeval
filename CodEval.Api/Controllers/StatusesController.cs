﻿using CodEval.Api.Filters;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class StatusesController : ApiController
    {
        private TestsService testsService;

        public StatusesController()
            : this(new CodEvalEntities())
        {

        }

        public StatusesController(DbContext context)
        {
            testsService = new TestsService(context, String.Empty);
        }

        // GET api/statuses
        [HttpGet]
        public IEnumerable<Status> Get()
        {
            return testsService.GetAllStatuses();
        }

        // GET api/statuses/5
        [HttpGet]
        public Status Get(int id)
        {
            return testsService.GetStatusById(id);
        }
    }
}
