﻿using CodEval.Api.Filters;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodEval.Api.Controllers
{
    [AuthorizedAdmin]
    public class TestsController : ApiController
    {
        private static readonly string TestsFolder = CodEval.Api.Properties.Settings.Default.RootDataFolder + @"Tests\";
        private TestsService testsService;

        public TestsController()
            : this(new CodEvalEntities())
        {

        }

        public TestsController(DbContext context)
        {
            testsService = new TestsService(context, String.Empty);
        }

        // GET api/tests
        [HttpGet]
        public IEnumerable<Test> Get()
        {
            return testsService.GetAllTests();
        }       

        // GET api/tests/5
        [HttpGet]
        public Test Get(int id)
        {
            return testsService.GetTestById(id);
        }

        // GET api/tests?idProblem=5
        [HttpGet]
        public IEnumerable<Test> GetByProblemId(int idProblem)
        {
            return testsService.GetAllTestsByProblemId(idProblem);
        }

        // GET api/tests/5/InputFile
        [HttpGet]
        public HttpResponseMessage GetInputFile(int id)
        {
            var test = Get(id);
            if (test != null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new System.IO.FileStream(TestsFolder + test.ID_Problem + @"\" + test.InputLocation, System.IO.FileMode.Open);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = test.Name + ".in"
                };
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                return response;
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        }

        // GET api/tests/5/OKFile
        [HttpGet]
        public HttpResponseMessage GetOKFile(int id)
        {
            var test = Get(id);
            if (test != null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new System.IO.FileStream(TestsFolder + test.ID_Problem + @"\" + test.OKLocation, System.IO.FileMode.Open);
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = test.Name + ".ok"
                };
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                return response;
            }
            return new HttpResponseMessage(HttpStatusCode.NotFound);
        } 
    }
}
