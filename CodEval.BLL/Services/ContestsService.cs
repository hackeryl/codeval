﻿using CodEval.BLL.Models;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    public class ContestsService
    {
        private UnitOfWork unit;

        public ContestsService(DbContext context)
        {
            unit = new UnitOfWork(context);
        }

        public string GetSubmissionName(int problemId, int contestantId)
        {
            string result = String.Empty;
            var sol = unit.SubmissionsRepository.Get().OrderByDescending(a => a.SubmissionTime).FirstOrDefault(a => a.ID_Contestant == contestantId && a.ID_Problem == problemId);
            if (sol != null)
            {
                result = sol.FileName;
            }
            return result;
        }

        public bool IsValidSubmission(int problemId, string fileName, int fileSize)
        {
            var problem = unit.ProblemsRepository.Get(a => a.ID == problemId, null, "Contest,Contest.ProgrammingLanguages").FirstOrDefault();
            if (problem != null)
            {
                if (problem.Contest.StartDate <= DateTime.Now && DateTime.Now <= problem.Contest.EndDate)
                {
                    string problemName = problem.Name.ToLower();
                    fileName = fileName.ToLower();
                    if (fileSize <= Constants.MaxFileSize)
                    {
                        var isValidExtension = (!problem.Contest.ProgrammingLanguages.Any()) || problem.Contest.ProgrammingLanguages.Select(a => problemName + a.AcceptedSourcesExtensions.ToLower()).Contains(fileName);
                        if (isValidExtension)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool AddSubmission(int problemId, int contestantId, string destinationFolder, string fileName, Stream sourceFileStream)
        {
            try
            {
                if (!Directory.Exists(destinationFolder))
                {
                    Directory.CreateDirectory(destinationFolder);
                }
                fileName = Guid.NewGuid().ToString() + fileName;
                string destinationPath = destinationFolder + "/" + fileName;
                using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.Write))
                {
                    sourceFileStream.CopyTo(fileStream);
                }
                TimeZone zone = TimeZone.CurrentTimeZone;
                var submission = new Submission()
                {
                    FileName = fileName,
                    ID_Contestant = contestantId,
                    ID_Problem = problemId,
                    Status = (int)SubmissionStatus.Submitted,
                    SubmissionTime = zone.ToUniversalTime(DateTime.Now).AddHours(3)
                };
                unit.SubmissionsRepository.Insert(submission);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
            return true;
        }

        public List<ContestantResult> GetContestantResults(int contestantId)
        {
            var result = new List<ContestantResult>();
            var submissions = unit.SubmissionResultsRepository.Get(a => a.Submission.ID_Contestant == contestantId, null, "Submission,Submission.Problem,Test,Status").OrderByDescending(a => a.Submission.SubmissionTime);
            foreach (var item in submissions)
            {
                var problem = item.Submission.Problem;
                var testName = item.Test.Name;
                var status = item.Status.Name;
                ContestantResult cr = new ContestantResult(item.ID, problem.Id_Contest, problem.Name, testName, status, item.Time, item.Score, item.EvaluationTime);
                result.Add(cr);
            }
            return result;
        }

        public List<Submission> GetContestantSubmissions(int contestantId)
        {
            var submissions = unit.SubmissionsRepository.Get(a => a.ID_Contestant == contestantId).OrderByDescending(a => a.SubmissionTime);
            return submissions.ToList();
        }

        public Submission GetContestantSubmission(int contestantId, int submissionId)
        {
            var submission = unit.SubmissionsRepository.Get(a => a.ID == submissionId && a.ID_Contestant == contestantId, null, "Problem").FirstOrDefault();
            return submission;
        }

        public void DeleteOldSubmission(int problemId, int contestantId, string root)
        {
            try
            {
                var submission = unit.SubmissionsRepository.Get(a => a.ID_Contestant == contestantId && a.ID_Problem == problemId).FirstOrDefault();
                if (submission != null && !String.IsNullOrEmpty(submission.FileName))
                {
                    root += "/" + submission.FileName;
                    if (File.Exists(root))
                    {
                        File.Delete(root);
                        submission.FileName = String.Empty;
                        unit.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<Contest> GetAllContests()
        {
            List<Contest> result = new List<Contest>();
            result = unit.ContestsRepository.Get().ToList();
            return result;
        }

        public bool AddContest(Contest comp)
        {
            try
            {
                unit.ContestsRepository.Insert(comp);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateContest(Contest comp)
        {
            try
            {
                unit.ContestsRepository.Update(comp);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteContest(Contest comp, string submissionsFolder, string testsFolder)
        {
            try
            {
                comp = unit.ContestsRepository.GetById(comp.ID);
                if (comp != null)
                {
                    var probs = unit.ProblemsRepository.Get(a => a.Id_Contest == comp.ID).ToList();
                    foreach (var p in probs)
                    {
                        var tests = unit.TestsRepository.Get(a => a.ID_Problem == p.ID).ToList();
                        foreach (var t in tests)
                        {
                            unit.TestsRepository.Delete(t);
                        }
                        var results = unit.SubmissionResultsRepository.Get(a => a.Submission.ID_Problem == p.ID, null, "Submission").ToList();
                        foreach (var r in results)
                        {
                            unit.SubmissionResultsRepository.Delete(r);
                        }
                        var submissions = unit.SubmissionsRepository.Get(a => a.ID_Problem == p.ID).ToList();
                        foreach (var s in submissions)
                        {
                            unit.SubmissionsRepository.Delete(s);
                        }
                        unit.ProblemsRepository.Delete(p);
                    }
                    unit.ContestsRepository.Delete(comp);
                    unit.SaveChanges();
                    if (Directory.Exists(submissionsFolder + comp.ID))
                    {
                        Directory.Delete(submissionsFolder + comp.ID, true);
                    }
                    foreach (var p in probs)
                    {
                        if (Directory.Exists(testsFolder + p.ID))
                        {
                            Directory.Delete(testsFolder + p.ID, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<ContestantRank> GetRankingsForContest(int contestId)
        {
            List<ContestantRank> rankings = new List<ContestantRank>();
            var contest = unit.ContestsRepository.GetById(contestId);
            if (contest != null)
            {
                var problemsIds = unit.ProblemsRepository.Get(a => a.Id_Contest == contest.ID).Select(a => a.ID);
                var contestants = unit.ContestantsRepository.Get();
                var results = unit.SubmissionResultsRepository.Get(a => problemsIds.Contains(a.Submission.ID_Problem), null, "Submission");
                var submissions = unit.SubmissionsRepository.Get(a => problemsIds.Contains(a.ID_Problem));

                var data = from res in results
                           group res by res.Submission.ID_Contestant into cd
                           select new
                           {
                               Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key),
                               Score = (
                                        from item in results.Where(a => a.Submission.ID_Contestant == cd.Key)
                                        group item by item.Submission.ID_Problem into ps
                                        //select ps.OrderByDescending(a => a.Submission.SubmissionTime)
                                        select ps.Where(a => a.ID_Submission == (submissions.Where(s => s.ID_Problem == ps.Key && s.ID_Contestant == cd.Key).OrderByDescending(s => s.SubmissionTime).FirstOrDefault().ID)).Sum(a => (int?)a.Score) ?? 0
                                        ).Sum(a => (int?)a) ?? 0
                           };

                //var data = from res in results
                //           group res by res.ID_Contestant into cd
                //           select new { Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key), Score = results.Where(a => a.ID_Contestant == cd.Key).Sum(a => a.Score) };
                data = data.OrderByDescending(a => a.Score);
                int rank = 0;
                int lastScore = -1;
                foreach (var comp in data)
                {
                    if (comp.Score != lastScore)
                    {
                        ++rank;
                        lastScore = comp.Score;
                    }
                    rankings.Add(new ContestantRank(comp.Contestant.FirstName + " " + comp.Contestant.LastName, comp.Score, rank));
                }
            }
            return rankings;
        }

        public string GetContestNameById(int contestId)
        {
            var contest = unit.ContestsRepository.GetById(contestId);
            if (contest != null)
            {
                return contest.Name;
            }
            return String.Empty;
        }

        public bool IsActiveContest(int contestId)
        {
            var contest = unit.ContestsRepository.GetById(contestId);
            if (contest != null)
            {
                var currentTime = TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now).AddHours(3);
                return contest.StartDate <= currentTime && currentTime <= contest.EndDate;
            }
            return false;
        }

        public List<Probem_ContestName> GetAllProblem_ContestsNames()
        {
            List<Probem_ContestName> result = new List<Probem_ContestName>();
            var problems = unit.ProblemsRepository.Get(null, null, "Contest");
            if (problems != null && problems.Any())
            {
                foreach (var pr in problems)
                {
                    Probem_ContestName cn = new Probem_ContestName(pr.ID, pr.Id_Contest, pr.Contest.Name, pr.Name);
                    result.Add(cn);
                }
            }
            return result;
        }

        public List<ContestName> GetAllContestsNames()
        {
            List<ContestName> result = new List<ContestName>();
            var contests = unit.ContestsRepository.Get();
            if (contests != null && contests.Any())
            {
                foreach (var c in contests)
                {
                    ContestName cn = new ContestName(c.ID, c.Name);
                    result.Add(cn);
                }
            }
            return result;
        }

        public TimeSpan GetContestRemainingTime(int contestId)
        {
            var contest = unit.ContestsRepository.GetById(contestId);
            if (contest != null)
            {
                var currentTime = TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now).AddHours(3);
                if (contest.StartDate <= currentTime && currentTime <= contest.EndDate)
                {
                    return contest.EndDate - currentTime;
                }
            }
            return TimeSpan.MinValue;
        }

        public List<ProgrammingLanguage> GetContestProgrammingLanguages(int contestId)
        {
            List<ProgrammingLanguage> result = new List<ProgrammingLanguage>();
            var programmingLanguages = unit.ProgrammingLanguagesRepository.Get(a => a.Contests.Any(c => c.ID == contestId));
            return programmingLanguages.ToList();
        }

        public string GetContestDescription(int contestId)
        {
            var contest = unit.ContestsRepository.GetById(contestId);
            if (contest != null)
            {
                return contest.Description;
            }
            return String.Empty;
        }

        public List<SubmissionStatusView> GetAllSubmissionsStatuses()
        {
            var result = new List<SubmissionStatusView>();
            var statuses = Enum.GetValues(typeof(SubmissionStatus)).Cast<SubmissionStatus>();
            foreach (var status in statuses)
            {
                result.Add(new SubmissionStatusView(status, status.ToString()));
            }
            return result;
        }

        public List<SubmissionScore> GetContestantSubmissionsScores(int contestantId)
        {
            var result = new List<SubmissionScore>();
            var submissions = unit.SubmissionsRepository.Get(a => a.ID_Contestant == contestantId, null, "SubmissionResults");
            foreach (var submission in submissions)
            {
                if (submission.SubmissionResults.Any())
                {
                    result.Add(new SubmissionScore(submission.ID, submission.SubmissionResults.Sum(a => a.Score)));
                }
                else
                {
                    result.Add(new SubmissionScore(submission.ID, null));
                }
            }
            return result;
        }

        public List<SubmissionScore> GetSubmissionsScores()
        {
            var result = new List<SubmissionScore>();
            var submissions = unit.SubmissionsRepository.Get(null, null, "SubmissionResults");
            foreach (var submission in submissions)
            {
                if (submission.SubmissionResults.Any())
                {
                    result.Add(new SubmissionScore(submission.ID, submission.SubmissionResults.Sum(a => a.Score)));
                }
                else
                {
                    result.Add(new SubmissionScore(submission.ID, null));
                }
            }
            return result;
        }

        public string GetContestantSubmissionSourceCode(int contestantId, int submissionId, string submissionsFolder)
        {
            var submission = unit.SubmissionsRepository.Get(a => a.ID == submissionId && a.ID_Contestant == contestantId, null, "Problem,Contestant").FirstOrDefault();
            if (submission != null)
            {
                var submissionPath = submissionsFolder + submission.Problem.Id_Contest + "/" + submission.Contestant.Username + "/" + submission.FileName;
                if (File.Exists(submissionPath))
                {
                    return File.ReadAllText(submissionPath);
                }
            }
            return String.Empty;
        }

        public List<SubmissionResult> GetSubmissionResults(int contestantId, int submissionId)
        {
            var submissionResults = unit.SubmissionResultsRepository.Get(a => a.ID_Submission == submissionId && a.Submission.ID_Contestant == contestantId);
            return submissionResults.ToList();
        }

        public List<SubmissionResult> GetSubmissionResults(int submissionId)
        {
            var submissionResults = unit.SubmissionResultsRepository.Get(a => a.ID_Submission == submissionId);
            return submissionResults.ToList();
        }

        public List<Submission> GetAllSubmissions()
        {
            return unit.SubmissionsRepository.Get().OrderByDescending(a => a.SubmissionTime).ToList();
        }

        public Submission GetSubmissionById(int submissionId)
        {
            return unit.SubmissionsRepository.GetById(submissionId);
        }

        public Submission GetSubmissionById(int submissionId, string includeProperties)
        {
            return unit.SubmissionsRepository.Get(s => s.ID == submissionId, null, includeProperties).FirstOrDefault();
        }

        public List<SubmissionName> GetAllSubmissionsNames()
        {
            List<SubmissionName> result = new List<SubmissionName>();
            var submissions = unit.SubmissionsRepository.Get(null, null, "Problem,Contestant");
            if (submissions.Any())
            {
                foreach (var submission in submissions)
                {
                    result.Add(new SubmissionName(submission));
                }
            }
            return result;
        }

        public Contest GetContestById(int id)
        {
            return unit.ContestsRepository.GetById(id);
        }

        public IEnumerable<Submission> GetAllSubmissionsByStatus(SubmissionStatus status, SubmissionStatus? newStatus)
        {
            var submissions = unit.SubmissionsRepository.Get(a => a.Status == (int)status).OrderByDescending(a => a.SubmissionTime).ToList();
            if (newStatus.HasValue)
            {
                foreach (var s in submissions)
                {
                    s.Status = (int)newStatus.Value;
                }
                unit.SaveChanges();
            }
            return submissions;
        }

        public IEnumerable<Submission> GetAllContestSubmissionsByStatus(int idContest, SubmissionStatus status, SubmissionStatus? newStatus)
        {
            var submissions = unit.SubmissionsRepository.Get(a => a.Problem.Id_Contest == idContest && a.Status == (int)status).OrderByDescending(a => a.SubmissionTime).ToList();
            if (newStatus.HasValue)
            {
                foreach (var s in submissions)
                {
                    s.Status = (int)newStatus.Value;
                }
                unit.SaveChanges();
            }
            return submissions;
        }

        public bool SetSubmissionStatus(int submissionId, SubmissionStatus status)
        {
            var submissionStatus = unit.SubmissionsRepository.GetById(submissionId);
            if (submissionStatus != null)
            {
                submissionStatus.Status = (int)status;
                return true;
            }
            return false;
        }

        public IEnumerable<SubmissionResult> GetAllSubmissionsResults()
        {
            return unit.SubmissionResultsRepository.Get().ToList();
        }

        public SubmissionResult GetSubmissionResultById(int id)
        {
            return unit.SubmissionResultsRepository.GetById(id);
        }

        public void DeleteSubmissionResult(SubmissionResult result)
        {
            unit.SubmissionResultsRepository.Delete(result);
            unit.SaveChanges();
        }

        public void AddSubmissionsResults(List<SubmissionResult> results)
        {
            TimeZone zone = TimeZone.CurrentTimeZone;
            DateTime evaluationTime = zone.ToUniversalTime(DateTime.Now).AddHours(3);
            foreach (var result in results)
            {
                var submissionResult = unit.SubmissionResultsRepository.Get(a => a.ID_Submission == result.ID_Submission && a.ID_Test == result.ID_Test, null, "Submission").FirstOrDefault();
                if (submissionResult != null)
                {
                    //update
                    submissionResult.EvaluationTime = evaluationTime;
                    submissionResult.ID_Status = result.ID_Status;
                    submissionResult.Memory = result.Memory;
                    submissionResult.Score = result.Score;
                    submissionResult.Time = result.Time;
                    submissionResult.Submission.Status = (int)SubmissionStatus.Evaluated;
                }
                else
                {
                    //add
                    var submission = unit.SubmissionsRepository.GetById(result.ID_Submission);
                    if (submission != null)
                    {
                        result.EvaluationTime = evaluationTime;
                        unit.SubmissionResultsRepository.Insert(result);
                        submission.Status = (int)SubmissionStatus.Evaluated;
                    }
                }
            }
            unit.SaveChanges();
        }

        public void SetContestSubmissionsStatus(int contestId, SubmissionStatus submissionStatus)
        {
            foreach (var item in unit.SubmissionsRepository.Get(a => a.Problem.Id_Contest == contestId))
            {
                item.Status = (int)submissionStatus;
            }
            unit.SaveChanges();
        }

        public IList<Submission> GetAllSubmissionsByProblemId(int problemId)
        {
            return unit.SubmissionsRepository.Get(s => s.ID_Problem == problemId, null, "Contestant").ToList();
        }
    }
}
