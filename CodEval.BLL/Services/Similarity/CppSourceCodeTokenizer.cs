﻿using System;
using System.Collections.Generic;
using CodEval.BLL.Models.Similarity;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// C++ SourceCodeTokenizer.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Similarity.ISourceCodeTokenizer" />
    public class CppSourceCodeTokenizer : ISourceCodeTokenizer
    {
        /// <summary>
        /// The punctuation tokens.
        /// </summary>
        private static readonly Dictionary<char, string> PunctuationTokens;

        /// <summary>
        /// The reserved keywords
        /// </summary>
        private static readonly HashSet<string> ReservedKeywords;

        /// <summary>
        /// Initializes the <see cref="CppSourceCodeTokenizer"/> class.
        /// </summary>
        static CppSourceCodeTokenizer()
        {
            PunctuationTokens = new Dictionary<char, string>()
            {
                {' ',"SPACE"}, {'\t',"TAB"}, {'.',"DOT"}, {',',"COMMA"}, {'<',"LT"}, {'>',"GT"}, {'/',"DIV"}, {'?',"QUEST"}, {';',"SEMI"}, {':',"TWODOT"}, {'#',"SHARP"},
                {'\'',"SQUOTE"}, {'\"',"QUOTE"}, {'~',"TILDE"}, {'[',"LBRACK"}, {']',"RBRACK"}, {'{',"LCURLY"}, {'}',"RCURLY"}, {'\\',"BCKSLSH"}, {'|',"VERBAR"}, {'!',"ADMIR"}, {'@',"AT"},
                {'$',"DOLLAR"}, {'%',"PERCENT"}, {'^',"CARET"}, {'&',"AMPERS"}, {'*',"TIMES"}, {'(',"LPAREN"}, {')',"RPAREN"}, {'-',"MINUS"}, {'+',"PLUS"}, {'=',"ASSIGN"}
            };

            ReservedKeywords = new HashSet<string>()
            {
                "asm","auto","bool","break","case",
                "catch","char","class","const","continue","default","delete","do","double",
                "else","enum","extern","float","for","friend","goto","if","inline","int",
                "long","namespace","new","operator","private","protected","public",
                "register","return","short","signed","sizeof","static","struct","switch",
                "template","this","throw","try","typedef","union","unsigned","using",
                "virtual","void","volatile","while"
            };
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>ISourceCodeTokenizer.</returns>
        public static ISourceCodeTokenizer Create()
        {
            return new CppSourceCodeTokenizer();
        }

        /// <summary>
        /// Gets the source code tokens.
        /// </summary>
        /// <param name="sourceCode">The source code.</param>
        /// <returns>
        /// List of Tokens after source code tokenization process.
        /// </returns>
        public IList<Token> GetSourceCodeTokens(string sourceCode)
        {
            var tokens = new List<Token>();
            var lines = sourceCode.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < lines.Length; i++)
            {
                lines[i] = RemoveComments(lines[i]);
                var words = GetWords(lines[i]);
                foreach (var word in words)
                {
                    var tokenValue = GetTokenValue(word);
                    var token = new Token(word, tokenValue, i);
                    tokens.Add(token);
                }
            }

            return tokens;
        }

        /// <summary>
        /// Removes the comments.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns>line without comments</returns>
        private string RemoveComments(string line)
        {
            if (line.Trim().StartsWith("//"))
            {
                return string.Empty;
            }

            return line.Trim();
        }

        /// <summary>
        /// Gets the token value.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>Token Value.</returns>
        private string GetTokenValue(string word)
        {
            if (ReservedKeywords.Contains(word))
            {
                return "LITERAL_" + word;
            }
            else
            {
                if (word.Length == 1 && IsPunctuation(word[0]))
                {
                    return PunctuationTokens[word[0]];
                }
                else
                {
                    if (IsInt(word))
                    {
                        return "NUM_INT";
                    }
                    else
                    {
                        if (IsFloat(word))
                        {
                            return "NUM_FLOAT";
                        }
                        else
                        {
                            if (IsDouble(word))
                            {
                                return "NUM_DOUBLE";
                            }
                            else
                            {
                                if (IsLong(word))
                                {
                                    return "NUM_LONG";
                                }
                                else
                                {
                                    return "IDENT";
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the words.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns>List of words.</returns>
        private IList<string> GetWords(string line)
        {
            var words = new List<string>();
            var i = 0;
            while (i < line.Length)
            {
                if (IsPunctuation(line[i]))
                {
                    if (i > 0)
                    {
                        words.Add(line.Substring(0, i));
                    }

                    if (line[i] != ' ' && line[i] != '\t')
                    {
                        words.Add(line[i].ToString());
                    }

                    line = line.Substring(i + 1).Trim();
                    i = 0;
                }
                else
                {
                    i++;
                }
            }

            if (i == line.Length && !string.IsNullOrWhiteSpace(line))
            {
                words.Add(line);
            }

            return words;
        }

        /// <summary>
        /// Determines whether the specified c is punctuation.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <returns>
        ///   <c>true</c> if the specified c is punctuation; otherwise, <c>false</c>.
        /// </returns>
        private bool IsPunctuation(char c)
        {
            return PunctuationTokens.ContainsKey(c);
        }

        /// <summary>
        /// Determines whether the specified word is long.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>
        ///   <c>true</c> if the specified word is long; otherwise, <c>false</c>.
        /// </returns>
        private bool IsLong(string word)
        {
            long result;
            return long.TryParse(word, out result);
        }

        /// <summary>
        /// Determines whether the specified word is double.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>
        ///   <c>true</c> if the specified word is double; otherwise, <c>false</c>.
        /// </returns>
        private bool IsDouble(string word)
        {
            double result;
            return double.TryParse(word, out result);
        }

        /// <summary>
        /// Determines whether the specified word is float.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>
        ///   <c>true</c> if the specified word is float; otherwise, <c>false</c>.
        /// </returns>
        private bool IsFloat(string word)
        {
            float result;
            return float.TryParse(word, out result);
        }

        /// <summary>
        /// Determines whether the specified word is int.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>
        ///   <c>true</c> if the specified word is int; otherwise, <c>false</c>.
        /// </returns>
        private bool IsInt(string word)
        {
            int result;
            return int.TryParse(word, out result);
        }
    }
}
