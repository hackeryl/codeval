﻿using System.Collections.Generic;
using CodEval.BLL.Models.Similarity;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// ISourceCodeSimilarityDetector.
    /// </summary>
    public interface ISourceCodeSimilarityDetector
    {
        /// <summary>
        /// Computes the similarity percentage.
        /// </summary>
        /// <param name="sourceCode1">The source code1.</param>
        /// <param name="sourceCode2">The source code2.</param>
        /// <param name="minimumMatchLength">Minimum length of the match.</param>
        /// <returns>The similarity percentage between 0 and 1. 0 means totally different, and 1 means that the sources are similar.</returns>
        float ComputeSimilarityPercentage(SourceCode sourceCode1, SourceCode sourceCode2, int minimumMatchLength);

        /// <summary>
        /// Computes the similarity percentage.
        /// </summary>
        /// <param name="sourceCodes">The source codes.</param>
        /// <param name="minimumMatchLength">Minimum length of the match.</param>
        /// <returns>The similarity percentage between each source code in the list.</returns>
        float[,] ComputeSimilarityPercentage(IList<SourceCode> sourceCodes, int minimumMatchLength);
    }
}
