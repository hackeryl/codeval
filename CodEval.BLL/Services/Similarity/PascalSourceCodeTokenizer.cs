﻿using System.Collections.Generic;
using CodEval.BLL.Models.Similarity;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// Pascal SourceCodeTokenizer.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Similarity.ISourceCodeTokenizer" />
    public class PascalSourceCodeTokenizer : ISourceCodeTokenizer
    {
        /// <summary>
        /// Initializes the <see cref="PascalSourceCodeTokenizer"/> class.
        /// </summary>
        static PascalSourceCodeTokenizer()
        {
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>ISourceCodeTokenizer.</returns>
        public static ISourceCodeTokenizer Create()
        {
            return new PascalSourceCodeTokenizer();
        }

        /// <summary>
        /// Gets the source code tokens.
        /// </summary>
        /// <param name="sourceCode">The source code.</param>
        /// <returns>
        /// List of Tokens after source code tokenization process.
        /// </returns>
        public IList<Token> GetSourceCodeTokens(string sourceCode)
        {
            throw new System.NotImplementedException();
        }
    }
}
