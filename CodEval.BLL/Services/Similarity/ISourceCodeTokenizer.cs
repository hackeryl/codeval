﻿using System.Collections.Generic;
using CodEval.BLL.Models.Similarity;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// ISourceCodeTokenizer.
    /// </summary>
    public interface ISourceCodeTokenizer
    {
        /// <summary>
        /// Gets the source code tokens.
        /// </summary>
        /// <param name="sourceCode">The source code.</param>
        /// <returns>List of Tokens after source code tokenization process.</returns>
        IList<Token> GetSourceCodeTokens(string sourceCode);
    }
}
