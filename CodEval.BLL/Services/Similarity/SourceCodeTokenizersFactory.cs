﻿using System;
using System.Collections.Generic;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// SourceCodeTokenizersFactory.
    /// </summary>
    public class SourceCodeTokenizersFactory
    {
        /// <summary>
        /// The tokenizer creators.
        /// </summary>
        private static readonly Dictionary<string, Func<ISourceCodeTokenizer>> TokenizerCreators = new Dictionary<string, Func<ISourceCodeTokenizer>>();

        /// <summary>
        /// Registers the source code tokenizer creator.
        /// </summary>
        /// <param name="programmingLanguage">The programming language.</param>
        /// <param name="sourceCodeTokenizerCreator">The source code tokenizer creator.</param>
        public static void RegisterSourceCodeTokenizerCreator(string programmingLanguage, Func<ISourceCodeTokenizer> sourceCodeTokenizerCreator)
        {
            TokenizerCreators[programmingLanguage] = sourceCodeTokenizerCreator;
        }

        /// <summary>
        /// Gets the source code tokenizer.
        /// </summary>
        /// <param name="programmingLanguage">The programming language.</param>
        /// <returns>ISourceCodeTokenizer</returns>
        public ISourceCodeTokenizer GetSourceCodeTokenizer(string programmingLanguage)
        {
            return TokenizerCreators[programmingLanguage.ToLower()].Invoke();
        }
    }
}
