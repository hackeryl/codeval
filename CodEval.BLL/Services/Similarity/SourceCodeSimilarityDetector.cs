﻿using System.Collections.Generic;
using System.Linq;
using CodEval.BLL.Models.Similarity;
using iTextSharp.text.pdf.crypto;

namespace CodEval.BLL.Services.Similarity
{
    /// <summary>
    /// SourceCodeSimilarityDetector.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Similarity.ISourceCodeSimilarityDetector" />
    public class SourceCodeSimilarityDetector : ISourceCodeSimilarityDetector
    {
        /// <summary>
        /// The source code tokenizers factory
        /// </summary>
        private readonly SourceCodeTokenizersFactory sourceCodeTokenizersFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceCodeSimilarityDetector" /> class.
        /// </summary>
        /// <param name="sourceCodeTokenizersFactory">The source code tokenizers factory.</param>
        public SourceCodeSimilarityDetector(SourceCodeTokenizersFactory sourceCodeTokenizersFactory)
        {
            this.sourceCodeTokenizersFactory = sourceCodeTokenizersFactory;
        }

        /// <summary>
        /// Computes the similarity percentage.
        /// </summary>
        /// <param name="sourceCode1">The source code1.</param>
        /// <param name="sourceCode2">The source code2.</param>
        /// <param name="minimumMatchLength">Minimum length of the match.</param>
        /// <returns>
        /// The similarity percentage between 0 and 1. 0 means totally different, and 1 means that the sources are similar.
        /// </returns>
        public float ComputeSimilarityPercentage(SourceCode sourceCode1, SourceCode sourceCode2, int minimumMatchLength)
        {
            var sourceCode1Tokens =
                sourceCodeTokenizersFactory.GetSourceCodeTokenizer(sourceCode1.ProgrammingLanguage)
                    .GetSourceCodeTokens(sourceCode1.Code.ToLower());
            var sourceCode2Tokens =
                sourceCodeTokenizersFactory.GetSourceCodeTokenizer(sourceCode2.ProgrammingLanguage)
                    .GetSourceCodeTokens(sourceCode2.Code.ToLower());
            var tiles = ComputeSourceCodeTokenMatches(sourceCode1Tokens, sourceCode2Tokens, minimumMatchLength);
            var coverage = 2 * tiles.Sum(t => t.Length);
            var total = sourceCode1Tokens.Count + sourceCode2Tokens.Count;
            return (float)coverage / (float)(total);
        }

        /// <summary>
        /// Computes the similarity percentage.
        /// </summary>
        /// <param name="sourceCodes">The source codes.</param>
        /// <param name="minimumMatchLength">Minimum length of the match.</param>
        /// <returns>
        /// The similarity percentage between each source code in the list.
        /// </returns>
        public float[,] ComputeSimilarityPercentage(IList<SourceCode> sourceCodes, int minimumMatchLength)
        {
            var tokenLists = new List<IList<Token>>();
            foreach (var sourceCode in sourceCodes)
            {
                tokenLists.Add(sourceCodeTokenizersFactory.GetSourceCodeTokenizer(sourceCode.ProgrammingLanguage.ToLower()).GetSourceCodeTokens(sourceCode.Code));
            }

            var similarities = new float[sourceCodes.Count, sourceCodes.Count];
            for (var i = 0; i < tokenLists.Count; i++)
            {
                similarities[i, i] = 1;
                for (var j = i + 1; j < tokenLists.Count; j++)
                {
                    var tiles = ComputeSourceCodeTokenMatches(tokenLists[i], tokenLists[j], minimumMatchLength);
                    var coverage = 2 * tiles.Sum(t => t.Length);
                    var total = tokenLists[i].Count + tokenLists[j].Count;
                    var similarity = total != 0 ? (float)coverage / (float)(total) : 1.0f;
                    similarities[i, j] = similarity;
                    similarities[j, i] = similarity;
                }
            }

            return similarities;
        }

        /// <summary>
        /// Computes the source code token matches, using Greedy String Tiling algorithm.
        /// More info here: http://gagolewski.rexamine.com/publications/2014rcodesimilarity.pdf
        /// </summary>
        /// <param name="sourceCode1Tokens">The source code1 tokens.</param>
        /// <param name="sourceCode2Tokens">The source code2 tokens.</param>
        /// <param name="minimumMatchLength">Minimum length of the match.</param>
        /// <returns></returns>
        private IList<Match> ComputeSourceCodeTokenMatches(IList<Token> sourceCode1Tokens, IList<Token> sourceCode2Tokens, int minimumMatchLength)
        {
            var tiles = new List<Match>();
            var sourceCode1MarkedTokens = new bool[sourceCode1Tokens.Count + 1];
            var sourceCode2MarkedTokens = new bool[sourceCode2Tokens.Count + 1];
            int maxmatch;
            do
            {
                maxmatch = minimumMatchLength;
                var matches = new List<Match>();
                for (var a = 0; a < sourceCode1Tokens.Count; a++)
                {
                    if (!sourceCode1MarkedTokens[a])
                    {
                        for (var b = 0; b < sourceCode2Tokens.Count; b++)
                        {
                            if (!sourceCode2MarkedTokens[b])
                            {
                                var j = 0;
                                while (a + j < sourceCode1Tokens.Count && !sourceCode1MarkedTokens[a + j] &&
                                    b + j < sourceCode2Tokens.Count && !sourceCode2MarkedTokens[b + j] &&
                                    (sourceCode1Tokens[a + j].TokenValue == sourceCode2Tokens[b + j].TokenValue))
                                {
                                    j++;
                                }

                                if (j == maxmatch)
                                {
                                    if (matches.Count == 0)
                                    {
                                        matches.Add(new Match(a, b, j));
                                    }
                                    else
                                    {
                                        var lastMatch = matches[matches.Count - 1];
                                        if ((lastMatch.SourceCode1StartIndex + lastMatch.Length <= a) && (lastMatch.SourceCode2StartIndex + lastMatch.Length <= b))
                                        {
                                            matches.Add(new Match(a, b, j));
                                        }
                                    }
                                }
                                else if (j > maxmatch)
                                {
                                    maxmatch = j;
                                    matches = new List<Match>()
                                    {
                                        new Match(a, b, j)
                                    };
                                }
                            }
                        }
                    }
                }

                foreach (var match in matches)
                {
                    for (var j = 0; j < match.Length; j++)
                    {
                        sourceCode1MarkedTokens[match.SourceCode1StartIndex + j] = true;
                        sourceCode2MarkedTokens[match.SourceCode2StartIndex + j] = true;
                    }

                    tiles.Add(match);
                }
            } while (maxmatch > minimumMatchLength);

            return tiles;
        }
    }
}
