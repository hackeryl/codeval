﻿using CodEval.BLL.Models;
using CodEval.DAL.Repository;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    public class ProblemsService
    {
        private UnitOfWork unit;

        public ProblemsService(DbContext context)
        {
            unit = new UnitOfWork(context);
        }

        public Problem GetProblemById(int problemId)
        {
            return unit.ProblemsRepository.GetById(problemId);
        }

        public Problem GetFullProblemDataById(int problemId)
        {
            return unit.ProblemsRepository.Get(a => a.ID == problemId, null, "Admin,Contest,Contest.ProgrammingLanguages,ProblemReviews,ProblemReviews.Contestant,Submissions,Submissions.SubmissionResults,Tests,ProblemCategories").FirstOrDefault();
        }

        public bool AddProblem(Problem prob)
        {
            try
            {
                unit.ProblemsRepository.Insert(prob);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateProblem(Problem prob)
        {
            try
            {
                unit.ProblemsRepository.Update(prob);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteProblem(Problem prob)
        {
            try
            {
                prob = unit.ProblemsRepository.GetById(prob.ID);
                var tests = unit.TestsRepository.Get(a => a.ID_Problem == prob.ID).ToList();
                foreach (var t in tests)
                {
                    unit.TestsRepository.Delete(t);
                }
                var results = unit.SubmissionResultsRepository.Get(a => a.Submission.ID_Problem == prob.ID, null, "Submission").ToList();
                foreach (var r in results)
                {
                    unit.SubmissionResultsRepository.Delete(r);
                }
                var submissions = unit.SubmissionsRepository.Get(a => a.ID_Problem == prob.ID).ToList();
                foreach (var s in submissions)
                {
                    unit.SubmissionsRepository.Delete(s);
                }
                unit.ProblemsRepository.Delete(prob);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<Problem> GetAllProblems()
        {
            List<Problem> result = new List<Problem>();
            result = unit.ProblemsRepository.Get().ToList();
            return result;
        }

        public List<ProblemSubmission> GetContestProblems(int contestantId, int contestId)
        {
            var result = new List<ProblemSubmission>();

            foreach (var pr in unit.ProblemsRepository.Get(a => a.Id_Contest == contestId))
            {
                var sol = unit.SubmissionsRepository.Get().OrderByDescending(a => a.SubmissionTime).FirstOrDefault(a => a.ID_Problem == pr.ID && a.ID_Contestant == contestantId);
                ProblemSubmission ps;
                if (sol != null)
                {
                    ps = new ProblemSubmission(pr.ID, pr.Name, pr.Name, sol.SubmissionTime, sol.ID);
                }
                else
                {
                    ps = new ProblemSubmission(pr.ID, pr.Name, String.Empty, null, -1);
                }
                result.Add(ps);
            }
            return result;
        }

        public List<Problem> GetAllProblemsByContestId(int idContest)
        {
            return unit.ProblemsRepository.Get(a => a.Id_Contest == idContest).ToList();
        }

        public string GetProblemText(int problemId, out string problemName)
        {
            string result = String.Empty;
            problemName = String.Empty;
            var problem = unit.ProblemsRepository.GetById(problemId);
            if (problem != null)
            {
                problemName = problem.Name;
                //var filePath = _problemsFolder + problem.Id_Contest + "/" + problem.FileName;
                //if (File.Exists(filePath))
                //{
                //    return File.ReadAllText(filePath);
                //}
            }
            return result;
        }

        public void UpdateProblemFileName(int problemId, string fileName)
        {
            try
            {
                var prob = unit.ProblemsRepository.GetById(problemId);
                if (prob != null)
                {
                    //prob.FileName = fileName;
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<ProblemName> GetAllProblemsNames()
        {
            List<ProblemName> result = new List<ProblemName>();
            var problems = unit.ProblemsRepository.Get();
            if (problems != null && problems.Count() > 0)
            {
                foreach (var prob in problems)
                {
                    ProblemName pn = new ProblemName(prob.ID, prob.Name);
                    result.Add(pn);
                }
            }
            return result;
        }

        public void DeletePreviousProblemFile(int problemId)
        {
            try
            {
                var prob = unit.ProblemsRepository.GetById(problemId);
                //if (prob != null && !String.IsNullOrWhiteSpace(prob.FileName))
                //{
                //    if (File.Exists(_problemsFolder + prob.Id_Contest + "/" + prob.FileName))
                //    {
                //        File.Delete(_problemsFolder + prob.Id_Contest + "/" + prob.FileName);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public int GetProblemIdByTestId(int testId)
        {
            var test = unit.TestsRepository.GetById(testId);
            if (test != null)
            {
                var problem = unit.ProblemsRepository.GetById(test.ID_Problem);
                if (problem != null)
                {
                    return problem.ID;
                }
            }
            return -1;
        }

        public string GetProblemNameById(int problemId)
        {
            var prob = unit.ProblemsRepository.GetById(problemId);
            if (prob != null)
            {
                return prob.Name;
            }
            return String.Empty;
        }

        public List<ProblemReview> GetContestantProblemReviews(int problemId, int contestantId)
        {
            return unit.ProblemReviewsRepository.Get(a => a.Id_Problem == problemId && a.Id_Contestant == contestantId).ToList();
        }

        public List<ProblemReview> GetProblemReviews(int problemId)
        {
            return unit.ProblemReviewsRepository.Get(a => a.Id_Problem == problemId).ToList();
        }

        public bool AddProblemReview(ProblemReview review)
        {
            try
            {
                unit.ProblemReviewsRepository.Insert(review);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateProblemReview(ProblemReview review)
        {
            try
            {
                unit.ProblemReviewsRepository.Update(review);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteProblemReview(ProblemReview review)
        {
            try
            {
                review = unit.ProblemReviewsRepository.GetById(review.ID);
                unit.ProblemReviewsRepository.Delete(review);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<ProblemCategory> GetProblemCategories()
        {
            var result = unit.ProblemCategoriesRepository.Get().ToList();
            return result;
        }

        public List<ProblemCategory> GetProblemCategories(int problemId)
        {
            List<ProblemCategory> result = new List<ProblemCategory>();
            var problem = unit.ProblemsRepository.Get(a => a.ID == problemId, null, "ProblemCategories").FirstOrDefault();
            if (problem != null)
            {
                result = problem.ProblemCategories.ToList();
            }
            return result;
        }

        public bool AddProblemCategory(ProblemCategory problemCategory)
        {
            try
            {
                unit.ProblemCategoriesRepository.Insert(problemCategory);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateProblemCategory(ProblemCategory problemCategory)
        {
            try
            {
                unit.ProblemCategoriesRepository.Update(problemCategory);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteProblemCategory(ProblemCategory problemCategory)
        {
            try
            {
                problemCategory = unit.ProblemCategoriesRepository.GetById(problemCategory.ID);
                unit.ProblemCategoriesRepository.Delete(problemCategory);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }
    }
}
