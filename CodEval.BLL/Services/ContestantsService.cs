﻿using CodEval.BLL.Models;
using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    public class ContestantsService
    {
        private UnitOfWork unit;

        public ContestantsService(DbContext context)
        {
            unit = new UnitOfWork(context);
        }

        public bool RegisterContestant(string firstName, string lastName, string email, string username, string password, out Contestant contestant)
        {
            password = HashService.ComputeHash(password);
            contestant = new Contestant()
            {
                Username = username,
                Password = password,
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };
            if (unit.ContestantsRepository.Get(a => a.Email.ToLower().Equals(email) || a.Username.ToLower().Equals(username)).Any())
            {
                //already registered
                return false;
            }
            return AddContestant(contestant);
        }

        public bool IsRegisteredContestant(string username, string password, out Contestant user)
        {
            user = null;
            try
            {
                password = HashService.ComputeHash(password);
                user = unit.ContestantsRepository.Get(a => a.Username == username && a.Password == password).FirstOrDefault();
                return user != null;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
            return false;
        }

        public List<Contestant> GetAllContestants()
        {
            List<Contestant> result = new List<Contestant>();
            result = unit.ContestantsRepository.Get().ToList();
            return result;
        }

        public bool AddContestant(Contestant contestant)
        {
            try
            {
                contestant.DefaultPassword = new DefaultPassword()
                {
                    DefaultPassword1 = contestant.Password
                };
                unit.ContestantsRepository.Insert(contestant);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public bool UpdateContestant(Contestant contestant)
        {
            try
            {
                unit.ContestantsRepository.Update(contestant);
                var defaultPassword = unit.DefaultPasswordsRepository.GetById(contestant.ID);
                if (defaultPassword == null)
                {
                    defaultPassword = new DefaultPassword()
                    {
                        ContestantId = contestant.ID,
                        DefaultPassword1 = contestant.Password
                    };
                    unit.DefaultPasswordsRepository.Insert(defaultPassword);
                }
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public bool DeleteContestant(Contestant contestant)
        {
            try
            {
                var cont = unit.ContestantsRepository.GetById(contestant.ID);
                if (cont != null)
                {
                    var defaultPassword = unit.DefaultPasswordsRepository.GetById(contestant.ID);
                    if (defaultPassword != null)
                    {
                        unit.DefaultPasswordsRepository.Delete(defaultPassword);
                    }
                    unit.ContestantsRepository.Delete(cont);
                    unit.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public List<ContestantName> GetAllContestantsNames()
        {
            List<ContestantName> result = new List<ContestantName>();

            var contestants = unit.ContestantsRepository.Get();
            if (contestants != null && contestants.Count() > 0)
            {
                foreach (var comp in contestants)
                {
                    ContestantName cn = new ContestantName(comp.ID, comp.FirstName, comp.LastName);
                    result.Add(cn);
                }
            }
            return result;
        }

        public List<Question> GetAllQuestions()
        {
            List<Question> result = new List<Question>();
            result = unit.QuestionsRepository.Get().ToList();
            return result;
        }

        public List<Question> GetContestantQuestions(int contestantId)
        {
            List<Question> result = new List<Question>();
            var sol = unit.QuestionsRepository.Get(a => a.ID_Contestant == contestantId);
            if (sol != null && sol.Count() > 0)
            {
                result = sol.ToList();
            }
            return result;
        }

        public bool SubmitQuestion(int contestantId, string question)
        {
            if (String.IsNullOrEmpty(question) || contestantId <= 0)
            {
                return false;
            }
            try
            {
                Question q = new Question();
                q.Question1 = question;
                q.ID_Contestant = contestantId;
                q.Answered = false;
                q.Answer = String.Empty;
                unit.QuestionsRepository.Insert(q);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateQuestion(Question q, int idAdmin)
        {
            try
            {
                q.Answered = !String.IsNullOrWhiteSpace(q.Answer);
                q.ID_Admin = idAdmin;
                unit.QuestionsRepository.Update(q);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteQuestion(Question q)
        {
            try
            {
                var question = unit.QuestionsRepository.GetById(q.ID);
                if (question != null)
                {
                    unit.QuestionsRepository.Delete(question);
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<SubmissionResult> GetAllResults()
        {
            List<SubmissionResult> result = new List<SubmissionResult>();
            result = unit.SubmissionResultsRepository.Get().OrderByDescending(a => a.EvaluationTime).ToList();
            return result;
        }

        public List<ProblemSubmissionResult> GetAllSubmissionsResults()
        {
            List<ProblemSubmissionResult> results = new List<ProblemSubmissionResult>();
            var res = unit.SubmissionResultsRepository.Get(null, null, "Submission").OrderByDescending(a => a.EvaluationTime).ToList();
            var probs = unit.ProblemsRepository.Get();
            foreach (var r in res)
            {
                var p = probs.FirstOrDefault(a => a.ID == r.Submission.ID_Problem);
                if (p != null)
                {
                    results.Add(new ProblemSubmissionResult(r, p.Id_Contest));
                }
            }
            return results;
        }

        public bool AddResult(SubmissionResult r)
        {
            try
            {
                unit.SubmissionResultsRepository.Insert(r);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateResult(SubmissionResult r)
        {
            try
            {
                unit.SubmissionResultsRepository.Update(r);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteResult(SubmissionResult r)
        {
            try
            {
                var result = unit.SubmissionResultsRepository.GetById(r.ID);
                if (result != null)
                {
                    unit.SubmissionResultsRepository.Delete(result);
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<Submission> GetAllSubmissions()
        {
            List<Submission> result = new List<Submission>();
            result = unit.SubmissionsRepository.Get().OrderByDescending(a => a.SubmissionTime).ToList();
            return result;
        }

        public bool AddSubmission(Submission sol)
        {
            try
            {
                unit.SubmissionsRepository.Insert(sol);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateSubmission(Submission sol)
        {
            try
            {
                unit.SubmissionsRepository.Update(sol);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteSubmission(Submission sol)
        {
            try
            {
                var solution = unit.SubmissionsRepository.GetById(sol.ID);
                if (solution != null)
                {
                    unit.SubmissionsRepository.Delete(solution);
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public Contestant GetContestantById(int contestantId)
        {
            return unit.ContestantsRepository.GetById(contestantId);
        }

        public Dictionary<Contestant, string> GenerateTemporaryPassowrds()
        {
            var contestants = unit.ContestantsRepository.Get();
            Dictionary<Contestant, string> contestantsPasswords = new Dictionary<Contestant, string>();
            foreach (var contestant in contestants)
            {
                var newPassword = RandomService.GetRandomString(8);
                contestant.Password = HashService.ComputeHash(newPassword);
                contestantsPasswords[contestant] = newPassword;
                unit.ContestantsRepository.Update(contestant);
            }
            unit.SaveChanges();
            return contestantsPasswords;
        }        

        public void ResetDefaultPassowrds()
        {
            var contestants = unit.ContestantsRepository.Get(null, null, "DefaultPassword");
            foreach (var contestant in contestants)
            {
                if (contestant.DefaultPassword != null)
                {
                    contestant.Password = contestant.DefaultPassword.DefaultPassword1;
                    unit.ContestantsRepository.Update(contestant);
                }
            }
            unit.SaveChanges();
        }
    }
}
