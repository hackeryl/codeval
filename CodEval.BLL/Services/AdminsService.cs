﻿using CodEval.DAL.Repository;
using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    public class AdminsService
    {
        private UnitOfWork unit;
        private string SourcesFilesFolder;

        public AdminsService(DbContext context, string sourcesFilesFolder)
        {
            unit = new UnitOfWork(context);
            SourcesFilesFolder = sourcesFilesFolder;
        }

        public bool IsRegisteredAdmin(string username, string password, out Admin admin)
        {
            admin = null;
            try
            {
                password = HashService.ComputeHash(password);
                admin = unit.AdminsRepository.Get(a => a.UserName == username && a.Password == password).FirstOrDefault();
                return admin != null;

            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
            return false;
        }

        public bool TryToModifyAdmin(int idAdmin, string username, string password)
        {
            try
            {
                var admin = unit.AdminsRepository.Get(a => a.ID == idAdmin).FirstOrDefault(a => a.ID == idAdmin);
                if (admin != null)
                {
                    password = HashService.ComputeHash(password);
                    admin.UserName = username;
                    admin.Password = password;
                    unit.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public byte[] GenerateSubmissionsZip(int contestId)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                using (ZipFile zip = new ZipFile())
                {
                    zip.StatusMessageTextWriter = System.Console.Out;
                    zip.AddDirectory(SourcesFilesFolder + contestId);
                    zip.Save(ms);
                }
                return ms.ToArray();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return null;
            }
        }

        public byte[] GenerateContestRankingPDF(int contestId)
        {
            var contest = unit.ContestsRepository.Get(a => a.ID == contestId).FirstOrDefault();
            if (contest != null)
            {
                var problemsIds = unit.ProblemsRepository.Get(a => a.Id_Contest == contest.ID).Select(a => a.ID);
                var contestants = unit.ContestantsRepository.Get();
                var results = unit.SubmissionResultsRepository.Get(a => problemsIds.Contains(a.Submission.ID_Problem), null, "Submission");
                var submissions = unit.SubmissionsRepository.Get(a => problemsIds.Contains(a.ID_Problem));

                var data = from res in results
                           group res by res.Submission.ID_Contestant into cd
                           select new
                           {
                               Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key),
                               Score = (
                                        from item in results.Where(a => a.Submission.ID_Contestant == cd.Key)
                                        group item by item.Submission.ID_Problem into ps
                                        select ps.Where(a => a.ID_Submission == (submissions.Where(s => s.ID_Problem == ps.Key && s.ID_Contestant == cd.Key).OrderByDescending(s => s.SubmissionTime).FirstOrDefault().ID)).Sum(a => (int?)a.Score) ?? 0
                                        ).Sum(a => (int?)a) ?? 0
                           };
                //var data = from res in results
                //           group res by res.ID_Contestant into cd
                //           select new { Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key), Score = results.Where(a => a.ID_Contestant == cd.Key).Sum(a => a.Score) };
                data = data.OrderByDescending(a => a.Score);

                var document = new Document(PageSize.A4, 10, 10, 35, 10);

                var output = new MemoryStream();
                PdfWriter.GetInstance(document, output);

                document.Open();

                document.Add(new Phrase(contest.Name + " Ranking", new Font(Font.FontFamily.TIMES_ROMAN, 22f, Font.BOLD)));

                var dataTable = new PdfPTable(3);

                dataTable.DefaultCell.Padding = 3;
                dataTable.DefaultCell.BorderWidth = 2;
                dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                dataTable.AddCell(new PdfPCell(new Phrase("Rank", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
                dataTable.AddCell(new PdfPCell(new Phrase("Contestant", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
                dataTable.AddCell(new PdfPCell(new Phrase("Score", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));

                dataTable.HeaderRows = 1;
                dataTable.DefaultCell.BorderWidth = 1;

                int rank = 0;
                int lastScore = -1;
                foreach (var comp in data)
                {
                    if (comp.Score != lastScore)
                    {
                        ++rank;
                        lastScore = comp.Score;
                    }
                    dataTable.AddCell(rank.ToString());
                    dataTable.AddCell(comp.Contestant.FirstName + " " + comp.Contestant.LastName);
                    dataTable.AddCell(comp.Score.ToString());
                }

                document.Add(dataTable);
                document.Add(new Phrase("Time: " + TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now).AddHours(3).ToLongTimeString(), new Font(Font.FontFamily.TIMES_ROMAN, 22f, Font.BOLDITALIC)));
                document.Close();

                return output.ToArray();
            }

            return null;
        }

        public byte[] GenerateContestIndividualRakingsPDF(int contestId)
        {
            MemoryStream stream = new MemoryStream();
            try
            {
                var contest = unit.ContestsRepository.Get(a => a.ID == contestId).FirstOrDefault();
                if (contest != null)
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.StatusMessageTextWriter = System.Console.Out;
                        var tests = unit.TestsRepository.Get();
                        var statuses = unit.StatusesRepository.Get();
                        var problemsIds = unit.ProblemsRepository.Get(a => a.Id_Contest == contest.ID).Select(a => a.ID);
                        var contestants = unit.ContestantsRepository.Get();
                        var results = unit.SubmissionResultsRepository.Get(a => problemsIds.Contains(a.Submission.ID_Problem), null, "Submission");
                        var submissions = unit.SubmissionsRepository.Get(a => problemsIds.Contains(a.ID_Problem));

                        var data = from res in results
                                   group res by res.Submission.ID_Contestant into cd
                                   select new
                                   {
                                       Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key),
                                       Results = (
                                                from item in results.Where(a => a.Submission.ID_Contestant == cd.Key)
                                                group item by item.Submission.ID_Problem into ps
                                                //select ps.OrderByDescending(a => a.Submission.SubmissionTime).FirstOrDefault()
                                                select ps.Where(a => a.ID_Submission == (submissions.Where(s => s.ID_Problem == ps.Key && s.ID_Contestant == cd.Key).OrderByDescending(s => s.SubmissionTime).FirstOrDefault().ID))
                                                )
                                   };

                        //var data = from res in results
                        //           group res by res.ID_Contestant into cd
                        //           select new { Contestant = contestants.FirstOrDefault(a => a.ID == cd.Key), Results = results.Where(a => a.ID_Contestant == cd.Key) };

                        foreach (var contestant in data)
                        {
                            var document = new Document(PageSize.A4, 10, 10, 35, 10);
                            var output = new MemoryStream();
                            PdfWriter.GetInstance(document, output);
                            document.Open();

                            document.Add(new Phrase(contestant.Contestant.FirstName + " " + contestant.Contestant.LastName, new Font(Font.FontFamily.TIMES_ROMAN, 22f, Font.BOLD)));

                            var dataTable = new PdfPTable(4);
                            dataTable.DefaultCell.Padding = 3;
                            dataTable.DefaultCell.BorderWidth = 2;
                            dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                            dataTable.AddCell(new PdfPCell(new Phrase("Input File", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
                            dataTable.AddCell(new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
                            dataTable.AddCell(new PdfPCell(new Phrase("Time", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
                            dataTable.AddCell(new PdfPCell(new Phrase("Score", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));

                            dataTable.HeaderRows = 1;
                            dataTable.DefaultCell.BorderWidth = 1;

                            var contestantResults = contestant.Results.ToList();

                            foreach (var cr in contestantResults)
                            {
                                foreach (var problemResult in cr)
                                {
                                    dataTable.AddCell(tests.FirstOrDefault(a => a.ID == problemResult.ID_Test).Name);
                                    dataTable.AddCell(statuses.FirstOrDefault(a => a.ID == problemResult.ID_Status).Name);
                                    dataTable.AddCell(problemResult.Time.ToString());
                                    dataTable.AddCell(problemResult.Score.ToString());
                                }
                            }

                            document.Add(dataTable);
                            if (contestantResults.Count > 0)
                            {
                                int totalScore = contestantResults.Sum(a => (int?)(a.Sum(b => (int?)b.Score) ?? 0)) ?? 0;
                                document.Add(new Phrase("\t\t\t\t\t\tTotal Score:\t" + totalScore.ToString(), new Font(Font.FontFamily.TIMES_ROMAN, 22f, Font.BOLD)));
                            }
                            document.Add(new Phrase("\n\t\t\t\t\t\tTime: " + TimeZone.CurrentTimeZone.ToUniversalTime(DateTime.Now).AddHours(3).ToLongTimeString(), new Font(Font.FontFamily.TIMES_ROMAN, 8f, Font.ITALIC)));
                            document.Close();

                            zip.AddEntry(contestant.Contestant.Username + ".pdf", output.ToArray());
                        }
                        zip.Save(stream);
                        return stream.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
            return null;
        }

        public byte[] GenerateTemporaryPassowrdsPdf(Dictionary<Contestant, string> contestantsTemporaryPasswords)
        {
            var document = new Document(PageSize.A4, 10, 10, 35, 10);

            var output = new MemoryStream();
            PdfWriter.GetInstance(document, output);

            document.Open();

            document.Add(new Phrase("Temporary Passwords - " + DateTime.Now.ToString(), new Font(Font.FontFamily.TIMES_ROMAN, 22f, Font.BOLD)));

            var dataTable = new PdfPTable(3);

            dataTable.DefaultCell.Padding = 3;
            dataTable.DefaultCell.BorderWidth = 2;
            dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            dataTable.AddCell(new PdfPCell(new Phrase("Contestant", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
            dataTable.AddCell(new PdfPCell(new Phrase("Username", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));
            dataTable.AddCell(new PdfPCell(new Phrase("Password", new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLDITALIC))));

            dataTable.HeaderRows = 1;
            dataTable.DefaultCell.BorderWidth = 1;
            dataTable.DefaultCell.Padding = 5.0f;
            dataTable.DefaultCell.PaddingBottom = 5.0f;

            foreach (var d in contestantsTemporaryPasswords)
            {
                dataTable.AddCell(d.Key.FirstName + " " + d.Key.LastName);
                dataTable.AddCell(d.Key.Username);
                dataTable.AddCell(d.Value);
            }

            document.Add(dataTable);
            document.Close();

            return output.ToArray();
        }

        public bool RegisterAdmin(string firstName, string lastName, string email, string username, string password, out Admin admin)
        {
            password = HashService.ComputeHash(password);
            admin = new Admin()
            {
                UserName = username,
                Password = password,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                ApiKey = Guid.NewGuid().ToString()
            };
            if (unit.AdminsRepository.Get(a => a.Email.ToLower().Equals(email) || a.UserName.ToLower().Equals(username)).Any())
            {
                //already registered
                return false;
            }
            else
            {
                unit.AdminsRepository.Insert(admin);
                unit.SaveChanges();
                return true;
            }
        }

        public bool IsValidApiKey(string key)
        {
            return unit.AdminsRepository.Get(a => a.ApiKey == key).Any();
        }
    }
}
