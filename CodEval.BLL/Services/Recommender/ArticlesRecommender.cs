﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CodEval.BLL.Models.Recommender;
using CodEval.BLL.Services.Recommender.ArticlesApi;
using CodEval.DAL.Repository;

namespace CodEval.BLL.Services.Recommender
{
    /// <summary>
    /// ArticlesRecommender.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Recommender.IRecommender" />
    public class ArticlesRecommender : IRecommender
    {
        /// <summary>
        /// The unit
        /// </summary>
        private readonly UnitOfWork unit;

        /// <summary>
        /// The articles apis
        /// </summary>
        private readonly IList<IArticlesApi> articlesApis;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProblemRecommender"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ArticlesRecommender(DbContext context)
        {
            this.unit = new UnitOfWork(context);
            this.articlesApis = new List<IArticlesApi>()
            {
                new MdpiArticlesApi(),
                new GoogleScholarArticlesApi()
            };
        }

        /// <summary>
        /// Gets the recommendations for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// List of Recommendation for the user.
        /// </returns>
        public IList<Recommendation> GetRecommendationsForUser(int userId)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Gets the recommendations for problem.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="problemId">The problem identifier.</param>
        /// <returns>
        /// List of Recommendation for the problem.
        /// </returns>
        public IList<Recommendation> GetRecommendationsForProblem(int userId, int problemId)
        {
            var recommendations = new List<Recommendation>();
            var problem = unit.ProblemsRepository.Get(p => p.ID == problemId, null, "ProblemCategories").FirstOrDefault();
            if (problem != null)
            {
                foreach (var category in problem.ProblemCategories)
                {
                    var categoryArticles = GetArticlesForCategory(category.Name);
                    recommendations.AddRange(categoryArticles);
                }
            }

            return recommendations.GroupBy(r => r.Url).Select(g => g.FirstOrDefault()).ToList();
        }

        /// <summary>
        /// Gets the articles for category.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns></returns>
        private IList<Recommendation> GetArticlesForCategory(string categoryName)
        {
            var recommendations = new List<Recommendation>();
            foreach (var articlesApi in articlesApis)
            {
                var articles = articlesApi.GetArticles(categoryName);
                foreach (var article in articles)
                {
                    recommendations.Add(new Recommendation()
                    {
                        Name = article.Name,
                        Description = article.Description,
                        Url = article.Url,
                        Details = article
                    });
                }
            }

            return recommendations;
        }
    }
}
