﻿using System.Collections.Generic;
using CodEval.BLL.Models.Recommender;

namespace CodEval.BLL.Services.Recommender
{
    /// <summary>
    /// IRecommender.
    /// </summary>
    public interface IRecommender
    {
        /// <summary>
        /// Gets the recommendations for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>List of Recommendation for the user.</returns>
        IList<Recommendation> GetRecommendationsForUser(int userId);
        /// <summary>
        /// Gets the recommendations for problem.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="problemId">The problem identifier.</param>
        /// <returns>
        /// List of Recommendation for the problem.
        /// </returns>
        IList<Recommendation> GetRecommendationsForProblem(int userId, int problemId);
    }
}
