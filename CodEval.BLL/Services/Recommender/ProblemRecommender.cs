﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using CodEval.BLL.Models.Recommender;
using CodEval.DAL.Repository;

namespace CodEval.BLL.Services.Recommender
{
    /// <summary>
    /// ProblemRecommender
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Recommender.IRecommender" />
    public class ProblemRecommender : IRecommender
    {
        /// <summary>
        /// The unit
        /// </summary>
        private readonly UnitOfWork unit;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProblemRecommender"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ProblemRecommender(DbContext context)
        {
            this.unit = new UnitOfWork(context);
        }

        /// <summary>
        /// Gets the recommendations for user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// List of Recommendation for the user.
        /// </returns>
        public IList<Recommendation> GetRecommendationsForUser(int userId)
        {
            var recommendations = new List<Recommendation>();

            return recommendations;
        }

        /// <summary>
        /// Gets the recommendations for problem.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="problemId">The problem identifier.</param>
        /// <returns>
        /// List of Recommendation for the problem.
        /// </returns>
        public IList<Recommendation> GetRecommendationsForProblem(int userId, int problemId)
        {
            var recommendations = new List<Recommendation>();
            var problem = unit.ProblemsRepository.Get(p => p.ID == problemId, null, "ProblemReviews,ProblemReviews.Contestant,ProblemCategories,ProblemCategories.Problems,ProblemCategories.Problems.ProblemReviews").FirstOrDefault();
            if (problem != null)
            {
                var problems = new Dictionary<int, Problem>();
                foreach (var prob in problem.ProblemCategories.SelectMany(category => category.Problems.Where(p => !p.ProblemReviews.Any(pr => pr.Id_Contestant == userId))))
                {
                    problems[prob.ID] = prob;
                }

                problems.Remove(problemId);
                foreach (var prob in problems)
                {
                    IList<ProblemReview> sharedReviews;
                    var pearsonCorrelation = CalculatePearsonCorrelation(problem, prob.Value, out sharedReviews);
                    if (pearsonCorrelation > 0)
                    {
                        recommendations.Add(new Recommendation()
                        {
                            Rating = pearsonCorrelation,
                            Name = prob.Value.Name,
                            Description = string.Format("{0} recommended based on {1} reviews.", prob.Value.Name, string.Join(",", sharedReviews.Select(pr => pr.Contestant.FirstName + " " + pr.Contestant.LastName))),
                            Url = string.Format("{0}/Problem?id={1}", Properties.Resources.RootUrl, prob.Value.ID)
                        });
                    }
                }
            }

            return recommendations.OrderByDescending(r => r.Rating).ToList();
        }

        /// <summary>
        /// Calculates the pearson correlation.
        /// More details about the algorithm here: https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
        /// </summary>
        /// <param name="problem1">The problem1.</param>
        /// <param name="problem2">The problem2.</param>
        /// <param name="sharedReviews">The shared reviews.</param>
        /// <returns>
        /// This method will return a value between -1 and 1, where 1 means the two problems have exactly the same ratings, and it is most likely to be a good recommendation for the user.
        /// </returns>
        private double CalculatePearsonCorrelation(Problem problem1, Problem problem2, out IList<ProblemReview> sharedReviews)
        {
            sharedReviews = new List<ProblemReview>();

            // collect a list of user reviews have have reviews in common
            foreach (var review in problem1.ProblemReviews)
            {
                if (problem2.ProblemReviews.Any(pr => pr.Id_Contestant == review.Id_Contestant))
                {
                    sharedReviews.Add(review);
                }
            }

            if (sharedReviews.Count == 0)
            {
                // they have nothing in common => return zero
                return 0;
            }

            // sum up all the preferences
            double problem1ReviewSum = 0.00f;
            double problem2ReviewSum = 0.00f;
            foreach (var item in sharedReviews)
            {
                problem1ReviewSum += problem1.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value;
                problem2ReviewSum += problem2.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value;
            }

            // sum up the squares
            double problem1Rating = 0f;
            double problem2Rating = 0f;
            foreach (var item in sharedReviews)
            {
                problem1Rating += Math.Pow(problem1.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value, 2);
                problem2Rating += Math.Pow(problem2.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value, 2);
            }

            //sum up the products
            double criticsSum = 0f;
            foreach (var item in sharedReviews)
            {
                criticsSum +=
                    problem1.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value *
                    problem2.ProblemReviews.FirstOrDefault(pr => pr.Id_Contestant == item.Id_Contestant).Rate.Value;
            }

            //calculate pearson score
            double num = criticsSum - (problem1ReviewSum * problem2ReviewSum / sharedReviews.Count);
            double density = Math.Sqrt((problem1Rating - Math.Pow(problem1ReviewSum, 2) / sharedReviews.Count) * ((problem2Rating - Math.Pow(problem2ReviewSum, 2) / sharedReviews.Count)));
            if (density == 0)
            {
                return 0;
            }

            return num / density;
        }
    }
}
