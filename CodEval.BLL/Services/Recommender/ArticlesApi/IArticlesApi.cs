﻿using System.Collections.Generic;
using CodEval.BLL.Models.Recommender;

namespace CodEval.BLL.Services.Recommender.ArticlesApi
{
    /// <summary>
    /// IArticlesApi.
    /// </summary>
    public interface IArticlesApi
    {
        /// <summary>
        /// Gets the articles.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>List of Articles.</returns>
        IList<Article> GetArticles(string query);
    }
}
