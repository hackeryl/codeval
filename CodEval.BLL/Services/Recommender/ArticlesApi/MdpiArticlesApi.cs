﻿using System;
using System.Collections.Generic;
using CodEval.BLL.Models.Recommender;
using HtmlAgilityPack;

namespace CodEval.BLL.Services.Recommender.ArticlesApi
{
    /// <summary>
    /// MdpiArticlesApi - wrapper for http://www.mdpi.com articles database.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Recommender.ArticlesApi.IArticlesApi" />
    public class MdpiArticlesApi : IArticlesApi
    {
        /// <summary>
        /// Gets the articles.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>
        /// List of Articles.
        /// </returns>
        public IList<Article> GetArticles(string query)
        {
            try
            {
                var url = string.Format(Properties.Resources.MdpiSearchUrl, query);
                var client = new HtmlWeb();
                var document = client.Load(url);
                var articles = GetArticlesFromHtmlDocument(document);
                return articles;
            }
            catch (Exception)
            {
                return new List<Article>();
            }
        }

        /// <summary>
        /// Gets the articles from HTML document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        private IList<Article> GetArticlesFromHtmlDocument(HtmlDocument document)
        {
            var articles = new List<Article>();
            var articleNodes = document.DocumentNode.SelectNodes("//div[contains(@class, 'article-content')]");
            foreach (var articleNode in articleNodes)
            {
                var article = new Article();
                var titleNode = articleNode.SelectSingleNode(".//a[contains(@class, 'title-link')]");
                article.Name = titleNode.InnerText;
                article.Url = string.Format("{0}{1}", Properties.Resources.MdpiArticlesBaseUrl, titleNode.GetAttributeValue("href", string.Empty));
                var authorsNodes = articleNode.SelectNodes(".//div[contains(@class, 'authors')]//a");
                foreach (var authorNode in authorsNodes)
                {
                    article.Authors.Add(authorNode.InnerText);
                }

                var publicationDateNode = articleNode.SelectSingleNode(".//div[contains(@class, 'pubdates')]");
                article.PublicationDate = publicationDateNode.InnerText;
                var abstractNode = articleNode.SelectSingleNode(".//div[contains(@class, 'abstract-div')]//div[contains(@class, 'abstract-cropped inline')]//div") ??
                                   articleNode.SelectSingleNode(".//div[contains(@class, 'abstract-div')]");

                article.Description = abstractNode.InnerText;
                articles.Add(article);
            }

            return articles;
        }
    }
}
