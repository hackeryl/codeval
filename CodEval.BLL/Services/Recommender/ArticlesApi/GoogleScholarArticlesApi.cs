﻿using System;
using System.Collections.Generic;
using CodEval.BLL.Models.Recommender;
using HtmlAgilityPack;

namespace CodEval.BLL.Services.Recommender.ArticlesApi
{
    /// <summary>
    /// GoogleScholarArticlesApi - wrapper for https://scholar.google.com articles database.
    /// </summary>
    /// <seealso cref="CodEval.BLL.Services.Recommender.ArticlesApi.IArticlesApi" />
    public class GoogleScholarArticlesApi : IArticlesApi
    {
        /// <summary>
        /// Gets the articles.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>
        /// List of Articles.
        /// </returns>
        public IList<Article> GetArticles(string query)
        {
            try
            {
                var url = string.Format(Properties.Resources.GoogleScholarSearchUrl, query);
                var client = new HtmlWeb();
                var document = client.Load(url);
                var articles = GetArticlesFromHtmlDocument(document);
                return articles;
            }
            catch (Exception)
            {
                return new List<Article>();
            }
        }

        /// <summary>
        /// Gets the articles from HTML document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns></returns>
        private IList<Article> GetArticlesFromHtmlDocument(HtmlDocument document)
        {
            var articles = new List<Article>();
            var articleNodes = document.DocumentNode.SelectNodes("//div[@class='gs_r']");
            foreach (var articleNode in articleNodes)
            {
                var article = new Article();
                var titleNode = articleNode.SelectSingleNode(".//div[contains(@class, 'gs_ri')]//h3[contains(@class, 'gs_rt')]//a");
                if (titleNode == null)
                {
                    continue;
                }

                article.Name = titleNode.InnerHtml;
                article.Url = titleNode.GetAttributeValue("href", Properties.Resources.GoogleScholarSearchUrl);
                var authorsNodes = articleNode.SelectNodes(".//div[contains(@class, 'gs_a')]//a");
                if (authorsNodes != null)
                {
                    foreach (var authorsNode in authorsNodes)
                    {
                        article.Authors.Add(authorsNode.InnerText);
                    }
                }

                var abstractNode = articleNode.SelectSingleNode(".//div[contains(@class, 'gs_rs')]");
                if (abstractNode != null)
                {
                    article.Description = abstractNode.InnerText;
                }

                articles.Add(article);
            }

            return articles;
        }
    }
}
