﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    static class RandomService
    {
        private static readonly string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        private static readonly Random random = new Random(DateTime.Now.Millisecond);

        public static string GetRandomString(uint length)
        {
            string randomString = String.Empty;

            for (uint i = 0; i < length; i++)
            {
                randomString += chars[random.Next(chars.Length)];
            }

            return randomString;
        }
    }
}
