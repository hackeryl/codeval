﻿using CodEval.BLL.Models;
using CodEval.DAL.Repository;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Services
{
    public class TestsService
    {
        private UnitOfWork unit;
        private string _testsFolder;

        public TestsService(DbContext context, string testsFolder)
        {
            unit = new UnitOfWork(context);
            _testsFolder = testsFolder;
        }

        #region Tests
        public List<TestName> GetAllTestsNames()
        {
            List<TestName> result = new List<TestName>();
            var tests = unit.TestsRepository.Get();
            if (tests != null && tests.Count() > 0)
            {
                foreach (var test in tests)
                {
                    TestName tn = new TestName(test.ID, test.Name);
                    result.Add(tn);
                }
            }
            return result;
        }

        public List<TestName> GetTestsNamesForProblem(int problemId)
        {
            List<TestName> result = new List<TestName>();
            var tests = unit.TestsRepository.Get(a => a.ID_Problem == problemId);
            if (tests != null && tests.Count() > 0)
            {
                foreach (var test in tests)
                {
                    TestName tn = new TestName(test.ID, test.Name);
                    result.Add(tn);
                }
            }
            return result;
        }

        public List<Test> GetAllTests()
        {
            return unit.TestsRepository.Get().ToList();
        }

        public void UpdateProblemTests(int problemId)
        {
            try
            {
                var problem = unit.ProblemsRepository.GetById(problemId);
                if (problem != null)
                {
                    DeleteProblemTests(problemId);
                    for (int i = 0; i < 10; i++)
                    {
                        Test t = new Test()
                        {
                            ID_Problem = problem.ID,
                            Name = problem.Name.ToLower() + i,
                            InputLocation = String.Empty,
                            OKLocation = String.Empty,
                            Points = 10
                        };
                        unit.TestsRepository.Insert(t);
                    }
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteProblemTests(int problemId)
        {
            try
            {
                var problem = unit.ProblemsRepository.GetById(problemId);
                if (problem != null)
                {
                    var oldTests = unit.TestsRepository.Get(a => a.ID_Problem == problemId).ToList();
                    foreach (var t in oldTests)
                    {
                        DeleteTestFiles(t.ID);
                        unit.TestsRepository.Delete(t);
                    }
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public List<Test> GetAllTestsByProblemId(int problemId)
        {
            return unit.TestsRepository.Get(a => a.ID_Problem == problemId).ToList();
        }

        public bool AddTest(Test test)
        {
            try
            {
                unit.TestsRepository.Insert(test);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateTest(Test test)
        {
            try
            {
                unit.TestsRepository.Update(test);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteTest(Test test)
        {
            try
            {
                test = unit.TestsRepository.GetById(test.ID);
                if (test != null)
                {
                    DeleteTestFiles(test.ID);
                    var results = unit.SubmissionResultsRepository.Get(a => a.ID_Test == test.ID).ToList();
                    foreach (var r in results)
                    {
                        unit.SubmissionResultsRepository.Delete(r);
                    }
                    unit.TestsRepository.Delete(test);
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void UpdateTestFileName(int testId, string fileName)
        {
            try
            {
                var test = unit.TestsRepository.GetById(testId);
                if (test != null)
                {
                    test.InputLocation = fileName;
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void UpdateTestOKFileName(int testId, string fileName)
        {
            try
            {
                var test = unit.TestsRepository.GetById(testId);
                if (test != null)
                {
                    test.OKLocation = fileName;
                    unit.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public byte[] GenerateTestZip(int testId)
        {
            var test = unit.TestsRepository.GetById(testId);
            if (test != null)
            {
                var problem = unit.ProblemsRepository.GetById(test.ID_Problem);
                if (problem != null)
                {
                    try
                    {
                        using (ZipFile zip = new ZipFile())
                        {
                            MemoryStream stream = new MemoryStream();
                            zip.StatusMessageTextWriter = System.Console.Out;
                            zip.AddFile(_testsFolder + problem.ID + "/" + test.InputLocation, test.Name);
                            zip.AddFile(_testsFolder + problem.ID + "/" + test.OKLocation, test.Name);
                            zip.Save(stream);
                            return stream.ToArray();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                    }
                }
            }
            return null;
        }

        public byte[] GenerateTestsZip(int problemId)
        {
            var problem = unit.ProblemsRepository.Get(a => a.ID == problemId, null, "Tests").FirstOrDefault();
            if (problem != null)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        MemoryStream stream = new MemoryStream();
                        zip.StatusMessageTextWriter = System.Console.Out;
                        foreach (var test in problem.Tests)
                        {
                            if (File.Exists(_testsFolder + problem.ID + "/" + test.InputLocation))
                            {
                                zip.AddFile(_testsFolder + problem.ID + "/" + test.InputLocation, problem.Name);
                            }
                            if (File.Exists(_testsFolder + problem.ID + "/" + test.OKLocation))
                            {
                                zip.AddFile(_testsFolder + problem.ID + "/" + test.OKLocation, problem.Name);
                            }
                        }
                        zip.Save(stream);
                        return stream.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                }
            }
            return null;
        }        

        public List<StatusName> GetAllStatusesNames()
        {
            List<StatusName> result = new List<StatusName>();
            var statuses = unit.StatusesRepository.Get();
            if (statuses != null && statuses.Count() > 0)
            {
                foreach (var status in statuses)
                {
                    StatusName sn = new StatusName(status.ID, status.Name, status.Color);
                    result.Add(sn);
                }
            }
            return result;
        }

        public List<Status> GetAllStatuses()
        {
            return unit.StatusesRepository.Get().ToList();
        }

        public bool AddStatus(Status stat)
        {
            try
            {
                unit.StatusesRepository.Insert(stat);
                unit.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                return false;
            }
        }

        public void UpdateStatus(Status stat)
        {
            try
            {
                unit.StatusesRepository.Update(stat);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteStatus(Status stat)
        {
            try
            {
                stat = unit.StatusesRepository.GetById(stat.ID);
                unit.StatusesRepository.Delete(stat);
                unit.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        #endregion

        public bool SaveTestInputFile(Stream stream, int testId)
        {
            var test = unit.TestsRepository.Get(a => a.ID == testId, null, "Problem").FirstOrDefault();
            if (test != null)
            {
                try
                {
                    string destionationFolder = _testsFolder + "/" + test.Problem.ID + "/";
                    if (!Directory.Exists(destionationFolder))
                    {
                        Directory.CreateDirectory(destionationFolder);
                    }
                    string fileName = test.Name + "-" + Guid.NewGuid().ToString() + ".in";
                    string destinationPath = destionationFolder + fileName;
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.Write))
                    {
                        stream.CopyTo(fileStream);
                    }
                    if (!String.IsNullOrWhiteSpace(test.InputLocation) && File.Exists(destionationFolder + test.InputLocation))
                    {
                        File.Delete(destionationFolder + test.InputLocation);
                    }
                    test.InputLocation = fileName;
                    unit.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public bool SaveTestOKFile(Stream stream, int testId)
        {
            var test = unit.TestsRepository.Get(a => a.ID == testId, null, "Problem").FirstOrDefault();
            if (test != null)
            {
                try
                {
                    string destionationFolder = _testsFolder + "/" + test.Problem.ID + "/";
                    if (!Directory.Exists(destionationFolder))
                    {
                        Directory.CreateDirectory(destionationFolder);
                    }
                    string fileName = test.Name + "-" + Guid.NewGuid().ToString() + ".ok";
                    string destinationPath = destionationFolder + fileName;
                    using (var fileStream = new FileStream(destinationPath, FileMode.Create, FileAccess.Write))
                    {
                        stream.CopyTo(fileStream);
                    }
                    if (!String.IsNullOrWhiteSpace(test.OKLocation) && File.Exists(destionationFolder + test.OKLocation))
                    {
                        File.Delete(destionationFolder + test.OKLocation);
                    }
                    test.OKLocation = fileName;
                    unit.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        private void DeleteTestFiles(int testId)
        {
            DeleteTestInputFile(testId);
            DeleteTestOKFile(testId);
        }

        private void DeleteTestInputFile(int testId)
        {
            var test = unit.TestsRepository.Get(a => a.ID == testId, null, "Problem").FirstOrDefault();
            if (test != null)
            {
                try
                {
                    string destionationFolder = _testsFolder + "/" + test.Problem.ID + "/";
                    if (!String.IsNullOrWhiteSpace(test.InputLocation) && File.Exists(destionationFolder + test.InputLocation))
                    {
                        File.Delete(destionationFolder + test.InputLocation);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                }
            }
        }

        private void DeleteTestOKFile(int testId)
        {
            var test = unit.TestsRepository.Get(a => a.ID == testId, null, "Problem").FirstOrDefault();
            if (test != null)
            {
                try
                {
                    string destionationFolder = _testsFolder + "/" + test.Problem.ID + "/";
                    if (!String.IsNullOrWhiteSpace(test.OKLocation) && File.Exists(destionationFolder + test.OKLocation))
                    {
                        File.Delete(destionationFolder + test.OKLocation);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Logger.Error(MethodBase.GetCurrentMethod().DeclaringType, MethodBase.GetCurrentMethod().Name, ex);
                }
            }
        }        

        public bool MatchAndSaveTestFile(int problemId, string fileName, Stream stream)
        {
            var problem = unit.ProblemsRepository.Get(a => a.ID == problemId, null, "Tests").FirstOrDefault();
            if (problem != null && !String.IsNullOrWhiteSpace(fileName))
            {
                fileName = fileName.ToLower();
                var test = problem.Tests.FirstOrDefault(a => fileName.Contains(a.Name.ToLower()));
                if (test != null)
                {                                      
                    if (fileName.EndsWith(".in"))
                    {
                        SaveTestInputFile(stream, test.ID);
                        return true;
                    }
                    else if (fileName.EndsWith(".ok"))
                    {
                        SaveTestOKFile(stream, test.ID);
                        return true;
                    }                    
                }                
            }
            return false;
        }

        public Status GetStatusById(int id)
        {
            return unit.StatusesRepository.GetById(id);
        }

        public Test GetTestById(int id)
        {
            return unit.TestsRepository.GetById(id);
        }
    }
}
