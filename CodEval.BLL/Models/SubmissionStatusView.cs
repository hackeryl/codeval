﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class SubmissionStatusView
    {
        public int ID_Status { get; set; }
        public SubmissionStatus Status { get; set; }
        public string Status_Name { get; set; }

        public SubmissionStatusView(SubmissionStatus status, string statusName)
        {
            Status = status;
            ID_Status = (int)status;
            Status_Name = statusName;
        }
    }
}
