﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class ProblemName
    {
        public int ID_Problem { get; set; }
        public string Problem_Name { get; set; }
        public ProblemName(int idProblem, string problemName)
        {
            ID_Problem = idProblem;
            Problem_Name = problemName;
        }
    }
}