﻿using System.Collections.Generic;

namespace CodEval.BLL.Models.Recommender
{
    /// <summary>
    /// Article.
    /// </summary>
    public class Article
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the authors.
        /// </summary>
        /// <value>
        /// The authors.
        /// </value>
        public IList<string> Authors { get; set; }

        /// <summary>
        /// Gets or sets the publication date time.
        /// </summary>
        /// <value>
        /// The publication date time.
        /// </value>
        public string PublicationDate { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Article"/> class.
        /// </summary>
        public Article()
        {
            Authors = new List<string>();
        }
    }
}
