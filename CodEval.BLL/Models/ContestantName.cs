﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class ContestantName
    {
        public int ID_Contestant { get; set; }
        public string ContestantFullName { get; set; }
        public ContestantName(int idContestant, string contestantFirstName, string contestantLastName)
        {
            ID_Contestant = idContestant;
            ContestantFullName = contestantFirstName + " " + contestantLastName;
        }
    }
}
