﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class ProblemSubmission
    {
        public int ProblemId { get; set; }
        public string ProblemName { get; set; }
        public string SolutionName { get; set; }
        public DateTime? AcceptedTime { get; set; }
        public string AcceptedTimeString { get; set; }
        public int SubmissionId { get; set; }

        public ProblemSubmission(int problemId, string problemName, string solutionName, DateTime? acceptedTime, int submissionId)
        {
            ProblemId = problemId;
            ProblemName = problemName;
            SolutionName = solutionName;
            AcceptedTime = acceptedTime;
            AcceptedTimeString = acceptedTime.HasValue ? acceptedTime.Value.ToShortTimeString() : String.Empty;
            SubmissionId = submissionId;
        }
    }
}