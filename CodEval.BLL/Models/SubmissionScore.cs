﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class SubmissionScore
    {
        public int ID_Submission { get; set; }
        public int? Submission_Score { get; set; }

        public SubmissionScore(int submissionId, int? submissionScore)
        {
            ID_Submission = submissionId;
            Submission_Score = submissionScore;
        }
    }
}
