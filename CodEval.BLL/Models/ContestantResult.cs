﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class ContestantResult
    {
        public int ID { get; set; }
        public int ID_Contest { get; set; }
        public string ProblemName { get; set; }
        public string TestName { get; set; }
        public string Status { get; set; }
        public decimal Time { get; set; }
        public int Score { get; set; }
        public DateTime EvaluationTime { get; set; }
        public string EvaluationTimeString { get; set; }

        public ContestantResult(int id, int contestId, string problemName, string testName, string status, decimal time, int score, DateTime evaluationTime)
        {
            ID = id;
            ID_Contest = contestId;
            ProblemName = problemName;
            TestName = testName;
            Status = status;
            Time = time;
            Score = score;
            EvaluationTime = evaluationTime;
            EvaluationTimeString = evaluationTime.ToShortTimeString();
        }
    }
}