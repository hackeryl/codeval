﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class ContestName
    {
        public int ID_Contest { get; set; }
        public string Contest_Name { get; set; }
        
        public ContestName(int idContest, string contestName)
        {
            ID_Contest = idContest;
            Contest_Name = contestName;
        }
    }
}
