﻿namespace CodEval.BLL.Models.Similarity
{
    /// <summary>
    /// Match.
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the start index of the source code1.
        /// </summary>
        /// <value>
        /// The start index of the source code1.
        /// </value>
        public int SourceCode1StartIndex { get; set; }

        /// <summary>
        /// Gets or sets the start index of the source code2.
        /// </summary>
        /// <value>
        /// The start index of the source code2.
        /// </value>
        public int SourceCode2StartIndex { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>
        /// The length.
        /// </value>
        public int Length { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Match"/> class.
        /// </summary>
        /// <param name="sourceCode1StartIndex">Start index of the source code1.</param>
        /// <param name="sourceCode2StartIndex">Start index of the source code2.</param>
        /// <param name="length">The length.</param>
        public Match(int sourceCode1StartIndex, int sourceCode2StartIndex, int length)
        {
            SourceCode1StartIndex = sourceCode1StartIndex;
            SourceCode2StartIndex = sourceCode2StartIndex;
            Length = length;
        }
    }
}
