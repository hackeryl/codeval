﻿namespace CodEval.BLL.Models.Similarity
{
    /// <summary>
    /// Token.
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Gets or sets the original value.
        /// </summary>
        /// <value>
        /// The original value.
        /// </value>
        public string OriginalValue { get; set; }

        /// <summary>
        /// Gets or sets the token value.
        /// </summary>
        /// <value>
        /// The token value.
        /// </value>
        public string TokenValue { get; set; }

        /// <summary>
        /// Gets or sets the line number.
        /// </summary>
        /// <value>
        /// The line.
        /// </value>
        public int LineNumber { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Token"/> class.
        /// </summary>
        public Token()
            : this(string.Empty, string.Empty, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Token" /> class.
        /// </summary>
        /// <param name="originalValue">The original value.</param>
        /// <param name="tokenValue">The token value.</param>
        /// <param name="lineNumber">The line number.</param>
        public Token(string originalValue, string tokenValue, int lineNumber)
        {
            OriginalValue = originalValue;
            TokenValue = tokenValue;
            LineNumber = lineNumber;
        }
    }
}
