﻿namespace CodEval.BLL.Models.Similarity
{
    /// <summary>
    /// Source Code.
    /// </summary>
    public class SourceCode
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the programming language.
        /// </summary>
        /// <value>
        /// The programming language.
        /// </value>
        public string ProgrammingLanguage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceCode"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="code">The code.</param>
        /// <param name="programmingLanguage">The programming language.</param>
        public SourceCode(int id, string code, string programmingLanguage)
        {
            Id = id;
            Code = code;
            ProgrammingLanguage = programmingLanguage;
        }
    }
}
