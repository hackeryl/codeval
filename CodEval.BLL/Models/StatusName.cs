﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class StatusName
    {
        public int ID_Status { get; set; }
        public string Status_Name { get; set; }
        public string Status_Color { get; set; }
        public StatusName(int idStatus, string statusName, string statusColor)
        {
            ID_Status = idStatus;
            Status_Name = statusName;
            Status_Color = statusColor;
        }
    }
}