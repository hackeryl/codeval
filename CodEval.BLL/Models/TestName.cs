﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class TestName
    {
        public int ID_Test { get; set; }
        public string Test_Name { get; set; }
        public TestName(int idTest, string testName)
        {
            ID_Test = idTest;
            Test_Name = testName;
        }
    }
}