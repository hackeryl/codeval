﻿using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class ProblemSubmissionResult
    {
        public int ID { get; set; }
        public int ID_Contest { get; set; }
        public int ID_Contestant { get; set; }
        public int ID_Problem { get; set; }
        public int ID_Submission { get; set; }
        public int ID_Test { get; set; }
        public int Score { get; set; }
        public decimal Time { get; set; }
        public decimal Memory { get; set; }
        public int ID_Status { get; set; }
        public System.DateTime EvaluationTime { get; set; }

        public ProblemSubmissionResult(SubmissionResult result, int contestId)
        {
            this.ID = result.ID;
            this.ID_Contest = contestId;
            this.ID_Contestant = result.Submission.ID_Contestant;
            this.ID_Problem = result.Submission.ID_Problem;
            this.ID_Submission = result.ID_Submission;
            this.ID_Test = result.ID_Test;
            this.Score = result.Score;
            this.Time = result.Time;
            this.Memory = result.Memory;
            this.ID_Status = result.ID_Status;
            this.EvaluationTime = result.EvaluationTime;
        }

        public ProblemSubmissionResult()
        {
        }
    }
}
