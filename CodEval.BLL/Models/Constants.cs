﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.BLL.Models
{
    public class Constants
    {
        public static readonly string pas = ".pas";
        public static readonly string cpp = ".cpp";
        public static readonly string c = ".c";
        public static readonly string _in = ".in";
        /// <summary>
        /// 1 MB
        /// </summary>
        public static readonly int MaxFileSize = 1048576;
    }
}