﻿using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class SubmissionName
    {
        public int ID_Submission { get; set; }
        public string Submission_Name { get; set; }

        public SubmissionName(Submission submission)
        {
            this.ID_Submission = submission.ID;
            this.Submission_Name = submission.Contestant.FirstName + " " + submission.Contestant.LastName + "-" + submission.Problem.Name + "-" + submission.SubmissionTime.ToString();
        }

        public SubmissionName()
        {

        }
    }
}
