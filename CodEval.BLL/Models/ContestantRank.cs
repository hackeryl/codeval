﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class ContestantRank
    {
        public string ContestantName {get;set;}
        public int Score { get; set; }
        public int Rank { get; set; }
        public ContestantRank(string contestantName, int score, int rank)
        {
            ContestantName = contestantName;
            Score = score;
            Rank = rank;
        }
    }
}
