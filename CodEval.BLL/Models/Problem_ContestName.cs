﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodEval.BLL.Models
{
    public class Probem_ContestName
    {
        public int ID_Problem { get; set; }
        public int ID_Contest { get; set; }
        public string Contest_Name { get; set; }
        public string Problem_Name { get; set; }

        public Probem_ContestName(int idProblem, int idContest, string contestName, string problemName)
        {
            ID_Problem = idProblem;
            ID_Contest = idContest;
            Contest_Name = contestName;
            Problem_Name = problemName;
        }
    }
}
