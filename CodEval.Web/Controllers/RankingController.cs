﻿using CodEval.BLL.Models;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class RankingController : Controller
    {
        private ContestsService _contestsService;
        
        public RankingController()
            : this(new CodEvalEntities())
        {
        }

        public RankingController(DbContext context)
        {
            _contestsService = new ContestsService(context);                        
        }

        public ActionResult Index(int? contestId)
        {
            if (!contestId.HasValue)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ContestName = _contestsService.GetContestNameById(contestId.Value);
            return View(contestId.Value);
        }

        public ActionResult GetRankings([DataSourceRequest] DataSourceRequest request, int contestId)
        {
            return Json(_contestsService.GetRankingsForContest(contestId).ToDataSourceResult(request));
        }
    }
}
