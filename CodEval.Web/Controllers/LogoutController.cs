﻿using CodEval.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class LogoutController : Controller
    {
        public ActionResult Index()
        {
            SessionHelper.CurrentContestant = null;
            SessionHelper.CurrentAdmin = null;
            return RedirectToAction("Index", "Login");
        }
    }
}
