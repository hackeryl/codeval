﻿using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class QuestionsController : Controller
    {
        private ContestantsService _contestantsService;

        public QuestionsController()
            : this(new CodEvalEntities())
        {
        }

        public QuestionsController(DbContext context)
        {
            _contestantsService = new ContestantsService(context);
        }

        public ActionResult Index()
        {
            ViewBag.Message = "Your question page.";
            return View();
        }

        public ActionResult GetQuestions([DataSourceRequest] DataSourceRequest request)
        {
            if (SessionHelper.CurrentContestant != null)
            {
                return Json(_contestantsService.GetContestantQuestions(SessionHelper.CurrentContestant.ID).ToDataSourceResult(request));
            }
            return Json((new List<Question>()).ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult SendQuestion(string question)
        {
            if (SessionHelper.CurrentContestant != null && !String.IsNullOrEmpty(question) && question.Length < 3072)
            {
                return Json(_contestantsService.SubmitQuestion(SessionHelper.CurrentContestant.ID, question));
            }
            return Json(false);
        }
    }
}
