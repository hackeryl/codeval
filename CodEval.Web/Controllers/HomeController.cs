﻿using CodEval.BLL.Services;
using CodEval.Web.Helpers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodEval.BLL.Models;
using CodEval.DAL.Repository;
using System.IO;
using System.Data.Entity;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class HomeController : Controller
    {
        private ContestsService _contestsService;
        private ProblemsService _problemsService;

        public HomeController()
            : this(new CodEvalEntities())
        {
        }

        public HomeController(DbContext context)
        {
            _contestsService = new ContestsService(context);
            _problemsService = new ProblemsService(context);            
        }

        #region Contests

        public ActionResult Index(int? contestId)
        {
            if (contestId.HasValue)
            {
                ViewBag.ContestName = _contestsService.GetContestNameById(contestId.Value);
                ViewBag.IsActiveContest = _contestsService.IsActiveContest(contestId.Value);
                ViewBag.RemainingTime = _contestsService.GetContestRemainingTime(contestId.Value);
                ViewBag.AcceptedProgrammingLanguages = _contestsService.GetContestProgrammingLanguages(contestId.Value);
                ViewBag.ContestDescription = _contestsService.GetContestDescription(contestId.Value);
            }
            return View(contestId);
        }        

        public ActionResult GetContests([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_contestsService.GetAllContests().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }        

        public ActionResult GetProblems([DataSourceRequest] DataSourceRequest request, int contestId)
        {
            if (SessionHelper.CurrentContestant != null)
            {
                return Json(_problemsService.GetContestProblems(SessionHelper.CurrentContestant.ID, contestId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            return Json((new List<ProblemSubmission>()).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }        
        
        #endregion        
    }
}
