﻿using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class ProblemController : Controller
    {
        private ProblemsService _problemsService;

        public ProblemController()
            : this(new CodEvalEntities())
        {
        }

        public ProblemController(DbContext context)
        {
            _problemsService = new ProblemsService(context);
        }

        public ActionResult Index(int? id)
        {
            if (id.HasValue)
            {
                var problem = _problemsService.GetFullProblemDataById(id.Value);
                if (problem != null)
                {
                    if (problem.Contest.StartDate <= DateTime.Now)
                    {
                        return View(problem);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home", new { contestId = problem.Id_Contest });
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Review(ProblemReview review)
        {
            review.Id_Contestant = SessionHelper.CurrentContestant.ID;
            review.ReviewDate = DateTime.Now;
            _problemsService.AddProblemReview(review);
            return RedirectToAction("Index", new { id = review.Id_Problem });
        }
    }
}
