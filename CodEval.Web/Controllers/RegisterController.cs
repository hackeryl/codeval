﻿using CodEval.BLL.Models;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodEval.Web.Controllers
{
    public class RegisterController : Controller
    {
        private ContestantsService contestantsHelper;

        public RegisterController()
            : this(new CodEval.DAL.Repository.CodEvalEntities())
        {
        }

        public RegisterController(DbContext context)
        {
            contestantsHelper = new ContestantsService(context);
        }
        
        public ActionResult Index()
        {
            ViewBag.IsLoginPage = true;
            return View();
        }

        [HttpPost]
        public ActionResult Index(RegisterModel user)
        {
            ViewBag.IsLoginPage = true;
            if (ModelState.IsValid)
            {
                Contestant contestant;
                if (contestantsHelper.RegisterContestant(user.FirstName, user.LastName, user.Email, user.UserName, user.Password, out contestant))
                {
                    SessionHelper.CurrentContestant = contestant;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Register data is incorrect!");
                }
            }
            return View(user);
        }
    }
}
