﻿using System.Data.Entity;
using System.Web.Mvc;
using CodEval.BLL.Services.Recommender;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;

namespace CodEval.Web.Controllers
{
    /// <summary>
    /// RecommenderController.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    [Authenticated]
    public class RecommenderController : Controller
    {
        /// <summary>
        /// The problem recommender
        /// </summary>
        private readonly IRecommender problemRecommender;

        /// <summary>
        /// The articles recommender
        /// </summary>
        private readonly IRecommender articlesRecommender;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecommenderController"/> class.
        /// </summary>
        public RecommenderController()
            : this(new CodEvalEntities() { Configuration = { ProxyCreationEnabled = true } })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecommenderController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public RecommenderController(DbContext context)
        {
            this.problemRecommender = new ProblemRecommender(context);
            this.articlesRecommender = new ArticlesRecommender(context);
        }

        /// <summary>
        /// Problemses the specified problem identifier.
        /// </summary>
        /// <param name="problemId">The problem identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Problems(int problemId)
        {
            return Json(problemRecommender.GetRecommendationsForProblem(SessionHelper.CurrentContestant.ID, problemId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Articleses the specified problem identifier.
        /// </summary>
        /// <param name="problemId">The problem identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Articles(int problemId)
        {
            return Json(articlesRecommender.GetRecommendationsForProblem(SessionHelper.CurrentContestant.ID, problemId), JsonRequestBehavior.AllowGet);
        }
    }
}
