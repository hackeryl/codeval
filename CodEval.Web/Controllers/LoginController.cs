﻿using CodEval.BLL.Models;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodEval.Web.Controllers
{
    public class LoginController : Controller
    {
        private ContestantsService contestantsService;

        public LoginController()
            : this(new CodEval.DAL.Repository.CodEvalEntities())
        {
        }

        public LoginController(DbContext context)
        {            
            contestantsService = new ContestantsService(context);
        }

        public ActionResult Index()
        {
            ViewBag.IsLoginPage = true;
            return View();
        }

        [HttpPost]
        public ActionResult Index(User user)
        {
            ViewBag.IsLoginPage = true;
            if (ModelState.IsValid)
            {
                Contestant contestant;
                if (contestantsService.IsRegisteredContestant(user.UserName, user.Password, out contestant))
                {
                    SessionHelper.CurrentContestant = contestant;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            return View(user);
        }
    }
}
