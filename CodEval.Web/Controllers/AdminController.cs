﻿using CodEval.BLL.Models;
using CodEval.BLL.Services;
using CodEval.DAL.Repository;
using CodEval.Web.Helpers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using CodEval.BLL.Models.Similarity;
using CodEval.BLL.Services.Similarity;

namespace CodEval.Web.Controllers
{
    public class AdminController : Controller
    {
        private static readonly string _submissionsFolder = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Submissions/");
        private static readonly string _testsFolder = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Tests/");

        private AdminsService adminsService;
        private ProblemsService problemsService;
        private TestsService testsService;
        private ContestsService contestsService;
        private ContestantsService contestantsService;
        private SourceCodeTokenizersFactory sourceCodeTokenizersFactory;
        private ISourceCodeSimilarityDetector sourceCodeSimilarityDetector;

        public AdminController()
            : this(new CodEval.DAL.Repository.CodEvalEntities())
        {
        }

        public AdminController(DbContext context)
        {
            adminsService = new AdminsService(context, _submissionsFolder);
            testsService = new TestsService(context, _testsFolder);
            contestsService = new ContestsService(context);
            contestantsService = new ContestantsService(context);
            problemsService = new ProblemsService(context);
            sourceCodeTokenizersFactory = new SourceCodeTokenizersFactory();
            sourceCodeSimilarityDetector = new SourceCodeSimilarityDetector(sourceCodeTokenizersFactory);
        }

        [AuthenticatedAdminAttribute]
        public ActionResult Index()
        {
            ViewBag.ApiKey = SessionHelper.CurrentAdmin.ApiKey;
            return View();
        }

        #region Login
        [Helpers.AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Helpers.AllowAnonymous]
        public ActionResult Login(User user)
        {
            ViewBag.IsLoginPage = true;
            if (ModelState.IsValid)
            {
                Admin admin;
                if (adminsService.IsRegisteredAdmin(user.UserName, user.Password, out admin))
                {
                    SessionHelper.CurrentAdmin = admin;
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }
            }
            return View(user);
        }
        #endregion

        #region Register
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel user, string AdministratorSecurityKey)
        {
            if (ModelState.IsValid && Properties.Settings.Default.AdministratorSecurityKey == AdministratorSecurityKey)
            {
                Admin admin;
                if (adminsService.RegisterAdmin(user.FirstName, user.LastName, user.Email, user.UserName, user.Password, out admin))
                {
                    SessionHelper.CurrentAdmin = admin;
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    ModelState.AddModelError("", "Register data is incorrect!");
                }
            }
            return View(user);
        }
        #endregion

        #region Contests
        [AuthenticatedAdminAttribute]
        public ActionResult GetContests([DataSourceRequest] DataSourceRequest request)
        {
            return Json(contestsService.GetAllContests().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddContest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contest> contests)
        {
            var results = new List<Contest>();
            if (contests != null && ModelState.IsValid)
            {
                foreach (var comp in contests)
                {
                    comp.Id_Admin = SessionHelper.CurrentAdmin.ID;
                    if (contestsService.AddContest(comp))
                    {
                        results.Add(comp);
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateContest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contest> contests)
        {
            if (contests != null && ModelState.IsValid)
            {
                foreach (var comp in contests)
                {
                    comp.Id_Admin = SessionHelper.CurrentAdmin.ID;
                    contestsService.UpdateContest(comp);
                }
            }

            return Json(contests.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteContest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contest> contests)
        {
            if (contests.Any())
            {
                foreach (var comp in contests)
                {
                    contestsService.DeleteContest(comp, _submissionsFolder, _testsFolder);
                }
            }

            return Json(contests.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DownloadSubmissions(int? contestId)
        {
            if (contestId.HasValue)
            {
                var zip = adminsService.GenerateSubmissionsZip(contestId.Value);
                if (zip != null)
                {
                    return File(zip, "application/zip", "SurseConcurenti.zip");
                }
            }
            return RedirectToAction("Index", "Admin");
        }
        [AuthenticatedAdminAttribute]
        public ActionResult GetContestProgrammingLanguages([DataSourceRequest] DataSourceRequest request, int contestId)
        {
            return Json(contestsService.GetContestProgrammingLanguages(contestId).ToDataSourceResult(request));
        }
        #endregion

        #region Contestants
        [AuthenticatedAdminAttribute]
        public ActionResult Users()
        {
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetContestants([DataSourceRequest] DataSourceRequest request)
        {
            return Json(contestantsService.GetAllContestants().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddContestant([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contestant> contestants)
        {
            var results = new List<Contestant>();
            if (contestants != null && ModelState.IsValid)
            {
                foreach (var comp in contestants)
                {
                    Contestant contestant = null;
                    contestantsService.RegisterContestant(comp.FirstName, comp.LastName, comp.Email, comp.Username, comp.Password, out contestant);
                    if (contestant != null)
                    {
                        results.Add(comp);
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateContestant([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contestant> contestants)
        {
            if (contestants != null && ModelState.IsValid)
            {
                foreach (var comp in contestants)
                {
                    contestantsService.UpdateContestant(comp);
                    comp.DefaultPassword = null;
                }
            }

            return Json(contestants.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteContestant([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Contestant> contestants)
        {
            if (contestants.Any())
            {
                foreach (var comp in contestants)
                {
                    contestantsService.DeleteContestant(comp);
                }
            }

            return Json(contestants.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult GenerateTemporaryPassowrds()
        {
            var contestantsTemporaryPasswords = contestantsService.GenerateTemporaryPassowrds();
            var pdf = adminsService.GenerateTemporaryPassowrdsPdf(contestantsTemporaryPasswords);
            if (pdf != null)
            {
                return File(pdf, "application/pdf", "Passwords.pdf");
            }
            return RedirectToAction("Users");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult ResetDefaultPassowrds()
        {
            contestantsService.ResetDefaultPassowrds();
            return RedirectToAction("Users");
        }
        #endregion

        #region Submissions
        [AuthenticatedAdminAttribute]
        public ActionResult Submissions()
        {
            var problem_contest = contestsService.GetAllProblem_ContestsNames();
            var submissionsStatuses = contestsService.GetAllSubmissionsStatuses();
            var submissionsScores = contestsService.GetSubmissionsScores();
            var contestants = contestantsService.GetAllContestantsNames();

            ViewData["contestants"] = contestants;
            ViewData["problem_contest"] = problem_contest;
            ViewData["submissionsStatuses"] = submissionsStatuses;
            ViewData["submissionsScores"] = submissionsScores;
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetSubmissions([DataSourceRequest] DataSourceRequest request)
        {
            return Json(contestsService.GetAllSubmissions().ToDataSourceResult(request));
        }
        #endregion

        #region SubmissionsResults
        [AuthenticatedAdminAttribute]
        public ActionResult SubmissionsResults()
        {
            var contests = contestsService.GetAllContestsNames();
            ViewData["contests"] = contests;
            ViewBag.DefaultContest = contests.Count > 0 ? contests.First().ID_Contest : 0;
            var contestants = contestantsService.GetAllContestantsNames();
            ViewData["contestants"] = contestants;
            ViewBag.DefaultContestant = contestants.Count > 0 ? contestants.First().ID_Contestant : 0;
            var problems = problemsService.GetAllProblemsNames();
            ViewData["problems"] = problems;
            ViewBag.DefaultProblem = problems.Count > 0 ? problems.First().ID_Problem : 0;
            var submissions = contestsService.GetAllSubmissionsNames();
            ViewData["submissions"] = submissions;
            ViewBag.DefaultSubmission = submissions.Count > 0 ? submissions.First().ID_Submission : 0;
            var tests = testsService.GetAllTestsNames();
            ViewData["tests"] = tests;
            ViewBag.DefaultTest = tests.Count > 0 ? tests.First().ID_Test : 0;
            var statuses = testsService.GetAllStatusesNames();
            ViewData["statuses"] = statuses;
            ViewBag.DefaultStatus = statuses.Count > 0 ? statuses.First().ID_Status : 0;
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetSubmissionsResults([DataSourceRequest] DataSourceRequest request)
        {
            return Json(contestantsService.GetAllSubmissionsResults().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddSubmissionResult([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CodEval.BLL.Models.ProblemSubmissionResult> results)
        {
            var res = new List<ProblemSubmissionResult>();
            if (results != null)// && ModelState.IsValid)
            {
                TimeZone zone = TimeZone.CurrentTimeZone;
                foreach (var r in results)
                {
                    SubmissionResult sr = new SubmissionResult()
                    {
                        EvaluationTime = zone.ToUniversalTime(DateTime.Now).AddHours(3),
                        ID_Status = r.ID_Status,
                        ID_Submission = r.ID_Submission,
                        ID_Test = r.ID_Test,
                        Memory = r.Memory,
                        Score = r.Score,
                        Time = r.Time
                    };
                    if (contestantsService.AddResult(sr))
                    {
                        res.Add(r);
                    }
                }
            }
            return Json(res.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateSubmissionResult([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubmissionResult> results)
        {
            if (results != null && ModelState.IsValid)
            {
                foreach (var r in results)
                {
                    contestantsService.UpdateResult(r);
                }
            }

            return Json(new List<CodEval.BLL.Models.ProblemSubmissionResult>().ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteSubmissionResult([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SubmissionResult> results)
        {
            if (results.Any())
            {
                foreach (var r in results)
                {
                    contestantsService.DeleteResult(r);
                }
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult ExportContestRakingToPDF(int? contestId)
        {
            if (contestId.HasValue)
            {
                return File(adminsService.GenerateContestRankingPDF(contestId.Value), "application/pdf", "Ranking.pdf");
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult ExportContestIndividualRakingsToPDF(int? contestId)
        {
            if (contestId.HasValue)
            {
                return File(adminsService.GenerateContestIndividualRakingsPDF(contestId.Value), "application/zip", "Rankings.zip");
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult ReEvaluateSubmissions(int? contestId)
        {
            if (contestId.HasValue)
            {
                contestsService.SetContestSubmissionsStatus(contestId.Value, SubmissionStatus.Submitted);
            }
            return RedirectToAction("SubmissionsResults");
        }
        #endregion

        #region Questions
        [AuthenticatedAdminAttribute]
        public ActionResult Questions()
        {
            ViewData["contestants"] = contestantsService.GetAllContestantsNames();
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetQuestions([DataSourceRequest] DataSourceRequest request)
        {
            return Json(contestantsService.GetAllQuestions().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateQuestion([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Question> questions)
        {
            if (questions != null && ModelState.IsValid && SessionHelper.CurrentAdmin != null)
            {
                foreach (var q in questions)
                {
                    contestantsService.UpdateQuestion(q, SessionHelper.CurrentAdmin.ID);
                }
            }

            return Json(questions.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteQuestion([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Question> questions)
        {
            if (questions.Any())
            {
                foreach (var q in questions)
                {
                    contestantsService.DeleteQuestion(q);
                }
            }

            return Json(questions.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Status
        [AuthenticatedAdminAttribute]
        public ActionResult Statuses()
        {
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetStatuses([DataSourceRequest] DataSourceRequest request)
        {
            return Json(testsService.GetAllStatuses().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddStatus([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Status> statuses)
        {
            var results = new List<Status>();
            if (statuses != null && ModelState.IsValid)
            {
                foreach (var stat in statuses)
                {
                    if (String.IsNullOrEmpty(stat.Code) || String.IsNullOrEmpty(stat.Name) || String.IsNullOrWhiteSpace(stat.Color))
                    {
                        continue;
                    }
                    if (testsService.AddStatus(stat))
                    {
                        results.Add(stat);
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateStatus([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Status> statuses)
        {
            if (statuses != null && ModelState.IsValid)
            {
                foreach (var stat in statuses)
                {
                    testsService.UpdateStatus(stat);
                }
            }

            return Json(statuses.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteStatus([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Status> statuses)
        {
            if (statuses.Any())
            {
                foreach (var stat in statuses)
                {
                    testsService.DeleteStatus(stat);
                }
            }

            return Json(statuses.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Problems
        [AuthenticatedAdminAttribute]
        public ActionResult Problems(int? contestId)
        {
            if (!contestId.HasValue)
            {
                return RedirectToAction("Index");
            }
            ViewBag.ContestName = contestsService.GetContestNameById(contestId.Value);
            return View(contestId.Value);
        }

        [AuthenticatedAdminAttribute]
        public ActionResult Problem(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index");
            }
            var problem = problemsService.GetFullProblemDataById(id.Value);
            if (problem != null)
            {
                ViewBag.ProblemCategories = problemsService.GetProblemCategories();
                return View(problem);
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult Problem(Problem problem)
        {
            if (problem != null)
            {
                problem.Body = HttpUtility.HtmlDecode(problem.Body);
                problem.Input = HttpUtility.HtmlDecode(problem.Input);
                problem.Output = HttpUtility.HtmlDecode(problem.Output);
                problem.Restrictions = HttpUtility.HtmlDecode(problem.Restrictions);
                problem.Examples = HttpUtility.HtmlDecode(problem.Examples);
                if (problem.ID == 0)
                {
                    problemsService.AddProblem(problem);
                }
                else
                {
                    problemsService.UpdateProblem(problem);
                }
                return RedirectToAction("Problem", new { id = problem.ID });
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateProblemCategories(int problemId, List<int> problemCategoryIds)
        {
            var problem = problemsService.GetFullProblemDataById(problemId);
            if (problem != null)
            {
                problemCategoryIds = problemCategoryIds ?? new List<int>();
                var currentProblemCategories = problem.ProblemCategories.ToList();
                foreach (var currentProblemCategory in currentProblemCategories)
                {
                    if (!problemCategoryIds.Contains(currentProblemCategory.ID))
                    {
                        problem.ProblemCategories.Remove(currentProblemCategory);
                    }
                }

                var categories = problemsService.GetProblemCategories();
                foreach (var problemCategoryId in problemCategoryIds)
                {
                    if (!problem.ProblemCategories.Any(pc => pc.ID == problemCategoryId))
                    {
                        problem.ProblemCategories.Add(categories.FirstOrDefault(c => c.ID == problemCategoryId));
                    }
                }

                problemsService.UpdateProblem(problem);
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetProblems([DataSourceRequest] DataSourceRequest request, int? contestId)
        {
            if (contestId.HasValue)
            {
                return Json(problemsService.GetAllProblemsByContestId(contestId.Value).ToDataSourceResult(request));
            }
            else
            {
                return Json(new List<Problem>().ToDataSourceResult(request));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddProblem([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Problem> problems, int? contestId)
        {
            var results = new List<Problem>();
            if (contestId.HasValue)
            {
                if (problems != null && ModelState.IsValid)
                {
                    foreach (var prob in problems)
                    {
                        if (String.IsNullOrWhiteSpace(prob.Body))
                        {
                            prob.Body = String.Empty;
                        }
                        if (String.IsNullOrWhiteSpace(prob.Input))
                        {
                            prob.Input = String.Empty;
                        }
                        if (String.IsNullOrWhiteSpace(prob.Output))
                        {
                            prob.Output = String.Empty;
                        }
                        prob.Id_Contest = contestId.Value;
                        prob.Id_Admin = SessionHelper.CurrentAdmin.ID;
                        if (problemsService.AddProblem(prob))
                        {
                            //results.Add(prob);
                            testsService.UpdateProblemTests(prob.ID);
                        }
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateProblem([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Problem> problems)
        {
            if (problems != null && ModelState.IsValid)
            {
                foreach (var prob in problems)
                {
                    if (String.IsNullOrWhiteSpace(prob.Body))
                    {
                        prob.Body = String.Empty;
                    }
                    if (String.IsNullOrWhiteSpace(prob.Input))
                    {
                        prob.Input = String.Empty;
                    }
                    if (String.IsNullOrWhiteSpace(prob.Output))
                    {
                        prob.Output = String.Empty;
                    }
                    prob.Id_Admin = SessionHelper.CurrentAdmin.ID;
                    problemsService.UpdateProblem(prob);
                    testsService.UpdateProblemTests(prob.ID);
                }
            }

            return Json(new List<Problem>().ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteProblem([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Problem> problems)
        {
            if (problems.Any())
            {
                foreach (var prob in problems)
                {
                    testsService.DeleteProblemTests(prob.ID);
                    problemsService.DeleteProblem(prob);
                    //problemsService.DeleteProblemTests(prob.ID);
                }
            }

            return Json(problems.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region Tests
        [AuthenticatedAdminAttribute]
        public ActionResult Tests(int? problemId)
        {
            if (!problemId.HasValue)
            {
                return RedirectToAction("Index");
            }
            ViewBag.ProblemName = problemsService.GetProblemNameById(problemId.Value);
            return View(problemId.Value);
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetTests([DataSourceRequest] DataSourceRequest request, int? problemId)
        {
            if (problemId.HasValue)
            {
                return Json(testsService.GetAllTestsByProblemId(problemId.Value).ToDataSourceResult(request));
            }
            else
            {
                return Json(new List<Test>().ToDataSourceResult(request));
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddTest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Test> tests, int? problemId)
        {
            var results = new List<Test>();
            if (problemId.HasValue)
            {
                if (tests != null && ModelState.IsValid)
                {
                    foreach (var test in tests)
                    {
                        if (String.IsNullOrEmpty(test.InputLocation)) test.InputLocation = String.Empty;
                        if (String.IsNullOrEmpty(test.OKLocation)) test.OKLocation = String.Empty;
                        test.ID_Problem = problemId.Value;
                        if (testsService.AddTest(test))
                        {
                            results.Add(test);
                        }
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateTest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Test> tests)
        {
            if (tests != null && ModelState.IsValid)
            {
                foreach (var test in tests)
                {
                    if (String.IsNullOrEmpty(test.InputLocation)) test.InputLocation = String.Empty;
                    if (String.IsNullOrEmpty(test.OKLocation)) test.OKLocation = String.Empty;
                    testsService.UpdateTest(test);
                }
            }

            return Json(tests.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteTest([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Test> tests)
        {
            if (tests.Any())
            {
                foreach (var test in tests)
                {
                    testsService.DeleteTest(test);
                }
            }

            return Json(tests.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        [AuthenticatedAdminAttribute]
        public ActionResult UploadTestInputFile(IEnumerable<HttpPostedFileBase> files, int testId)
        {
            foreach (var file in files)
            {
                if (testsService.SaveTestInputFile(file.InputStream, testId))
                {
                    return Content(String.Empty);
                }
            }
            return Content("Invalid file!");
        }

        [HttpPost]
        [AuthenticatedAdminAttribute]
        public ActionResult UploadTestOKFile(IEnumerable<HttpPostedFileBase> files, int testId)
        {
            foreach (var file in files)
            {
                if (testsService.SaveTestOKFile(file.InputStream, testId))
                {
                    return Content(String.Empty);
                }
            }
            return Content("Invalid file!");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DownloadTest(int? testId)
        {
            if (testId.HasValue)
            {
                return File(testsService.GenerateTestZip(testId.Value), "application/zip", "Test.zip");
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DownloadTests(int? problemId)
        {
            if (problemId.HasValue)
            {
                return File(testsService.GenerateTestsZip(problemId.Value), "application/zip", "Tests.zip");
            }
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UploadTests(IEnumerable<HttpPostedFileBase> files, int? problemId)
        {
            int matchCount = 0;
            if (problemId.HasValue)
            {
                foreach (var testFile in files)
                {
                    if (testsService.MatchAndSaveTestFile(problemId.Value, testFile.FileName, testFile.InputStream))
                    {
                        matchCount++;
                    }
                }
            }
            if (matchCount > 0)
            {
                return Json(new { OK = true, Message = "Test Files Uploaded!\n" + matchCount + " files were matched and saved!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { OK = false, Message = "No Files Matched with the tests!" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Problem Categories
        [AuthenticatedAdminAttribute]
        public ActionResult ProblemCategories()
        {
            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult GetProblemCategories([DataSourceRequest] DataSourceRequest request)
        {
            return Json(problemsService.GetProblemCategories().ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult AddProblemCategory([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProblemCategory> problemCategories)
        {
            var results = new List<ProblemCategory>();
            if (problemCategories != null && ModelState.IsValid)
            {
                foreach (var problemCategory in problemCategories)
                {
                    if (String.IsNullOrEmpty(problemCategory.Name))
                    {
                        continue;
                    }
                    if (problemsService.AddProblemCategory(problemCategory))
                    {
                        results.Add(problemCategory);
                    }
                }
            }
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult UpdateProblemCategory([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProblemCategory> problemCategories)
        {
            if (problemCategories != null && ModelState.IsValid)
            {
                foreach (var problemCategory in problemCategories)
                {
                    problemsService.UpdateProblemCategory(problemCategory);
                }
            }

            return Json(problemCategories.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AuthenticatedAdminAttribute]
        public ActionResult DeleteProblemCategory([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProblemCategory> problemCategories)
        {
            if (problemCategories.Any())
            {
                foreach (var problemCategory in problemCategories)
                {
                    problemsService.DeleteProblemCategory(problemCategory);
                }
            }

            return Json(problemCategories.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Similarities

        /// <summary>
        /// Similaritieses the specified contest identifier.
        /// </summary>
        /// <param name="contestId">The contest identifier.</param>
        /// <param name="problemId">The problem identifier.</param>
        /// <param name="submissionIds">The submission ids.</param>
        /// <returns></returns>
        [AuthenticatedAdminAttribute]
        public ActionResult Similarities(int? contestId, int? problemId)
        {
            ViewBag.SelectedCotestId = contestId ?? -1;
            ViewBag.SelectedProblemId = problemId ?? -1;
            ViewBag.Contests = contestsService.GetAllContests();
            if (contestId.HasValue)
            {
                ViewBag.Problems = problemsService.GetAllProblemsByContestId(contestId.Value);
            }

            if (problemId.HasValue)
            {
                ViewBag.Submissions = contestsService.GetAllSubmissionsByProblemId(problemId.Value);
            }

            return View();
        }

        [AuthenticatedAdminAttribute]
        public ActionResult SimilarityReport(int? contestId, int? problemId, int? threashold, HashSet<int> sIds)
        {
            if (problemId.HasValue && (sIds == null || sIds.Count < 2))
            {
                var problemSubmissions = contestsService.GetAllSubmissionsByProblemId(problemId.Value).Select(s => s.ID);
                sIds = new HashSet<int>(problemSubmissions);
            }

            if (sIds != null && sIds.Count > 1)
            {
                var sourceCodeList = new List<SourceCode>();
                var submissionList = new List<Submission>();
                foreach (var submissionId in sIds)
                {
                    var submission = contestsService.GetSubmissionById(submissionId, "Problem,Contestant");
                    if (submission != null)
                    {
                        var submissionSourceCode = contestsService.GetContestantSubmissionSourceCode(submission.ID_Contestant, submission.ID, _submissionsFolder);
                        var sourceCode = new SourceCode(submission.ID, submissionSourceCode,
                            submission.FileName.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries)[1]);
                        sourceCodeList.Add(sourceCode);
                        submissionList.Add(submission);
                    }
                }

                var result = sourceCodeSimilarityDetector.ComputeSimilarityPercentage(sourceCodeList, threashold ?? 10);
                ViewBag.Result = result;
                ViewBag.Submissions = submissionList;
            }

            return View();
        }

        #endregion
    }
}
