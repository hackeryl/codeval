﻿using CodEval.BLL.Models;
using CodEval.BLL.Services;
using CodEval.Web.Helpers;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using CodEval.DAL.Repository;
using System.IO;

namespace CodEval.Web.Controllers
{
    [Authenticated]
    public class SubmissionsController : Controller
    {
        private static string _submissionsFolder = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Submissions/");

        private ContestsService _contestsService;
        private ProblemsService _problemsService;
        private TestsService _testsService;

        public SubmissionsController()
            : this(new CodEvalEntities())
        {
        }

        public SubmissionsController(DbContext context)
        {
            _contestsService = new ContestsService(context);
            _problemsService = new ProblemsService(context);
            _testsService = new TestsService(context, String.Empty);
        }

        public ActionResult Index(int? id)
        {
            if (id.HasValue)
            {
                Submission submission;
                if (SessionHelper.CurrentAdmin != null)
                {
                    submission = _contestsService.GetSubmissionById(id.Value);
                }
                else
                {
                    submission = _contestsService.GetContestantSubmission(SessionHelper.CurrentContestant.ID, id.Value);
                }                 
                if (submission != null)
                {
                    var submissionSourceCode = _contestsService.GetContestantSubmissionSourceCode(submission.ID_Contestant, id.Value, _submissionsFolder);
                    ViewBag.Submission = submission;
                    ViewBag.SubmissionSourceCode = submissionSourceCode;
                    ViewData["tests"] = _testsService.GetTestsNamesForProblem(submission.ID_Problem);
                    ViewData["statuses"] = _testsService.GetAllStatusesNames();
                    return View(id);
                }
                return RedirectToAction("Index");
            }
            else
            {
                var problem_contest = _contestsService.GetAllProblem_ContestsNames();
                var submissionsStatuses = _contestsService.GetAllSubmissionsStatuses();
                List<SubmissionScore> submissionsScores;
                if (SessionHelper.CurrentAdmin != null)
                {
                    submissionsScores = _contestsService.GetSubmissionsScores();
                }
                else
                {
                    submissionsScores = _contestsService.GetContestantSubmissionsScores(SessionHelper.CurrentContestant.ID);
                }
                ViewData["problem_contest"] = problem_contest;
                ViewData["submissionsStatuses"] = submissionsStatuses;
                ViewData["submissionsScores"] = submissionsScores;

                return View(id);
            }
        }

        public ActionResult GetSubmissions([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_contestsService.GetContestantSubmissions(SessionHelper.CurrentContestant.ID).ToDataSourceResult(request));
        }

        public ActionResult GetSubmissionResults([DataSourceRequest] DataSourceRequest request, int submissionId)
        {
            if (SessionHelper.CurrentAdmin != null)
            {
                return Json(_contestsService.GetSubmissionResults(submissionId).ToDataSourceResult(request));
            }
            else
            {
                return Json(_contestsService.GetSubmissionResults(SessionHelper.CurrentContestant.ID, submissionId).ToDataSourceResult(request));
            }            
        }

        [HttpPost]
        public ActionResult Submit(HttpPostedFileBase file, int problemId)
        {
            var problem = _problemsService.GetProblemById(problemId);
            if (problem != null)
            {
                if (_contestsService.IsValidSubmission(problemId, file.FileName, file.ContentLength))
                {
                    var destinationFolder = _submissionsFolder + problem.Id_Contest + "/" + SessionHelper.CurrentContestant.Username;
                    bool status = _contestsService.AddSubmission(problemId, SessionHelper.CurrentContestant.ID, destinationFolder, file.FileName.ToLower(), file.InputStream);
                    if (status)
                    {
                        return Json(new { OK = true, Message = "Submitted!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { OK = false, Message = "An unexpected error occured, please try again!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { OK = false, Message = "Invalid submission file!" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { OK = false, Message = "Invalid problem!" }, JsonRequestBehavior.AllowGet);
        }
    }
}
