﻿using CodEval.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodEval.Web.Helpers
{
    public class SessionHelper
    {
        internal class Names
        {
            public static readonly string CurrentContestant = "CurrentContestant";
            public static readonly string CurrentAdmin = "CurrentAdmin";
        }

        public static Contestant CurrentContestant
        {
            get
            {
                return HttpContext.Current.Session[Names.CurrentContestant] as Contestant;
            }
            set
            {
                if (value is Contestant) HttpContext.Current.Session[Names.CurrentContestant] = value;
                else HttpContext.Current.Session[Names.CurrentContestant] = null;
            }
        }

        public static Admin CurrentAdmin
        {
            get
            {
                return HttpContext.Current.Session[Names.CurrentAdmin] as Admin;
            }
            set
            {
                if (value is Admin) HttpContext.Current.Session[Names.CurrentAdmin] = value;
                else HttpContext.Current.Session[Names.CurrentAdmin] = null;
            }
        }

        public static bool Authentificated 
        {
            get
            {
                return CurrentContestant != null || CurrentAdmin != null;
            }
        }
    }
}