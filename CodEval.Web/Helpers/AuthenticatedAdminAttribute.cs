﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CodEval.Web.Helpers
{
    public class AuthenticatedAdminAttribute : AuthorizeAttribute
    {
        private static readonly ActionResult UnauthorizedRedirectResult = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "Admin",
                                action = "Login"
                            })
                        );

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return SessionHelper.CurrentAdmin != null;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = UnauthorizedRedirectResult;
        }
    }
}