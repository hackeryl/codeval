﻿var Layout = (function (layout) {
    kendo.culture("en-GB");

    var time;
    var timeDisplay;

    layout.Init = function (currentTime, currentTimeDisplay) {
        timeDisplay = currentTimeDisplay;
        time = currentTime;
        setInterval(function () { nextSecond() }, 1000);
    };

    function nextSecond() {
        time = new Date(time.getTime() + 1000);
        timeDisplay.innerHTML = time.toLocaleTimeString("ro-RO");
    }

    function removeAdds() {
        for (var i = 0; i < 5; i++) {
            $('footer').next().next().remove();
        }
    }

    return layout;
})(Layout || {});