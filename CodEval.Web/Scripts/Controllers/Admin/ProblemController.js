﻿var ProblemController = function (controller) {
    var problemForm = $("#problemForm");

    var getProblemCategories = function () {
        var problemCategories = [];
        problemForm.find("#problemCategories").find("input[name='ProblemCategory']:checked").each(function (idx, s) {
            problemCategories.push(s.value);
        });

        return problemCategories;
    };

    var updateProblemCategories = function () {
        var problemId = problemForm.find("#ID").val();
        var problemCategories = getProblemCategories();
        var data = { problemId: problemId, problemCategoryIds: problemCategories };
        $.ajax({
            url: "/Admin/UpdateProblemCategories",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            async: false,
            success: function (response) {
                console.log(response);
            },
            error: function (jqXHR, exception) {
                console.log(jqXHR);
                console.log(exception);
            }
        });
    };

    var init = function () {
        problemForm.on("submit", function (e) {
            updateProblemCategories();
            return true;
        });
    };

    init();

    return controller;
}(ProblemController || {});