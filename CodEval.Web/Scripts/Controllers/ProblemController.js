﻿var ProblemController = function (controller) {
    var problemId = $("#problemId").val();
    var recommendationsContainer = $("#recommendations");
    var recommendationsTable = recommendationsContainer.find("#recommendationsTable > tbody");

    var displayRecommendations = function (recommendations) {
        console.log(recommendations);
        if (recommendations && recommendations.length > 0) {
            recommendationsContainer.css("display", "inline-block");
        }

        var baseKeys = ["Name", "Description", "Url"];
        for (var i = 0; i < recommendations.length; i++) {
            var row = "<tr><td><a href='{{Url}}' target='_blank'>{{Name}}</a></td><td><i>{{Description}}</i></td></tr>";
            row = row.replace("{{Name}}", recommendations[i].Name);
            row = row.replace("{{Description}}", recommendations[i].Description);
            row = row.replace("{{Url}}", recommendations[i].Url);
            recommendationsTable.append(row);
            if (recommendations[i].Details && recommendations[i].Details != null) {
                var hasDetails = false;
                row = "<tr><td colspan='2'>{{Details}}</td></tr>";
                var details = "<ul>";
                for (var key in recommendations[i].Details) {
                    if (baseKeys.indexOf(key) === -1 && recommendations[i].Details.hasOwnProperty(key) && recommendations[i].Details[key]) {
                        if (recommendations[i].Details[key].length === 0) {
                            continue;
                        }

                        hasDetails = true;
                        var detail = "<li><b>{{DetailKey}}</b>: <i>{{DetailValue}}</i></li>";
                        detail = detail.replace("{{DetailKey}}", key);
                        detail = detail.replace("{{DetailValue}}", recommendations[i].Details[key]);
                        details += detail;
                    }
                }

                details += "</ul>";
                row = row.replace("{{Details}}", details);
                if (hasDetails) {
                    recommendationsTable.append(row);
                }
            }
        }
    }

    var getProblemRecommendations = function () {
        $.getJSON("/Recommender/Problems?problemId=" + problemId, {}, displayRecommendations);
        $.getJSON("/Recommender/Articles?problemId=" + problemId, {}, displayRecommendations);
    };

    var init = function () {
        $(document).ready(getProblemRecommendations);
    };

    init();

    return controller;
}(ProblemController || {});